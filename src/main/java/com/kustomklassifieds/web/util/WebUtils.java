package com.kustomklassifieds.web.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by md_al on 11-Jan-18.
 */
public class WebUtils {

    public static List<Integer> getRandomNumbers(int prodCount, int listSize) {

        List<Integer> list = new ArrayList<>();

        if(listSize>=prodCount) {
            for(int i=0,j=0;i<listSize;i++) {
                if(j>=prodCount) {
                    j=0;
                }
                list.add(j);
            }
            return list;
        }

        Random random = new Random();

        for(int i=0;i<listSize;) {
            int now  = Math.abs(random.nextInt())%listSize;
            if(list.contains(now)) {
                continue;
            }
            list.add(now);
            i++;
        }


        return list;
    }

    public static boolean isNullOrEmpty(Object object) {
        return object==null || object.toString().equals("");
    }

    public static String fixApoS(String param) {
        return param==null?null:param.replace("'","\\'");
    }
}
