/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kustomklassifieds.web.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author md_al
 */
public class Constants {
    
    public static int TOTAL_CAR_PICS = 33; 
    
    public List<String> getDB_TABLES() {
        List<String> DB_TABLES = new ArrayList<String>();
        //DB_TABLES.add("category");
        DB_TABLES.add("condition_");
        DB_TABLES.add("customtype");
        DB_TABLES.add("drivetype");
        DB_TABLES.add("enginecylinders");
        DB_TABLES.add("fueldelivery");
        DB_TABLES.add("fueltype");
        //DB_TABLES.add("listingfeatures");
        //DB_TABLES.add("location");
        DB_TABLES.add("poweradders");
        DB_TABLES.add("transmissiontype");
        DB_TABLES.add("vehicletype");
        return DB_TABLES;
    }
    
    public List<String> getSEARCH_CRITERIA() {
        List<String> SEARCH_CRITERIA = new ArrayList<String>();
        //SEARCH_CRITERIA.add("Category");
        SEARCH_CRITERIA.add("Condition");
        SEARCH_CRITERIA.add("Kustom Type");
        SEARCH_CRITERIA.add("Drive Type");
        SEARCH_CRITERIA.add("Engine Cylinders");
        SEARCH_CRITERIA.add("Fuel Delivery");
        SEARCH_CRITERIA.add("Fuel Type");
        //SEARCH_CRITERIA.add("Listing Features");
        //SEARCH_CRITERIA.add("Location");
        SEARCH_CRITERIA.add("Power Adders");
        SEARCH_CRITERIA.add("Transmission Type");
        SEARCH_CRITERIA.add("Vehicle Type");
        return SEARCH_CRITERIA;
    }
    
    public Map<String,String> getDBMap() {
        Map<String,String> mpp  = new HashMap<>();
        
        List<String> DB_TABLES = getDB_TABLES();
        List<String> SEARCH_CRITERIA = getSEARCH_CRITERIA();
        
        for(int i=0;i<DB_TABLES.size();i++) {
            mpp.put(DB_TABLES.get(i), SEARCH_CRITERIA.get(i));
        }
        return mpp;
    }
    
    public String getCommaSeparatedTableNames() {
        String ret = "";
        List<String> DB_TABLES = getDB_TABLES();
        for(int i=0;i<DB_TABLES.size();i++) {
            if(i>0) {
                ret+=",";
            }
            ret+=DB_TABLES.get(i)+"Id";
        }
        return ret;
    }
}
