package com.kustomklassifieds.web.util;

import com.kustomklassifieds.Util.Utils;
import org.springframework.ui.Model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 * Created by md_al on 07-Mar-18.
 */
public class EmailUtil {
    public static void resetPasswordSendEmail(String email, String key) {
        String from = "kustomklassifieds";
        String pass = "Loiloi321";
        String[] to = { email }; // list of recipient email addresses
        String subject = "KustomKlassifieds! Password Reset Request!";
        String body = "Hi "+email+", \nSomeone has requested a password reset for your account! \n" +
                "Here is the password reset link: <a href='http://kustomklassifieds.com/loginRegister?email="+email+"&passResetKey="+key+"'>click here</a> \n" +
                "Didn't you requested for password reset? No worry! no one can do anything with your account! We will take care of it. :)\n\nHappy Day! :D.";

        sendFromGMail(from, pass, to, subject, body, false);
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body,boolean isHtml) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i = 0; i < to.length; i++ ) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);


            if(isHtml) {
                message.setContent(body, "text/html" );
            } else {
                message.setText(body, "UTF-8", "html");
            }



            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (AddressException ae) {
            ae.printStackTrace();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }

    public static void sendReceipt(String txId, String username, String paymentMethod) throws SQLException {

        Map<String, String> invoice = Utils.getInvoice(txId);

        String email= Utils.getEmail(Integer.parseInt(invoice.get("payer_id")));
        String invoiceId = invoice.get("id");
        String invoiceDate = invoice.get("datetime");
        String totalAmount = invoice.get("amount");


        if(email==null || email.equals("") || username==null || username.equals("")) {
            return;
        }



        String from = "kustomklassifieds";
        String pass = "Loiloi321";
        String[] to = { email }; // list of recipient email addresses
        String subject = "KustomKlassifieds! Payment Successful!";
        String body = "<!doctype html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <title>A simple, clean, and responsive HTML invoice template</title>\n" +
                "\n" +
                "    <style>\n" +
                "        .invoice-box {\n" +
                "            max-width: 800px;\n" +
                "            margin: auto;\n" +
                "            padding: 30px;\n" +
                "            border: 1px solid #eee;\n" +
                "            box-shadow: 0 0 10px rgba(0, 0, 0, .15);\n" +
                "            font-size: 16px;\n" +
                "            line-height: 24px;\n" +
                "            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;\n" +
                "            color: #555;\n" +
                "\n" +
                "            background-image: url('http://www.kustomklassifieds.com/resources/img/cars/3.jpg');\n" +
                "            background-size:     cover;\n" +
                "            color: white;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table {\n" +
                "            width: 100%;\n" +
                "            line-height: inherit;\n" +
                "            text-align: left;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table td {\n" +
                "            padding: 5px;\n" +
                "            vertical-align: top;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr td:nth-child(2) {\n" +
                "            text-align: right;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.top table td {\n" +
                "            padding-bottom: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.top table td.title {\n" +
                "            font-size: 45px;\n" +
                "            line-height: 45px;\n" +
                "            color: #333;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.information table td {\n" +
                "            padding-bottom: 40px;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.heading td {\n" +
                "            background: #eee;\n" +
                "            border-bottom: 1px solid #ddd;\n" +
                "            font-weight: bold;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.details td {\n" +
                "            padding-bottom: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.item td{\n" +
                "            border-bottom: 1px solid #eee;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.item.last td {\n" +
                "            border-bottom: none;\n" +
                "        }\n" +
                "\n" +
                "        .invoice-box table tr.total td:nth-child(2) {\n" +
                "            border-top: 2px solid #eee;\n" +
                "            font-weight: bold;\n" +
                "        }\n" +
                "\n" +
                "        @media only screen and (max-width: 600px) {\n" +
                "            .invoice-box table tr.top table td {\n" +
                "                width: 100%;\n" +
                "                display: block;\n" +
                "                text-align: center;\n" +
                "            }\n" +
                "\n" +
                "            .invoice-box table tr.information table td {\n" +
                "                width: 100%;\n" +
                "                display: block;\n" +
                "                text-align: center;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        /** RTL **/\n" +
                "        .rtl {\n" +
                "            direction: rtl;\n" +
                "            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;\n" +
                "        }\n" +
                "\n" +
                "        .rtl table {\n" +
                "            text-align: right;\n" +
                "        }\n" +
                "\n" +
                "        .rtl table tr td:nth-child(2) {\n" +
                "            text-align: left;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "\n" +
                "<body >\n" +
                "<div class=\"invoice-box\">\n" +
                "    <table cellpadding=\"0\" cellspacing=\"0\">\n" +
                "        <tr class=\"top\">\n" +
                "            <td colspan=\"2\">\n" +
                "                <table>\n" +
                "                    <tr>\n" +
                "                        <td class=\"title\">\n" +
                "                            <a class=\"navbar-brand\" style=\"width:200px;\" href=\"http://www.kustomklassifieds.com\"><img src=\"http://www.kustomklassifieds.com/resources/img/logo-white-t.png\" style=\"width: 100px; height: 27px;\"></a>\n" +
                "                        </td>\n" +
                "\n" +
                "                        <td style=\"float:right;\">\n" +
                "                            Invoice #: "+invoiceId+"<br>\n" +
                "                            Created: "+invoiceDate+"<br>\n" +
                "                            Status: PAID\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr class=\"information\">\n" +
                "            <td colspan=\"2\">\n" +
                "                <table>\n" +
                "                    <tr>\n" +
                "                        <td>\n" +
                "                            KustomKlassifieds, Inc.<br>\n" +
                "                            12345 Sunny Road<br>\n" +
                "                            New Jersey, USA\n" +
                "                        </td>\n" +
                "\n" +
                "                        <td style=\"float:right;\">\n" +
                "                            "+username+"<br>\n" +
                "                            "+email+"\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr class=\"heading\">\n" +
                "            <td style=\"background: #0e3b35cc\">\n" +
                "                Payment Method\n" +
                "            </td>\n" +
                "\n" +
                "            <td style=\"background: #0e3b35cc; \">\n" +
                "                <span style=\"float:right;\">Transaction #</span>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr class=\"details\">\n" +
                "            <td>\n" +
                "                "+paymentMethod+"\n" +
                "            </td>\n" +
                "\n" +
                "            <td >\n" +
                "                <span style=\"float:right;\">"+txId+"</span>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr class=\"heading\">\n" +
                "            <td style=\"background: #0e3b35cc\">\n" +
                "                Item\n" +
                "            </td>\n" +
                "\n" +
                "            <td style=\"background: #0e3b35cc;\">\n" +
                "                <span style=\"float:right;\">Price</span>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr class=\"item\">\n" +
                "            <td>\n" +
                "                Vehicle Listing\n" +
                "            </td>\n" +
                "\n" +
                "            <td>\n" +
                "                <span style=\"float:right;\">$"+totalAmount+"</span>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr class=\"total\">\n" +
                "            <td></td>\n" +
                "\n" +
                "            <td>\n" +
                "                <span style=\"float:right;\">Total: $"+totalAmount+"</span>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";

        sendFromGMail(from, pass, to, subject, body, true);
    }
}