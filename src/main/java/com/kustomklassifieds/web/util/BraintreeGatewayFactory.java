package com.kustomklassifieds.web.util;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class BraintreeGatewayFactory {
    public static BraintreeGateway fromConfigMapping(Map<String, String> mapping) {
        return new BraintreeGateway(
            mapping.get("BT_ENVIRONMENT"),
            mapping.get("BT_MERCHANT_ID"),
            mapping.get("BT_PUBLIC_KEY"),
            mapping.get("BT_PRIVATE_KEY")
        );
    }

    public static BraintreeGateway fromConfigFile(File configFile) {
        InputStream inputStream = null;
        Properties properties = new Properties();

        try {
            inputStream = new FileInputStream(configFile);
            properties.load(inputStream);
        } catch (Exception e) {
            System.err.println("Exception: " + e);
        } finally {
            try { inputStream.close(); }
            catch (IOException e) { System.err.println("Exception: " + e); }
        }

        return new BraintreeGateway(
            properties.getProperty("BT_ENVIRONMENT"),
            properties.getProperty("BT_MERCHANT_ID"),
            properties.getProperty("BT_PUBLIC_KEY"),
            properties.getProperty("BT_PRIVATE_KEY")
        );
    }

    public static BraintreeGateway fromDirectInsertion() {
        /*String BT_ENVIRONMENT="sandbox";
        String BT_MERCHANT_ID="tsvr5m595q9m83bn";
        String BT_PUBLIC_KEY="d8wvfh6p6cqshnn8";
        String BT_PRIVATE_KEY="b4a29a69835a12dfb18051e1e7812f5a";
        return new BraintreeGateway(Environment.SANDBOX,BT_MERCHANT_ID,BT_PUBLIC_KEY,BT_PRIVATE_KEY);*/

        /*String BT_MERCHANT_ID="q7nqt6y76mx3kqm7";
        String BT_PUBLIC_KEY="vvyz8f6dt4f7xwvz";
        String BT_PRIVATE_KEY="6dcad619994dfafdd5a8d558b180f050";
        return new BraintreeGateway(Environment.PRODUCTION,BT_MERCHANT_ID,BT_PUBLIC_KEY,BT_PRIVATE_KEY);*/

        BraintreeGateway productionGateway = new BraintreeGateway(
                Environment.PRODUCTION,
                "q7nqt6y76mx3kqm7",
                "mk3gjkmsd9wh9gpz",
                "9b313f6331e03ea2bff1b5d39b1b07b1"
        );

        BraintreeGateway sandboxGateway = new BraintreeGateway(
                Environment.SANDBOX,
                "tsvr5m595q9m83bn",
                "d8wvfh6p6cqshnn8",
                "b4a29a69835a12dfb18051e1e7812f5a"
        );
        return Statics.IS_PRODUCTION?productionGateway:sandboxGateway;
    }
}
