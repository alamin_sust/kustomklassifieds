/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kustomklassifieds.web.util;

import java.util.List;

/**
 *
 * @author md_al
 */
public class Statics {
    //success
    public static final String LOGIN_SUCCEESS = "Successfully Logged In!";
    public static final String REGISTER_SUCCEESS = "Registration Successfull!";
    
    //error
    public static final String LOGIN_USERNAME_NOT_EXISTS = "Username doesn't exist!";
    public static final String LOGIN_MISSMATCH = "Username and Password doesn't match!";
    public static final String REGISTER_USERNAME_ALREADY_EXISTS = "Username or Email already exists!";
    public static final String REGISTER_PASSWORD_MISSMATCH = "Passwords doesn't match!";
    public static final String FP_EMAIL_NOT_EXISTS = "Email doesn't exist!";
    public static final String LINK_EXPIRED = "Link Expired or doesn't exist! Please Try Again.";
    public static final String PASSWORD_RESET_SUCCESS = "Password reset successfully!";
    public static final String PASSWORD_RESET_MAIL_SENT = "Email Sent with Password Reset Link! Please Check Your Email. Don't forget to check spam folder too.";
    public static final String SOMETHING_WENT_WRONG = "Something Went Wrong, Please Try Again.";

    public static final String PRODUCT_STATUS_UNPAID = "unpaid";
    public static final String PRODUCT_STATUS_PAID = "paid";

    public static final String PAYMENT_SUCCESS = "Payment Successful.";
    public static final String ALREADY_PAID = "You've Already Paid.";

    public static final boolean IS_PRODUCTION = true;

    public static final int COUPON_ACTIVE = 1;
    public static final int COUPON_USED = 2;
    public static final int COUPON_EXPIRED = 3;
    public static final int COUPON_CANCELLED_BY_ADMIN = 4;

}
