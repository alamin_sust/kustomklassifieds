package com.kustomklassifieds.web.util;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by md_al on 01-Feb-18.
 */
public class LocationUtils {
    public static String getLocationByIp(String ip) throws IOException, GeoIp2Exception {
        String location="";

        //String ip = "your-ip-address";
        String dbLocation = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\KustomKlassifieds\\src\\main\\resources\\GeoLite2-City_20180102\\GeoLite2-City.mmdb";

        File database = new File(dbLocation);
        DatabaseReader dbReader = new DatabaseReader.Builder(database)
                .build();

        InetAddress ipAddress = InetAddress.getByName(ip);
        CityResponse response = dbReader.city(ipAddress);

        String countryName = response.getCountry().getName();
        String cityName = response.getCity().getName();
        String postal = response.getPostal().getCode();
        String state = response.getLeastSpecificSubdivision().getName();

        String latitude = String.valueOf(response.getLocation().getLatitude());
        String longitude = String.valueOf(response.getLocation().getLongitude());

        location = latitude+"_"+longitude+"_"+countryName+"_"+state+"_"+cityName;

        return location;
    }

    public static boolean isDistanceWithinMeters(String lat1, String lng1, String lat2, String lng2, float meters) {

        if(lat1==null || lat2==null || lng1 == null || lng2 == null) return true;

        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(Float.valueOf(lat2) - Float.valueOf(lat1));
        double dLng = Math.toRadians(Float.valueOf(lng2) - Float.valueOf(lng1));
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(Float.valueOf(lat1))) * Math.cos(Math.toRadians(Float.valueOf(lat2))) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist<=meters;
    }

    public static String getIpByRequest(HttpServletRequest request) {
        return request.getRemoteAddr().equals("0:0:0:0:0:0:0:1")?"204.12.241.178":request.getRemoteAddr();
    }
}
