package com.kustomklassifieds.web.controller;

import com.kustomklassifieds.service.AppAuthService;
import com.kustomklassifieds.web.util.EmailUtil;
import com.kustomklassifieds.web.util.WebUtils;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by md_al on 09-Dec-17.
 */
@Controller
public class OtherController {

    @Autowired
    private AppAuthService appAuthService;

    @GetMapping("/addProduct")
    public String addProductGet() {
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addOrUpdateProductPost(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws SQLException, IOException, FileUploadException {
        String ret = appAuthService.processMultipartRequest(request, session);
        if(ret.startsWith("id")) {
            return "products";
        } else if(!WebUtils.isNullOrEmpty(ret)) {
            session.setAttribute("paymentProductId", ret);
            return "redirect:checkout";
        } else {
            return "addProduct";
        }
    }

    @GetMapping("/aboutUs")
    public String aboutUsGet() {
        return "aboutUs";
    }

    @PostMapping("/aboutUs")
    public String aboutUsPost() {
        return "aboutUs";
    }

    @GetMapping("/admin")
    public String adminGet() {
        return "admin";
    }

    @PostMapping("/admin")
    public String adminPost() {
        return "admin";
    }

    @GetMapping("/customSearch")
    public String customSearchGet() {
        return "customSearch";
    }

    @PostMapping("/customSearch")
    public String customSearchPost() {
        return "customSearch";
    }

    @GetMapping("/legal")
    public String legalGet() {
        return "legal";
    }

    @PostMapping("/legal")
    public String legalPost() {
        return "legal";
    }

    @GetMapping("/loginRegister")
    public String loginRegisterGet() {
        return "loginRegister";
    }

    @PostMapping("/loginRegister")
    public String loginRegisterPost() {
        return "loginRegister";
    }

    @GetMapping("/payment")
    public String paymentGet() {
        return "payment";
    }

    @PostMapping("/payment")
    public String paymentPost() {
        return "payment";
    }

    @GetMapping("/products")
    public String productsGet() {
        return "products";
    }

    @PostMapping("/products")
    public String productsPost() {
        return "products";
    }

    @GetMapping("/profile")
    public String profileGet() {
        return "profile";
    }

    @PostMapping("/profile")
    public String profilePost() {
        return "profile";
    }

    @GetMapping("/termsOfService")
    public String termsOfServiceGet() {
        return "termsOfService";
    }

    @PostMapping("/termsOfService")
    public String termsOfServicePost() {
        return "termsOfService";
    }

    @GetMapping("/dropin")
    public String dropinGet() {
        return "dropin";
    }

    @GetMapping("/chat")
    public String chatGet() {
        return "chat";
    }

    @PostMapping("/chat")
    public String chatPost() {
        return "chat";
    }

    @GetMapping("/createDiscountCoupon")
    public String createDiscountCouponGet() {
        return "createDiscountCoupon";
    }

    @PostMapping("/createDiscountCoupon")
    public String createDiscountCouponPost() {
        return "createDiscountCoupon";
    }

    @GetMapping("/productDetails")
    public String productDetailsGet() {
        return "productDetails";
    }

    @PostMapping("/productDetails")
    public String productDetailsPost() {
        return "productDetails";
    }

}