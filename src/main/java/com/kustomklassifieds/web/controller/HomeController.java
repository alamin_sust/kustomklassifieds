package com.kustomklassifieds.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by Al-Amin on 3/8/2017.
 */
@Controller
public class HomeController {

    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String REDIRECT_COMING_SOON = "redirect:/comingSoon";
    private static final String HOME = "home";
    private static final String COMING_SOON = "comingSoon";

    @GetMapping("/")
    public String goHome() {
        return REDIRECT_HOME;
    }

    @GetMapping("/home")
    public String home() {
        return HOME;
    }

    @PostMapping("/home")
    public String homePost() {
        return HOME;
    }

    @GetMapping("/comingSoon")
    public String cominSoon() {
        return COMING_SOON;
    }

    @PostMapping("/comingSoon")
    public String cominSoonPost() {
        return COMING_SOON;
    }
}
