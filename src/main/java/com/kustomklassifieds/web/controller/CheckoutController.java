package com.kustomklassifieds.web.controller;

import com.braintreegateway.*;
import com.braintreegateway.Transaction.Status;
import com.kustomklassifieds.service.AppAuthService;
import com.kustomklassifieds.web.util.Application;
import com.kustomklassifieds.web.util.EmailUtil;
import com.kustomklassifieds.web.util.Statics;
import com.kustomklassifieds.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Arrays;

import static com.kustomklassifieds.web.util.WebUtils.fixApoS;

@Controller
public class CheckoutController {

    @Autowired
    private AppAuthService appAuthService;

    private BraintreeGateway gateway = Application.getGateway();

     private Status[] TRANSACTION_SUCCESS_STATUSES = new Status[] {
        Status.AUTHORIZED,
        Status.AUTHORIZING,
        Status.SETTLED,
        Status.SETTLEMENT_CONFIRMED,
        Status.SETTLEMENT_PENDING,
        Status.SETTLING,
        Status.SUBMITTED_FOR_SETTLEMENT
     };

    /*@RequestMapping(value = "/", method = RequestMethod.GET)
    public String root(Model model) {
        return "redirect:checkout";
    }*/

    @GetMapping("/checkout")
    public String checkout(Model model, HttpServletRequest request, HttpSession session) throws SQLException {

        String productId = (String) session.getAttribute("paymentProductId");
        session.setAttribute("paymentProductId", null);

        if(productId!=null && !productId.equals("")
                && appAuthService.checkIfPaid(fixApoS(productId))) {
            String clientToken = gateway.clientToken().generate();
            model.addAttribute("clientToken", clientToken);
            model.addAttribute("productId", fixApoS(productId));
            model.addAttribute("totalAmount", appAuthService.getTotalAmountToPay(fixApoS(productId)));
            return "new";
        } else {
            session.setAttribute("errorMsg", Statics.SOMETHING_WENT_WRONG);
            return "addProduct";
        }

    }

    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public String postForm(HttpSession session, @RequestParam("amount") double amount, @RequestParam("payment_method_nonce") String nonce, @RequestParam("productId") String productId, Model model, final RedirectAttributes redirectAttributes) throws SQLException, InterruptedException {

        Thread.sleep(5000);

        if(!(productId!=null && !productId.equals("")
                && appAuthService.checkIfPaid(productId)) || appAuthService.isNonceUsed(nonce)) {
            if(!appAuthService.checkIfPaid(productId)) {
                session.setAttribute("errorMsg", Statics.ALREADY_PAID);
            } else {
                session.setAttribute("errorMsg", Statics.SOMETHING_WENT_WRONG);
            }
            return "addProduct";
        }


        BigDecimal decimalAmount;
        try {
            amount = Math.round(amount*100.0)/100.0;
            decimalAmount = new BigDecimal(String.valueOf(amount));
        } catch (NumberFormatException e) {
            session.setAttribute("errorMsg", "Error: 81503: Amount is an invalid format.");
            return "addProduct";
        }


            TransactionRequest request = new TransactionRequest()
                    .amount(decimalAmount)
                    .paymentMethodNonce(Statics.IS_PRODUCTION?nonce:"fake-valid-nonce")
                    .options()
                    .submitForSettlement(true)
                    .done();
        if((productId!=null && !productId.equals("")
                && appAuthService.checkIfPaid(productId))) {
            Result<Transaction> result = gateway.transaction().sale(request);
            Transaction transaction;
            CreditCard creditCard;
            Customer customer;
            if (result.isSuccess()) {
                transaction = result.getTarget();
                appAuthService.insertTransaction(transaction.getId(), (String) session.getAttribute("id"),productId, String.valueOf(transaction.getAmount()));
                appAuthService.setProductStatus(productId, Statics.PRODUCT_STATUS_PAID, transaction.getId());
            } else if (result.getTransaction() != null) {
                //transaction = result.getTransaction();
                //appAuthService.setProductStatus(productId, Statics.PRODUCT_STATUS_PAID, transaction.getId());
            } else if(appAuthService.checkIfPaid(productId)){
                String errorString = "";
                for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
                    errorString += "Error: " + error.getCode() + ": " + error.getMessage() + "\n";
                }
                session.setAttribute("errorMsg", errorString);
                return "addProduct";
            }
            if (!appAuthService.checkIfPaid(productId)) {

                try {
                    transaction = gateway.transaction().find(appAuthService.getPaymentTxId(productId));
                    creditCard = transaction.getCreditCard();
                    customer = transaction.getCustomer();

                    model.addAttribute("isSuccess", "Success: " + Arrays.asList(TRANSACTION_SUCCESS_STATUSES).contains(transaction.getStatus()));
                    model.addAttribute("transaction", "transaction: " + transaction.getId());
                    model.addAttribute("amount", "amount: " + transaction.getAmount());
                    model.addAttribute("creditCard", "creditCard: " + creditCard.getCardholderName());
                    model.addAttribute("customer", "customer: " + customer.getLastName() + "," + customer.getFirstName());
                    if(session.getAttribute("id")!=null && !session.getAttribute("id").toString().equals("")) {
                        EmailUtil.sendReceipt(transaction.getId(), (String) session.getAttribute("username"), transaction.getPaymentInstrumentType());
                    }
                    session.setAttribute("successMsg", Statics.PAYMENT_SUCCESS);
                    return "paymentSuccess";
                } catch (Exception e) {
                    System.out.println("Exception: " + e);
                    session.setAttribute("errorMsg", Statics.SOMETHING_WENT_WRONG);
                    return "paymentSuccess";
                }
            }
        }
        return "paymentSuccess";
    }

    /*@GetMapping(value = "/checkout/{transactionId}")
    public String getTransaction(@PathVariable String transactionId, Model model) {
        Transaction transaction;
        CreditCard creditCard;
        Customer customer;

        try {
            transaction = gateway.transaction().find(transactionId);
            creditCard = transaction.getCreditCard();
            customer = transaction.getCustomer();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            return "addProduct";
        }

        model.addAttribute("isSuccess","Success: "+ Arrays.asList(TRANSACTION_SUCCESS_STATUSES).contains(transaction.getStatus()));
        model.addAttribute("transaction","transaction: "+ transaction.getAmount());
        model.addAttribute("creditCard","creditCard: "+ creditCard.getCardholderName());
        model.addAttribute("customer","customer: "+ customer.getLastName()+","+customer.getFirstName());

        return "paymentSuccess";
    }*/

    @GetMapping("/paymentSuccess")
    public String paymentSuccessGet() {
        return "paymentSuccess";
    }
}
