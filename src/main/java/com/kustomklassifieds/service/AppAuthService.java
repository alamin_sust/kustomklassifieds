package com.kustomklassifieds.service;

import com.kustomklassifieds.Util.Utils;
import com.kustomklassifieds.connection.Database;
import com.kustomklassifieds.web.util.Constants;
import com.kustomklassifieds.web.util.LocationUtils;
import com.kustomklassifieds.web.util.Statics;
import com.kustomklassifieds.web.util.WebUtils;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Al-Amin on 3/11/2017.
 */
@Service
public class AppAuthService {


    public boolean isAuthorized(String authImie) throws SQLException {
        Database db = new Database();
        db.connect();
        try {


            Statement stAuth = db.connection.createStatement();

            //String authPassword = fixApoS(request.getParameter("authPassword"));

            //String qryAuth = "select password from soldier where username like '" + authUsername + "'";
            String qryAuth = "select count(*) as cnt from registered_mobiles where substr(imie_number,1,15) like '" + authImie.replace("/", "").substring(0, 15) + "' and status=1";


            ResultSet rsAuth = stAuth.executeQuery(qryAuth);

            rsAuth.next();

            if (rsAuth.getString("cnt").equals("1")) {
                db.close();
                return true;
            } else {
                db.close();
                return !false;
            }


            /*if (rsAuth.getString("password").equals(authPassword)) {
            db.close();
                return true;
            } else {
             db.close();
                return false;
            }*/
        } catch (Exception e) {
            db.close();
            return !false;
        }
    }

    public String processMultipartRequest(HttpServletRequest request, HttpSession session) throws FileUploadException, IOException, SQLException {

        Database db = new Database();
        String ret = "";
        db.connect();
        try {
            HashMap<String, String> mpp = new HashMap<>();

            String name = "";
            String price = "";
            String year = "";
            String make = "";
            String model = "";
            String place = "";
            String contactInfo = "";
            String zipCode = "";
            String token = "";
            String feature = "0";
            String imageCount = "";
            String ownerId = "";
            String insertProduct = "";
            String updateProduct = "";
            String details = "";
            String pkg = "";
            String isFeatured = "0";

            String totalAmount = "0";
            String add5days = "false";
            String searchFeature = "false";

            String q = "select max(id)+1 as mxId from product";
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            rs.next();
            String mxId = rs.getString("mxId");


            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

            List<InputStream> fileContentList = new LinkedList<>();
            boolean hasImage = false;

            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldvalue = item.getString().replace("'","\\'");
                    // ... (do your job here)
                    if (fieldname.equals("ownerId")) {
                        ownerId = fieldvalue;
                    } else if (fieldname.equals("name")) {
                        name = fieldvalue;
                    } else if (fieldname.equals("details")) {
                        details = fieldvalue;
                    } else if (fieldname.equals("price")) {
                        price = fieldvalue.replace("$", "").replace(",", "");
                    } else if (fieldname.equals("year")) {
                        year = fieldvalue;
                    } else if (fieldname.equals("make")) {
                        make = fieldvalue;
                    } else if (fieldname.equals("model")) {
                        model = fieldvalue;
                    } else if (fieldname.equals("place")) {
                        place = fieldvalue;
                    } else if (fieldname.equals("contactInfo")) {
                        contactInfo = fieldvalue;
                    } else if (fieldname.equals("zipCode")) {
                        zipCode = fieldvalue;
                    } else if (fieldname.equals("token")) {
                        token = fieldvalue;
                    } else if (fieldname.equals("feature")) {
                        feature = fieldvalue;
                    } else if (fieldname.equals("imageCount")) {
                        imageCount = fieldvalue;
                    } else if (fieldname.equals("insert")) {
                        insertProduct = fieldvalue;
                    } else if (fieldname.equals("update")) {
                        updateProduct = fieldvalue;
                    } else if (fieldname.equals("totalAmount")) {
                        totalAmount = fieldvalue;
                    } else if (fieldname.equals("add5days")) {
                        add5days = fieldvalue;
                    } else if (fieldname.equals("searchFeature")) {
                        searchFeature = fieldvalue;
                    } else if (fieldname.equals("package")) {
                        pkg = fieldvalue;
                    } else if (fieldname.equals("isFeatured")) {
                        isFeatured = fieldvalue;
                    } else {
                        mpp.put(fieldname, fieldvalue);
                    }
                } else {
                    // Process form file field (input type="file").
                    String fieldname = item.getFieldName();
                    //String filename = FilenameUtils.getName(item.getName());

                    if (fieldname.startsWith("file")) {
                        fileContentList.add(item.getInputStream());
                        hasImage = true;
                    }
                }
            }

            //validation
            Double calculatedTotalAmount=0.0;

            if (updateProduct.equals("")) {
                if (pkg.equals("free")) {
                    calculatedTotalAmount = 0.0;
                } else if (pkg.equals("premium")) {
                    calculatedTotalAmount = 8.99;
                } else {
                    calculatedTotalAmount = 14.99;
                }

                if(isFeatured.equals("1")) {
                    calculatedTotalAmount += 3.99;
                }

                if (Math.abs(calculatedTotalAmount - Double.valueOf(totalAmount)) > 0.00001) {
                    session.setAttribute("errorMsg", "Something Went Wrong! Please Try Again.");
                    return "";
                }

                calculatedTotalAmount = makeDiscountByToken(token, calculatedTotalAmount);
            }


            if (hasImage) {
                // ... (do your job here)
//                        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
//                        Calendar cal = Calendar.getInstance();
                //System.out.println(dateFormat.format(cal.getTime()));

                for (int i = 0; i < fileContentList.size(); i++) {

                    InputStream fileContent = fileContentList.get(i);

                    String postFix = "";

                    if (i != 0) {
                        postFix += "-" + (i + 1);
                    }

                    File bfile = new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\KustomKlassifieds\\src\\main\\webapp\\resources\\img\\products\\" + ownerId + "_" + (!insertProduct.equals("") ? mxId : mpp.get("productId")) + postFix + ".jpg");
                    File bfileBuild = new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\KustomKlassifieds\\build\\libs\\exploded\\KustomKlassifieds.war\\resources\\img\\products\\" + ownerId + "_" + (!insertProduct.equals("") ? mxId : mpp.get("productId")) + postFix + ".jpg");
                    //out.print(getServletContext().getRealPath("/"));
                    OutputStream outStream = new FileOutputStream(bfile);
                    OutputStream outStreamBuild = new FileOutputStream(bfileBuild);

                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = fileContent.read(buffer)) > 0) {
                        outStream.write(buffer, 0, length);
                        outStreamBuild.write(buffer, 0, length);
                    }
                    fileContent.close();
                    outStream.close();
                    outStreamBuild.close();
                }


                //String query2 = "insert into pillar_updated_by(soldier_id,pillar_id,img_url,longitude,latitude,battalion,imie_number) values(" + soldierId + ","+id+", '" + id + "_" + dateFormat.format(cal.getTime()) + "_" + situation + "_.jpg" + "','"+longitude+"','"+latitude+"','"+ ConverterUtil.getBattalionFromImie(authImie)+"','"+authImie+"')";

                //Statement st2 = db.connection.createStatement();

                //st2.executeUpdate(query2);
            }


            //String query = "insert into pillar(id,name,number,longitude,latitude,situation) values("+id+",'"+name+"',"+number+", '"+longitude+"', '"+latitude+"', '"+situation+"')";

            String query;
            Statement stMig = db.connection.createStatement();
            Constants constants = new Constants();

            String values = "";
            String valuesUpdate = "";
            List<String> tables = constants.getDB_TABLES();
            for (int i = 0; i < tables.size(); i++) {
                if (i > 0) {
                    values += ",";
                }
                values += (mpp.get(tables.get(i)) == null || mpp.get(tables.get(i)).equals("")) ? "0" : mpp.get(tables.get(i));
            }
            for (int i = 0; i < tables.size(); i++) {

                String value = WebUtils.isNullOrEmpty(mpp.get(tables.get(i))) ? "0" : mpp.get(tables.get(i));

                valuesUpdate += ", " + tables.get(i) + "Id=" + value + " ";

            }
            if (!insertProduct.equals("")) {


                String ip = LocationUtils.getIpByRequest(request);
                String geoLocation = LocationUtils.getLocationByIp(ip);
                String latitude = geoLocation.split("_")[0];
                String longitude = geoLocation.split("_")[1];

                String q2 = "insert into product (id,name,details,price,year,make,model,place,contact_info,zip_code,token,feature,image_count,ownerId,"
                        + constants.getCommaSeparatedTableNames() + ",categoryId" + ",ip,latitude,longitude,datetime,listing_fees,package,is_featured) values("
                        + mxId + ",'"
                        + name + "','"
                        + details + "',"+ price
                        + ",'" + year + "','" + make
                        + "','" + model + "', '"+ place + "', '" + contactInfo + "', '" +zipCode + "', '" +token + "', " + feature + " ," + fileContentList.size() + "," + ownerId
                        + "," + values + ",1,'" + ip + "','" + latitude + "','" + longitude + "'" +
                        ",now(),'" + (Math.round(calculatedTotalAmount * 100)/100.0) + "','"+pkg+"',"+isFeatured+")";


                Statement st2 = db.connection.createStatement();
                st2.executeUpdate(q2);

                ret = calculatedTotalAmount>0.0000001?mxId:"";

                if(!WebUtils.isNullOrEmpty(session.getAttribute("username"))
                        && Utils.isAdmin(session.getAttribute("username"))) {

                    Utils.setProductStatus(mxId, Statics.PRODUCT_STATUS_PAID, "00");
                    ret="";
                } else if(ret.equals("")) {
                    Utils.setProductStatus(mxId, Statics.PRODUCT_STATUS_PAID, "XX");
                }

                session.setAttribute("successMsg", "Product Added Successfully!");

            } else if (!updateProduct.equals("")) {
                query = "update product  set " +
                        " name='" + name
                        + "', price=" + price
                        + ", year='" + year
                        + "', make='" + make
                        + "', model='" + model
                        + "', place='" + place
                        + "', contact_info='" + contactInfo
                        + "', zip_code='" + zipCode
                        + "', details='" + details
                        + "', ownerId=" + ownerId
                        + valuesUpdate
                        + " where id=" + mpp.get("productId");
                stMig.executeUpdate(query);
                session.setAttribute("successMsg", "Successfully Updated!");
                ret="id="+mpp.get("productId");
            }

        } catch (Exception e) {
            System.out.println(e);
            ret = "";
        } finally {
            db.close();
        }
        return ret;
    }

    private Double makeDiscountByToken(String token, Double calculatedTotalAmount) throws SQLException {

        if(token==null || token.equals("")) {
            return calculatedTotalAmount;
        }

        Database db = new Database();
        db.connect();
        try {
            String q = "select * from token where code='"+token+"' and is_active="+Statics.COUPON_ACTIVE;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if(rs.next()) {
                calculatedTotalAmount = Math.round(calculatedTotalAmount * (100 - rs.getInt("discount_percentage"))) / 100.0;
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return calculatedTotalAmount;

    }

    public boolean checkIfPaid(String productId) throws SQLException {
        Database db = new Database();
        boolean ret=false;
        db.connect();
        try {
            String q = "select * from product where id="+productId;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if(rs.next()&& rs.getString("status")!=null && rs.getString("status").equals("unpaid")) {
               ret=true;
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return ret;
    }

    public String getTotalAmountToPay(String productId) throws SQLException {
        Database db = new Database();
        String ret="";
        db.connect();
        try {
            String q = "select * from product where id="+productId;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if(rs.next()&& rs.getString("listing_fees")!=null && !rs.getString("listing_fees").equals("")) {
                ret=rs.getString("listing_fees");
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return ret;
    }

    public void setProductStatus(String productId, String status, String txId) throws SQLException {
        Utils.setProductStatus(productId,status,txId);
    }

    public String getPaymentTxId(String productId) throws SQLException {
        Database db = new Database();
        String ret="";
        db.connect();
        try {
            String q = "select * from product where id="+productId;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if(rs.next()) {
                ret=rs.getString("payment_tx_id");
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return ret;
    }

    public boolean isNonceUsed(String nonce) throws SQLException {
        Database db = new Database();
        boolean ret=true;
        db.connect();
        try {
            String q = "select count(*) as cnt from used_nonce where nonce='"+nonce+"'";
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if(rs.next() && rs.getString("cnt").equals("0")) {
                ret=false;
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return ret;
    }

    public void insertTransaction(String txId, String payerId, String productId, String amount) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            if(payerId==null||payerId.equals("")) {
                payerId="0";
            }
            if(productId==null||productId.equals("")) {
                productId="0";
            }
            String q = "INSERT INTO transaction (tx_id, payer_id, product_id,amount) VALUES ('"+txId+"',"+payerId+","+productId+",'"+amount+"')";
            Statement st = db.connection.createStatement();
            st.executeUpdate(q);

        } catch (Exception e) {

        } finally {
            db.close();
        }
    }
}
