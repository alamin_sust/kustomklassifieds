package com.kustomklassifieds.Util;

import com.kustomklassifieds.connection.Database;
import com.kustomklassifieds.service.AppAuthService;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class Utils {

    public static void setProductStatus(String productId, String status, String txId) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            String q = "update product set status='"+status+"',payment_tx_id='"+txId+"' where id="+productId+" and status!='"+status+"'";
            Statement st = db.connection.createStatement();
            st.executeUpdate(q);
        } catch (Exception e) {

        } finally {
            db.close();
        }
    }

    public static String getEmail(int id) throws SQLException {
        Database db = new Database();
        db.connect();
        String ret = "";
        try {
            String q = "select email from user where id="+id;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            rs.next();
            ret= rs.getString("email");
        } catch (Exception e) {

        } finally {
            db.close();
        }
        return ret;
    }

    public static Map<String,String> getInvoice(String txId) throws SQLException {
        Database db = new Database();
        db.connect();
        Map<String,String> ret = new TreeMap<>();
        try {
            String q = "select * from transaction where tx_id='"+txId+"'";
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            rs.next();
            ret.put("id",rs.getString("id"));
            ret.put("tx_id",rs.getString("tx_id"));
            ret.put("payer_id",rs.getString("payer_id"));
            ret.put("product_id",rs.getString("product_id"));
            ret.put("amount",rs.getString("amount"));
            ret.put("datetime",rs.getString("datetime"));
        } catch (Exception e) {

        } finally {
            db.close();
        }
        return ret;
    }

    public static boolean isAdmin(Object username) {
        return  username!=null
                && (username.equals("al") || username.equals("kustomklassifieds"));
    }

    public static String generateDiscountCouponCode() {
        Random rand = new Random();
        String ret="";

        for(int i=0;i<8;i++) {
            int ind = rand.nextInt(36);
            if(ind<10) {
                ret+=""+ind;
            } else {
                ret+=""+ ((char)((ind-10)+(int)'A'));
            }
        }
        return ret;
    }
}
