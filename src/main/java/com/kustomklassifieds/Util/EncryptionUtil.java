package com.kustomklassifieds.Util;

import org.mindrot.jbcrypt.BCrypt;

public class EncryptionUtil {

    public static String generateSecuredHash(String originalString) {
        return BCrypt.hashpw(originalString, BCrypt.gensalt(12));
    }

    public static boolean matchWithSecuredHash(String providedString, String existingHash) {
        boolean ret;
        try {
            ret = BCrypt.checkpw(providedString, existingHash);
        } catch (Exception e) {
            ret=false;
            System.out.println(e);
        }
        return ret;
    }
}
