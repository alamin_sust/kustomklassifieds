RENAME TABLE `transaction` TO `deal`;

create table transaction
(
	id int auto_increment
		primary key,
	tx_id varchar(1010) not null,
	payer_id int null,
	product_id int not null,
	amount varchar(110) not null,
	datetime timestamp default CURRENT_TIMESTAMP null,
	constraint transaction_id_uindex
		unique (id),
	constraint transaction_txId_uindex
		unique (tx_id)
)
;











