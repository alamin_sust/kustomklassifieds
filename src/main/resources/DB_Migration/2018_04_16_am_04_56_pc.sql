-- auto-generated definition
create table token
(
  id          int auto_increment
    primary key,
  code        varchar(110)                        not null,
  name        varchar(110)                        null,
  expiry_date timestamp default CURRENT_TIMESTAMP not null
  on update CURRENT_TIMESTAMP,
  is_active   int                                 null,
  constraint token_id_uindex
  unique (id)
)
  engine = InnoDB;

ALTER TABLE product ADD token varchar(110) NULL;
