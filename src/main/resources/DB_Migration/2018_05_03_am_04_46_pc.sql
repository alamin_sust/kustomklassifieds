create table used_nonce
(
	id int auto_increment
		primary key,
	nonce varchar(1010) not null,
	constraint used_nonce_id_uindex
		unique (id)
)
;

