ALTER TABLE token ADD UNIQUE (code);
ALTER table token add COLUMN discount_percentage int;
ALTER TABLE token modify discount_percentage int not null;