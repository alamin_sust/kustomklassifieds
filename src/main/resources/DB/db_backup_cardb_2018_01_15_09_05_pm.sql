-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cardb
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (0,'0'),(1,'Vehicle'),(2,'Parts');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condition_`
--

DROP TABLE IF EXISTS `condition_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `condition_` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condition_`
--

LOCK TABLES `condition_` WRITE;
/*!40000 ALTER TABLE `condition_` DISABLE KEYS */;
INSERT INTO `condition_` VALUES (0,'0'),(1,'new'),(2,'used');
/*!40000 ALTER TABLE `condition_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customtype`
--

DROP TABLE IF EXISTS `customtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customtype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customtype`
--

LOCK TABLES `customtype` WRITE;
/*!40000 ALTER TABLE `customtype` DISABLE KEYS */;
INSERT INTO `customtype` VALUES (0,'0'),(12,'Auto-cross'),(6,'Diesel'),(2,'Drag'),(1,'Drift'),(11,'Exotic'),(5,'High Risers'),(4,'Low Rider'),(7,'Offroad'),(14,'Other'),(8,'Performance Trucks'),(13,'Road Course'),(9,'Stanced Vehicles'),(3,'Street'),(10,'Vintage/Classic');
/*!40000 ALTER TABLE `customtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivetype`
--

DROP TABLE IF EXISTS `drivetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivetype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivetype`
--

LOCK TABLES `drivetype` WRITE;
/*!40000 ALTER TABLE `drivetype` DISABLE KEYS */;
INSERT INTO `drivetype` VALUES (0,'0'),(1,'RWD'),(2,'AWD'),(3,'FWD'),(4,'4WD');
/*!40000 ALTER TABLE `drivetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enginecylinders`
--

DROP TABLE IF EXISTS `enginecylinders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enginecylinders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enginecylinders`
--

LOCK TABLES `enginecylinders` WRITE;
/*!40000 ALTER TABLE `enginecylinders` DISABLE KEYS */;
INSERT INTO `enginecylinders` VALUES (0,'0'),(1,'1'),(2,'2'),(3,'4'),(4,'6'),(5,'8'),(6,'10'),(7,'12'),(8,'Electric/Hybrid'),(9,'Other');
/*!40000 ALTER TABLE `enginecylinders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueldelivery`
--

DROP TABLE IF EXISTS `fueldelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueldelivery` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueldelivery`
--

LOCK TABLES `fueldelivery` WRITE;
/*!40000 ALTER TABLE `fueldelivery` DISABLE KEYS */;
INSERT INTO `fueldelivery` VALUES (0,'0'),(1,'Fuel Injected'),(2,'Carborated'),(3,'Other');
/*!40000 ALTER TABLE `fueldelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueltype`
--

DROP TABLE IF EXISTS `fueltype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueltype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueltype`
--

LOCK TABLES `fueltype` WRITE;
/*!40000 ALTER TABLE `fueltype` DISABLE KEYS */;
INSERT INTO `fueltype` VALUES (0,'0'),(1,'E85'),(2,'Pump Gas'),(3,'Race Gas'),(4,'Diesel'),(5,'Other'),(6,'Gas & Oil');
/*!40000 ALTER TABLE `fueltype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listingfeatures`
--

DROP TABLE IF EXISTS `listingfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listingfeatures` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listingfeatures`
--

LOCK TABLES `listingfeatures` WRITE;
/*!40000 ALTER TABLE `listingfeatures` DISABLE KEYS */;
INSERT INTO `listingfeatures` VALUES (0,'0'),(1,'Listings with photos'),(2,'Listing with price');
/*!40000 ALTER TABLE `listingfeatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (0,'0'),(1,'Range feature (under x miles)'),(2,'Unlimited Distance ');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poweradders`
--

DROP TABLE IF EXISTS `poweradders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poweradders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poweradders`
--

LOCK TABLES `poweradders` WRITE;
/*!40000 ALTER TABLE `poweradders` DISABLE KEYS */;
INSERT INTO `poweradders` VALUES (0,'0'),(1,'Supercharger'),(2,'Single Turbo'),(3,' Twin Turbo'),(4,' Nitrous'),(5,' Naturally Aspirated'),(6,' Compound/other');
/*!40000 ALTER TABLE `poweradders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `condition_Id` int(11) DEFAULT NULL,
  `customtypeId` int(11) DEFAULT NULL,
  `drivetypeId` int(11) DEFAULT NULL,
  `enginecylindersId` int(11) DEFAULT NULL,
  `fueldeliveryId` int(11) DEFAULT NULL,
  `fueltypeId` int(11) DEFAULT NULL,
  `listingfeaturesId` int(11) DEFAULT NULL,
  `poweraddersId` int(11) DEFAULT NULL,
  `transmissiontypeId` int(11) DEFAULT NULL,
  `vehicletypeId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `year` varchar(110) DEFAULT NULL,
  `make` varchar(110) DEFAULT NULL,
  `model` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'neymar',NULL,234,3,2,1,2,2,2,1,2,1,1,9,1,2,NULL,NULL,NULL),(6,'coutinho',NULL,234123,3,2,3,2,4,2,3,1,3,2,13,1,2,NULL,NULL,NULL),(7,'Sketch',NULL,1200,3,1,2,3,4,2,3,2,2,1,11,1,2,'2017','C','AVB'),(8,'Bird',NULL,123,3,2,4,3,2,3,3,1,4,1,8,1,1,'11','sd','fhgd'),(9,'Coutaaaaa',NULL,1112,3,1,4,3,2,1,3,1,2,1,3,1,2,'6556','65','456'),(10,'aaaaaaaaaaaa',NULL,22,3,2,11,1,5,1,3,1,3,2,3,1,1,'2','2342','234'),(11,'ert',NULL,234,12,2,4,3,4,2,4,1,3,2,3,1,1,'234','234','234'),(12,'tyu',NULL,67,3,2,4,2,2,2,3,1,2,1,2,2,1,'57','675','567'),(13,'neyyyyyy',NULL,123,3,2,3,4,5,1,2,1,3,1,2,1,2,'52345','2345234','345'),(14,'cccoooummmm',NULL,123567000000,3,2,3,1,3,3,2,1,3,2,11,2,1,'tetemm','etrtertmmm','567511111111'),(15,'erter',NULL,514,12,2,5,3,4,3,3,1,2,2,5,2,1,'534','345','34523'),(16,'iiiiii',NULL,467,12,2,4,3,4,2,4,2,3,1,13,1,2,'74567456','4756','567'),(17,'tytnn',NULL,2345460,3,1,11,3,3,3,1,1,2,2,12,1,2,'523456345','3452344365','3545234564356'),(18,'njr',NULL,1111,3,2,3,2,3,2,2,2,2,2,1,1,1,'1111','1111','1111'),(19,'ccccccccccccccc',NULL,53,14,2,3,2,6,2,4,1,2,2,11,1,2,'678','hfgh','567'),(20,'Lamborghini',NULL,123123,12,2,9,3,7,2,4,2,3,2,11,1,1,'2018','Dhk','2011');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribe_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe`
--

LOCK TABLES `subscribe` WRITE;
/*!40000 ALTER TABLE `subscribe` DISABLE KEYS */;
INSERT INTO `subscribe` VALUES (1,'a@a'),(2,'a@e'),(3,'a@eee'),(4,'kustomklassifieds@gmail.com'),(5,'kustomklassifieds@gmail.com');
/*!40000 ALTER TABLE `subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transmissiontype`
--

DROP TABLE IF EXISTS `transmissiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transmissiontype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transmissiontype`
--

LOCK TABLES `transmissiontype` WRITE;
/*!40000 ALTER TABLE `transmissiontype` DISABLE KEYS */;
INSERT INTO `transmissiontype` VALUES (0,'0'),(1,'Automatic'),(2,'Manual'),(3,'Other');
/*!40000 ALTER TABLE `transmissiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `phone` varchar(110) DEFAULT NULL,
  `mobile` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `sex` varchar(110) DEFAULT NULL,
  `email` varchar(110) NOT NULL,
  `type` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL),(1,'a','11','adasdas','asd','asdasdasdasd','asdffasd','Male','a@a','seller'),(2,'b','11','fsdfasdf','asdf','fasd','fsdafasd','Other','a@g','seller'),(3,'shahad','1234','Shahad','029112126','01711389230','salimullah road','Male','shahad@gmail.com','seller'),(4,'al','11','','','','','Male','aa@ss','seller'),(5,'aaa','11','rtwer','241','5345','6','null','rftgrtw@rwer','seller'),(6,'asdf','11','11','11','11','11','null','wqrw@etw','seller'),(7,'asas','11','11','11','11','11','null','rqwer@yerty','seller'),(8,'aass','11','11','11','11','11','undefined','qwer@qwr','seller'),(9,'asdfghjk','11','11','11','11','11','undefined','wrqerqwe@retwert','seller'),(10,'sha','11','11','11','11','11','undefined','qwer@dwert','seller'),(11,'www','11','11','11','11','','undefined','fas@dfg','seller'),(12,'sadi','11','Sadi Amin','001122','223344','Sylhet','undefined','sadi@gmail.com','seller'),(13,'admin','11','Karguy My Friend','1234567890','0987654321','USA','undefined','karguy@gmail.com',NULL),(14,'dev','11','Al- Amin','12323','35434534','Dhaka','undefined','aa@aaaaa','seller'),(15,'kustomklassifieds','admin','Kustom Klassifieds','0','0','0','undefined','kustomklassifieds@gmail.com','seller');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicletype`
--

DROP TABLE IF EXISTS `vehicletype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicletype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicletype`
--

LOCK TABLES `vehicletype` WRITE;
/*!40000 ALTER TABLE `vehicletype` DISABLE KEYS */;
INSERT INTO `vehicletype` VALUES (0,'0'),(1,'Truck'),(2,'SUV'),(3,'Coupe'),(4,'Sedan'),(5,'Hatchback'),(6,'Motorcycle'),(7,'RTV'),(8,'Crossover'),(9,'Hybrid'),(10,'Van'),(11,'Other'),(12,'Dirt Bikes'),(13,'ATV'),(14,'Boat'),(15,'Snowmobile'),(16,'Sabdrail');
/*!40000 ALTER TABLE `vehicletype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-15 16:05:11
