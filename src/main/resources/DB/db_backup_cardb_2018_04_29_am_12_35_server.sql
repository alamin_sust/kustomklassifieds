-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cardb
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (0,'0'),(1,'Vehicle'),(2,'Parts');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1` int(11) DEFAULT NULL,
  `user2` int(11) DEFAULT NULL,
  `message` varchar(1010) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (1,13,12,'','2018-03-03 12:17:13'),(2,12,3,'','2018-03-03 14:09:22'),(3,12,13,'hii','2018-03-03 14:17:53'),(4,12,13,'Hello KKK','2018-03-03 14:40:38'),(5,12,13,'hii','2018-03-03 14:45:49'),(6,12,3,'kkkkk','2018-03-03 14:46:00'),(7,13,12,'yes','2018-03-03 14:46:33'),(8,13,12,'Lool','2018-03-03 14:53:22'),(9,12,3,'lloo\r\n','2018-03-03 14:57:06'),(10,17,15,'','2018-03-25 22:53:49'),(11,17,15,'hello, you have a car for sale','2018-03-25 22:53:57'),(12,15,NULL,'hello\r\n','2018-04-14 16:06:34'),(13,15,NULL,'','2018-04-14 16:06:34'),(14,15,NULL,'','2018-04-23 03:08:16');
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condition_`
--

DROP TABLE IF EXISTS `condition_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `condition_` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condition_`
--

LOCK TABLES `condition_` WRITE;
/*!40000 ALTER TABLE `condition_` DISABLE KEYS */;
INSERT INTO `condition_` VALUES (0,'0'),(1,'new'),(2,'used');
/*!40000 ALTER TABLE `condition_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customtype`
--

DROP TABLE IF EXISTS `customtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customtype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customtype`
--

LOCK TABLES `customtype` WRITE;
/*!40000 ALTER TABLE `customtype` DISABLE KEYS */;
INSERT INTO `customtype` VALUES (0,'0'),(12,'Auto-cross'),(6,'Diesel'),(2,'Drag'),(1,'Drift'),(11,'Exotic'),(5,'High Risers'),(4,'Low Rider'),(7,'Offroad'),(14,'Other'),(8,'Performance Trucks'),(13,'Road Course'),(9,'Stanced Vehicles'),(3,'Street'),(10,'Vintage/Classic');
/*!40000 ALTER TABLE `customtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivetype`
--

DROP TABLE IF EXISTS `drivetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivetype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivetype`
--

LOCK TABLES `drivetype` WRITE;
/*!40000 ALTER TABLE `drivetype` DISABLE KEYS */;
INSERT INTO `drivetype` VALUES (0,'0'),(1,'RWD'),(2,'AWD'),(3,'FWD'),(4,'4WD');
/*!40000 ALTER TABLE `drivetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enginecylinders`
--

DROP TABLE IF EXISTS `enginecylinders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enginecylinders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enginecylinders`
--

LOCK TABLES `enginecylinders` WRITE;
/*!40000 ALTER TABLE `enginecylinders` DISABLE KEYS */;
INSERT INTO `enginecylinders` VALUES (0,'0'),(1,'1'),(2,'2'),(3,'4'),(4,'6'),(5,'8'),(6,'10'),(7,'12'),(8,'Electric/Hybrid'),(9,'Other');
/*!40000 ALTER TABLE `enginecylinders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueldelivery`
--

DROP TABLE IF EXISTS `fueldelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueldelivery` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueldelivery`
--

LOCK TABLES `fueldelivery` WRITE;
/*!40000 ALTER TABLE `fueldelivery` DISABLE KEYS */;
INSERT INTO `fueldelivery` VALUES (0,'0'),(1,'Fuel Injected'),(2,'Carborated'),(3,'Other');
/*!40000 ALTER TABLE `fueldelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueltype`
--

DROP TABLE IF EXISTS `fueltype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueltype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueltype`
--

LOCK TABLES `fueltype` WRITE;
/*!40000 ALTER TABLE `fueltype` DISABLE KEYS */;
INSERT INTO `fueltype` VALUES (0,'0'),(1,'E85'),(2,'Pump Gas'),(3,'Race Gas'),(4,'Diesel'),(5,'Other'),(6,'Gas & Oil');
/*!40000 ALTER TABLE `fueltype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listingfeatures`
--

DROP TABLE IF EXISTS `listingfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listingfeatures` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listingfeatures`
--

LOCK TABLES `listingfeatures` WRITE;
/*!40000 ALTER TABLE `listingfeatures` DISABLE KEYS */;
INSERT INTO `listingfeatures` VALUES (0,'0'),(1,'Listings with photos'),(2,'Listing with price');
/*!40000 ALTER TABLE `listingfeatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (0,'0'),(1,'Range feature (under x miles)'),(2,'Unlimited Distance ');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poweradders`
--

DROP TABLE IF EXISTS `poweradders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poweradders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poweradders`
--

LOCK TABLES `poweradders` WRITE;
/*!40000 ALTER TABLE `poweradders` DISABLE KEYS */;
INSERT INTO `poweradders` VALUES (0,'0'),(1,'Supercharger'),(2,'Single Turbo'),(3,' Twin Turbo'),(4,' Nitrous'),(5,' Naturally Aspirated'),(6,' Compound/other');
/*!40000 ALTER TABLE `poweradders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `condition_Id` int(11) DEFAULT NULL,
  `customtypeId` int(11) DEFAULT NULL,
  `drivetypeId` int(11) DEFAULT NULL,
  `enginecylindersId` int(11) DEFAULT NULL,
  `fueldeliveryId` int(11) DEFAULT NULL,
  `fueltypeId` int(11) DEFAULT NULL,
  `listingfeaturesId` int(11) DEFAULT NULL,
  `poweraddersId` int(11) DEFAULT NULL,
  `transmissiontypeId` int(11) DEFAULT NULL,
  `vehicletypeId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `year` varchar(110) DEFAULT NULL,
  `make` varchar(110) DEFAULT NULL,
  `model` varchar(110) DEFAULT NULL,
  `is_sold` int(11) DEFAULT '0',
  `status` varchar(110) DEFAULT 'unpaid',
  `zip` varchar(110) DEFAULT NULL,
  `feature` int(11) DEFAULT '1',
  `ip` varchar(110) DEFAULT NULL,
  `latitude` varchar(110) DEFAULT NULL,
  `longitude` varchar(110) DEFAULT NULL,
  `image_count` int(11) DEFAULT '5',
  `details` varchar(4000) DEFAULT 'Have a look at this car! It is awesome',
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `listing_fees` varchar(110) DEFAULT NULL,
  `payment_tx_id` varchar(1010) DEFAULT NULL,
  `package` varchar(110) DEFAULT 'free',
  `token` varchar(110) DEFAULT NULL,
  `place` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL,'free',NULL,NULL),(3,'Big Turbo Lexus IS300',NULL,12500,15,2,3,1,4,1,2,NULL,2,1,4,1,NULL,'2001','Lexus','IS300',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',4,'Clean title, aristo motor swap, it has all new seals new water pump new oil pump all from Toyota the turbo is a s366 with upgraded wheel and thrust washer upgrade, new style Cx racing  manifold, tial 44 wg, turbosmart trumpet BOV, custom full 3inch intercooler piping, mishimoto 3 row intercooler biggest I can fit with the stock bumper, custom 3inch down pipe to full 3inch exhaust all stainless to a greddy sp muffler for a Supra, mishimoto radiator, factory lsd, Megan coilovers, Megan rear lower control arms, walbro 255 high flow pump , Aeromotive fuel pressure regulator, factory aristo trans valve body upgrade, volk gt-7 18-9 front 18-10 rear.  Everything works in the car  including the paddle shift.','2018-03-25 18:09:10','6.73','dkbx2fzp','free',NULL,NULL),(4,'2004 LS1 GTO',NULL,8500,15,2,3,1,5,1,2,NULL,5,1,3,1,NULL,'2004','Pontiac','GTO',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'This car has headers, intake, exhaust, and 243 Heads. 99k miles and clean title in hand. For sale by Luis Alarcon and his email is lalarcon4259@gmail.com','2018-03-25 18:42:11','6.73','6zrsbzqp','free',NULL,NULL),(5,'MINT 2012 Shelby GT500',NULL,40000,15,2,3,1,5,1,2,NULL,1,2,3,1,NULL,'2012','Ford','Shelby GT500',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'','2018-03-25 18:57:50','6.93','6rvemgen','free',NULL,NULL),(7,'2001 Cammed Z06',NULL,17000,15,2,3,1,5,1,2,NULL,5,2,3,1,NULL,'2001','Chevrolet','Corvette',0,'unpaid',NULL,0,'72.213.42.34','41.2061','-96.0451',3,'I have my 2001 Z06 up for sale. It has 93k miles. At about 88k I did all the work to it adding a cam, built the head with all new valve train, added bigger injectors and put headers on the car. Got it tuned on a dunk and made 420hp/409tq I did all the work over a year ago and haven\'t had any issue. Inside of the car is red and black and is in good condition with any rips on the leather. VIDEOS OF THE CAR CAN BE FOUND ON MY INSTAGRAM @slow.roush TEXT ONLY PLEASE.','2018-03-25 21:59:30','6.73',NULL,'free',NULL,NULL),(8,'Tastefully Modded - 2013 BMW 335i M-sport',NULL,25000,15,2,3,1,4,1,1,NULL,2,1,4,1,NULL,'2013','BMW','335i M-Sport',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'2013 BMW 335i M-sport 65000miles 8sp sport automatic with QS2 Fully loaded black leather, shadow line option (black trim and exhaust tips)  $25000 ( willing to negotiate price). Runs on pump91,93 or e85, intake, intercooler, charge pipes, custom exhaust, downpipe, and JB4  So for these cars \"full bolt on\" ~350hp/470tq ish no dyno.     CONTACT: Sopaw2127@yahoo.com or 402-957-3313','2018-03-25 22:38:43','6.73','c4hqv8yn','free',NULL,NULL),(9,'2004 Volvo S60R Stage 3',NULL,6500,15,2,3,2,9,1,1,NULL,2,2,4,1,NULL,'2004','Volvo','S60R',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'Selling my 2004 Volvo S60R.  Overall it\'s in good shape not perfect has some scratches here and there nothing major still looks great. Interior is probably 6/10 needs some love but nothing wrong.  Car is Stage 3 and runs strictly e85 I\'ve never dynoed it cars with the same setup and mods have made around 350+awhp and 420+awtrq and it\'s definitely all of that. Mods list in pictures.  Currently car is having a small issue with inconsistent boost tracked it down to the map sensor car does not over boost and still drives fine I will edit the post once it\'s fixed. 198500 miles.  No trades at this time if I\'ll consider them the post will say so don\'t waist my time or yours. Price is obo','2018-03-25 22:49:29','6.73','7hdpgw53','free',NULL,NULL),(10,'2005 Subaru WRX STI swapped',NULL,17500,15,2,3,2,3,1,1,NULL,2,2,4,1,NULL,'2005','Subaru','WRX',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',14,'2005 Subaru Impreza WRX Premium  Obsidian Black Pearl Premium Package sunroof, heated seats, and heated windshield wipers  Clean Title in hand 125,xxx miles   Car runs and drives like a dream and is very quick and fun to drive. As far as the condition of the car goes the interior is in good shape other than the drivers seat bolster showing wear. On the exterior there are some imperfections like rock chips and scratches as the car is 12 years old and has 125k miles on it so it isn\'t perfect but a lot better shape than 99% of the blob eyes I\'ve seen around town. A lot of quality parts have been used on this build. Also Included with the sale is a load of stock parts I have in storage. All the major work on the car has been done at Russ Garage and he can vouch that nothing on this car has been half assed. I have 4 years worth of receipts for most of the work done to the car as well. The car just had the motor completely gone through and got a new passenger side head from a previous valve issue. Also has a new set of valves in both heads. After the motor was put back together I had it on the dyno for a fresh tune as of 10/17/17 from Graham. Only issue with the car is the rear differential makes a clicking noise under acceleration when the car is cold once it warms up it becomes less noticeable. It doesn\'t affect the way the car performs so I\'ve never messed with it. My asking price is not even close to how much money I have in this car so my price is firm. Not looking for any trades other than a corvette Z06. I\'m sure I\'m missing on some details so if you have any questions feel free to message me. No test drives unless cash in hand.  Performance: Built 2.0L short block from Outfront Motorsports Manley H beam rods Weisco Pistons ARP head studs  Race bearings  Killer B oil baffle and pick up line Blouch dominator 2.5xtr turbo 10cm hot side  ETS front mount intercooler  Tgv deletes  Cobb 4 bar Map sensor  AEM AIT sensor  Tial 38mm wastegate with dump tube ID1300cc injectors Five-O Motor Sports black ops 340lph fuel pump Radium fuel pump hard wire kit 2004 STI brembo brake kit (still 5x100) 2005 STI 6 speed transmission with DCCD Pro controls  R160 3.90 final drive LSD rear differential Competition Racing Stage 3 clutch with lightweight flywheel Grimmspeed electronic boost controller Go Fast Bits adjustable Blow Off Valve  Perrin turbo inlet Perrin cold air intake Grimmspeed external wastegate up pipe  TiTek Downpipe HKS catback exhaust BC Br Coilovers Koyorad aluminum radiator  Mishimoto radiator hoses  Grimmspeed Air Oil separator  Cobb Accessport V3  Tuned on E85 by Graham  Made 451 whp/417 torque   Exterior: Wingless trunk off a 2006 impreza 6k HID headlights  STI side skirts STI corner splitters STI fog light covers STI hood scoop STI V limited replica front lip Front grill badge delete  Perrin front plate delete  18x9.5 +40 Enkei NT03 in Sbc wrapped in Bridgestone potenzas   Interior: ATI tripple gauge clock pod with carbon fiber faceplate  Innovate LC-2 wideband in red Autometer boost and oil pressure gauges in red JDM STI blacked out dash with JDM red hazard button  STI gauge cluster STI Shift boot with shift trim Cobb Red shift knob  Pioneer 7\" touch screen  Viper 5701 Remote Start/Alarm System All interior lights replaced with white LED\'s  Call or text 4o2-35o-3o79','2018-03-25 23:12:40','8.53','9tpr7e9z','free',NULL,NULL),(11,'BUILT Shelby GT500',NULL,40000,15,2,2,1,5,1,1,NULL,1,2,3,1,NULL,'2011','Ford','Shelby GT500',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'Tons of pics and videos on this page and lsx_187r on instagram  Kona Blue 2011 GT500 less than 1k miles on motor and clutch  Mod List  Engine  FGT block  9.6:1  Cryo\'d double keyed crank  Manley rods w/ ARP2000 bolts  ARP hardware  Cometic head gaskets with ARP head studs  JDM superctock cams/springs  Valve job  Bowles cleaned up  Ported lower intake  Billet OPG  Billet crank sprocket  Billet tensioner arms Upgraded tensioners/gears Cryo\'d MMR XHD secondary chains and cryo\'d primary chains 15% OD IW HD balancer  BPS Underdrive alternator pulley  New plugs  BPS pulleys  Thump tensioner  Gates belts  UPR d/s catch can  Metco p/s breather  CFM oil cap  Induction  4.0 Whipple  Variety of pullies  Whipple eliptical t body  1320junkie DD149  PMAS 149mm MAF  Fuel  Fore triple pump return system(reg after rails)  10 feed/8 return  TI auto 274 pumps  ID1700x injectors  E85  Exhaust  ARH 2\" headers with 3.5\" collector  Off road H pipe  Stock cat back  Nitrous(New motor sprayed 1x on dyno with old tvs setup)  NX kit  NO nozzle in the CAI  NOS mini progressive controller  NO purge  NO switch panel  NO heated bottle bracket  Driveline  RXT 1200 clutch  MGW gen II shifter  SS clutch line  Amsoil ATF  DSS aluminum DS  DS loop  Welded tubes on rear end  Tru trac  Moser axles  FRPP girdle  373 gears  Suspension  BMR K member  BMR a arms  BPS radiator support(no sway bar)  Viking crusader coil overs  BPS spring perches  UPR strut mounts  BMR double adj. Rod end LCA  Steeda LCA relos  BMR UCA mount  BMR adj rod end UCA  BMR rod end anti roll bar  Steeda adj panhard bar and brace  Viking Crusader double adj shocks  Viking springs  JPC moly front bumper support  Bumpstop relos  TIG vision chute mount with green stroud chute  Brakes  Aerospace front brakes  JPC line lock  Aerospace rear brakes  SS lines  Amsoil fluid  Cooling  7gal trunk tank  Rule 2000 pump  VMP dual fan/triple pass HE  170 Tstat  Aluminum coolant tank  13/14 IC  13/14 radiator fan  Wheels/tires  Racestar 17x4.5 fronts with 27x4.5x17 hoosiers  Race star sngle champion beadlock 15x10 with 29x10.5Wx15 stiffwall Hoosiers 10ish passes  Stock SVTPP wheels w/ new rear tires  Another pair of Race star 15x10 with 29x10.5Wx15 stiffwall Hoosiers  Electronics/tuning  Remote tuned by Jon Lund II  Lund Ngauge  Raptor shift light  WOT box  Braille LW battery stock location  Interior  6pt wolfe racecraft bolt in Moly cage  Corbreau fx1 seats  Corbreau harnesses  Shrader rear seat delete  Aesthetics  13/14 grills  JLT battery and brake res covers  Various PC and polished underhood  LED light bulbs  Harwood cowl hood w/ quick latches Wrinkle black powdercoated lower intake, valve covers, timing cover, t stat housing, etc.  Have the stock seats, hood, suspension pieces, and much more that will go with it and I am sure I am forgetting other stuff as well.','2018-03-31 15:50:16','6.93','q45hkh9z','free',NULL,NULL),(12,'Built 13b FD RX7',NULL,25000,15,2,3,1,9,1,0,NULL,3,0,0,1,NULL,'1993','Mazda','RX7',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'-13brew with a half bridge port, goopy studs and E&J apex seals.  -haltech platinum 1000 standalone ecu  -spec stage 3+ clutch and ACT monolock collar -s4 tii diff included -walboro 485 fuel pump with a walbro 450 fuel pump -built 2 apex duel fuel pump hanger - all fuel lines are braided steel the fuel is supplied by -10 from the tank downsizing to -8 braided steel lines supplying all fuel rails with -6 return line -rising rate Fuel pressure regulator - turbos are ran with all braided steel also. the oil supply, coolant supply, and coolant return are -6 and the oil return is -10 -custom upper intake manifold with auxiliary fuel rail -720cc primary injectors, id2000 secondaries, and 760cc auxiliary injectors  -xcessive LIM -FFE secondary fuel rail -twin turbonetics t04e t3/t4 hybrid turbo with .83 ar hot sides and will include .63 ar hot sides also.  -twin tial MV-S waste gates that are recirculating to the exhaust. Have option to hook up coolant lines just never did so -HKS type-S bov -custom intake and intercooler piping by Hisetech. Intake is all 3\" and intercooler is 2\" off turbos to intercooler and 3\" from intercooler to throttle body - asp large smic custom made for the twin turbo setup (original owner Kevin owned asp) -electronic and manual boost controller hooked up to car currently so you can change from spring pressure to whatever you want manual controller by the flip of a stealthy switch inside the car. - ronnin speed works intercooler duct -all couplers are 4ply silicone couplers with stainless t-bolt clamps -greddy intake elbow - twin aem dry flow filters mounted under headlight -full custom exhaust 2 1/2\" from turbos to full 3\" cat less exhaust with a borla inline muffler and a supertrapp muffler and tip on the back,  a second axle back that is a borla muffler  - LS1 coils -10.5 mm taylor plug wires -innovative mtx wideband with 8ft lead -60mm autometer liquid filled boost gauge -60mm autometer oil pressure gauge  -rims are ccw classics 17*10 on front 17*12 on rear -tires are nitto NT555 275/40/17s in front and 315/35/17s in back 3k miles on tires. -the suspension uses gab super-r adjustable with eibach springs -the front sway bar is a 2 1/2\" 3 piece design and the back is a 2\" single piece -KTS 4 point rear strut bar -r1 front strut bar -momo steering wheel with quick release -custom sleepy eye headlight -hella projectors -battery mounted in a moroso sealed box to pass IHRA tech -15% tint all around with a stip on front windshield  Bad - secondary Injectors weren\'t staging during the tune which is why it was cutting out. Engine is pulled to clean up the bay and wiring.  This setup is built for 5-600whp. 641-660-7128','2018-04-01 01:41:38','6.93','29dtk3h5','free',NULL,NULL),(13,'2001 Cobra asking 10K',NULL,10000,15,2,3,1,5,1,2,NULL,5,2,3,1,NULL,'2001','Ford','Mustang Cobra',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',7,'Looking to sell my 2001 cobra, I am going to get Married soon and its time for something else. Always Mobil 1 syn at every 3k or sooner and meticulously maintained.  Diablosport tuned MGW shortshifter Steeda full length subframe connectors Cobra anniversary wheels (also have stock cobra wheels) 315/35/17 tires in the back Stoptech powerslot rotors H&R Sport Springs CC plates JLT cold Air intake UPR Xpipe magnaflow mufflers welded onto SLP catback inplace of resonators Real carbon side scoops Touchscreen navigation and GPS Mcloud lightweight flywheel and Mcloud Clutch good to 500HP Braum Elite Diamond racing seats (also have stock cobra seats)  Pictures show broken oil pressure gauge which was replaced also abs light which has been fixed as well.','2018-04-01 01:44:42','7.13','g4evb7hx','free',NULL,NULL),(14,'02 WRX FUllY built with 05 STI drive train',NULL,17000,15,2,3,2,3,1,2,NULL,2,2,4,1,NULL,'2002','Subaru','WRX',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',11,'location Fresno CA  Not sure if i want to sell the whole car or just part it out. mod list is below  i have an 02 WRX with a 2.5L fully built motor and built 2.5 heads.  car was tuned at YIMI sport.   car will pass SMOG runs great with no issues at all, just started a family. CLean title. My first car of 10+ years now must go. i do have the invoice for almost all the parts including the motor build and drive train invoice ect ect.    Willing to sell the car for 17k OBO. if not willing to part it out which im sure will be more that way. the car is on E85  shoot me over prices. willing to ship on buyer expense. No low balling. i know how much a complete STI drive train with an R180 diff goes for on here as well the Wilwood brakes would be added to the price.   the car is still be driven at the moment. i have 30k on the motor and 10k on the drive train.     im sure i have a ton more parts missing on the details here. this car was done right and i didn\'t half ass anything.     Mod list  >>>> 2002 WRX  >>>> stage2 block  >>>> EJ25 with 2.5 heads  >>>> CP Forged Pistons, pins,  >>>> and rings  >>>> ACL Race Series  >>>> Bearings  >>>> STI Forged Connecting  >>>> Rods  >>>> different Crankshaft  >>>> Balanced and Polished  >>>> crank, rods, and pistons  >>>> CNC Precision Bore and  >>>> Hone with torque plates  >>>> Clearanced, Blueprinted,  >>>> and Assembled  >>>> - Motor: fully build EJ25,  >>>> CP pistons, BC titanium valve springs, BC 272 Cams, port and  >>>> polished heads(mild), Killer Bee oil pick up,   04 STI gauge cluster   Subaru NA intake manifold by outfront MS  1000cc ID injectors,  IAG Street AOS   STI ALM front control cars  >>>> DW, IAG fuel rails and IAG lines.  >>>> 300 320lp fuel pump,   >>>> CNT up pipe with a 44mm TIAL EWG, Tial bov , Perrin crank pulley,  >>>> CSF Radiator, Mishimoto Radiator hoses, COBB access port V3, Mishimoto 19row oil cooler  >>>> COBB EBC.  >>>> - Intercooler: ETS FMIC KSTEC 70m SRI ( speed density )   >>>> - Turbo: Cobb 20g  >>>> - Transmission: 2005 STI 5x114.3  >>>> with DCCD PRO with factory wheels that works with the STI cluster , Group N mount, Kartboy Short shifter, Kartboy  >>>> shifter bushing , Kartboy pitch stop mount, COBB shift knob  >>>> whiteline roll center kit  >>>> - Clutch: ACT HD clutch and ACT fly wheel ( they have 4k on them )   >>>> clutch, Goodridge stainless steel clutch line  >>>> SEATS Sparco Chrono, NGR roll bar, NRG 4 point seat belts.  >>>> - Brakes: WILWOOD BBK 6pot front, 4 rear  >>>> - Suspension: KW ClubSports with KW solid top mounts,  ST sway bars, Kartboy F&R  >>>> endlinks , Rear tower brace bars.  >>>> - Exhaust: P&L 3in catback, Invidia down pipe, HKS EL header.     >>>> -dai yoshihara D-12c 18x9 wheels   >>>> -tires FALKEN RT615k 255x40x18  VIS Demon carbon trunk and seibon carbon front lip.','2018-04-01 01:51:09','7.93','ag8tea73','free',NULL,NULL),(16,'Mazda RX8 LS swapped LS1/6 V8',NULL,16000,15,2,3,1,5,1,2,NULL,5,2,3,1,NULL,'2004','Mazda','RX8',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',8,'Decided to sell my pride.\r\n2004 Mazda RX-8 with LS swap.\r\nThe car is a turn key car, not a project. Fully completed \r\n\r\nExterior 9/10\r\nInterior 8/10\r\nClean title (Current registration in California)\r\n122k on chassis, 9k on the swap.\r\n\r\nEngine:\r\nLS1 bottom end(honed and hot tanked)\r\n243 head (polished and ported)\r\nnew valve springs\r\nnew pushrods\r\nLS6 cam\r\nLong tube intake\r\nHooker headers\r\nOil catch can\r\nAll new gaskets(zero leak all around)\r\n\r\nDrivetrain:\r\nTR6060 transmission from a 2015 camaro ss with 37k miles\r\nLS7 clutch\r\nLS7 flywheel\r\nCustom steel driveshaft\r\nCustom differential mount ppf delete\r\nBrand new axles\r\n\r\nOther:\r\nFull 2600$ yellow car wrap done by a professional shop with warranty\r\nTein Flex Z coilovers with only 1000 miles\r\nEnkei rpf1 EBC wheels 17x9\r\nHankook RS-3 tires 245width square set up \r\nPortfield R4-s brake pads\r\nSparco arcantara steering wheel\r\n\r\nThe car runs perfectly. Unique. Good for those who want to have a 4 door Corvette. Selling because I want to try VIP style modification with a Lexus LS460. \r\nAsking 16000, low baller will be ignored. Serious buyers contact me through (858) 281-3391. Located in LA Jolla, California ','2018-04-20 01:59:11','14.99','00','preferred','',NULL),(17,'v8 swap mazda miata will part out / trades',NULL,4500,15,2,2,1,5,1,2,NULL,5,2,3,1,NULL,'1993','Mazda','Miata',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',1,'I\'m putting up my project for sale its close to completion \r\ntrades for 4x4 jeep or diesel 4x4\r\n4500 call or txt for details \r\nengine \r\n302 5.0 HO completely rebuilt and painted \r\naftermarket aluminum heads closed style 64cc and 190cc intake runners \r\n2.02 1pice stainless intake valves and 1.60 exhaust valves \r\nvalve springs are 120lbs close pressure and can accommodate .530 lift\r\nford racing x 303 cam hydraulic roller \r\nnew cam Bearings and crank bearings and pistons rings \r\nfactory cobra bottom end with factor forged pistons 10 to1 \r\ndemon 650 cfm \r\nrpm air gap aluminum intake manifold \r\nford racing tall aluminum \r\n1.6 roller rockers scorpion \r\nhigh flow oil pump \r\nhigh flow water pump \r\nfront sump oil pan 7 quart\r\nshorty exhaust manifold \r\n3inch exhaust pipe with 3 inch bullet mufflers \r\nthe exhaust comes out the front bumper both sides and they are heat wrapped\r\nafter market 17 inch wheels and sport tires \r\naftermarket drilled and slotted performance brakes rotors and pads \r\nrear differential is a spooled rear with gearing 355 factory Miata differential\r\nford t5 world class 5 speed with a short throw shifter \r\nthere is frame stiffeners both sides from boss frog \r\nadjustable coil overs on 3 corners the right front has the factory strut do to defective Meagan racing strut \r\n3 core aluminum radiator with duel electric fans\r\nnew black convertible top installed \r\n\r\n\r\nthe list goes on \r\nwhat needs to be done \r\nthe speedometer needs to be hooked up and differential brace needs to be made and installed for pinion angle \r\na drive shaft to be made and a coolant temp gauge to be wired up \r\nthrottle pedal linkage to be connected and to bleed all the brakes and install the hydraulic clutch slave and bleed it \r\ninstall hood latched and add power steering system fluid to bleed the system \r\nand install a new alternator due to the one installed is not working properly and do a check for loose suspension before test drive the car will need paint and there is two rust small rust spots on the rear comers by the wheel well \r\nford 302 5.0 v8 swap drag race street forged sleeper Mazda 347 331\r\n\r\n(732) 977-6318 - Located in New Jersey','2018-04-20 02:01:44','14.99','00','preferred','',NULL),(18,'2JZ GTE SWAP Lexus is300',NULL,15900,15,2,3,1,4,1,2,NULL,2,1,4,1,NULL,'2003','Lexus','IS300',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',17,'Up for sale is my 2003 Lexus IS300. I bought this car stock about 5 years ago. I took on a project that I am unfortunately unable to finish due to the fact that my wife\'s expecting very soon and priorities are a little different for me now. All work was done by a performance shop in Oceanside. 2JZ GTE VVTI motor was purchased with around 40k miles and hasn\'t been driven since the swap. I do have paper work on a lot of the parts as well. The whole interior was ripped out and replaced with newer seats and door panels. Their is around about 25k into this vehicle on parts and labor alone but I need to move on with this project so my loss could be your gain. This track beast has a base map now which starts and drives all it really needs is a custom tune and fender rolling because tires rub a little bit on bumps. I am in no rush to sell whatsoever so please do not low ball me with offers and waste each others time.\r\n\r\nBuild list:\r\nENGINE/TRANS:\r\n1. 2JZ GTE VVTI Motor\r\n2. Built IS300 Trans Valvebody + Sprag Upgrades\r\n3. Custom driveshaft + adaptive plates for conversion\r\n4. BORG WARNER TURBO T4 66mm conversion\r\n5. 46 MM turbo exhaust manifold + waste gate\r\n6. 1200 CC Bosch injectors\r\n7. Mishimoto Dual core performance radiators \r\n8. External oil transmission cooler\r\n9. TIAL 50mm BOV\r\n10. AEM 380 LPH fuel pump\r\n11. 3\" Aluminum intercooler\r\n12. AEM Fuel band and Wideband gauges\r\n13. 3\" Stainless steel custom exhaust / Fujitsubo exhaust (mk4)\r\n14. New engine and transmission mounts installed\r\n15. AEM V3 standalone computer with dozed motorsports harness adapter\r\n\r\nBRAKES/SUSPENSION:\r\n1. Toyota supra Mk4 Front calipers and rotors (Brand new)\r\n2. Aeromotive regulator + fitting kit installed\r\n3. Stainless steel brake lines front and back\r\n4. Wilwood proportioning valve\r\n5. Teins springs + tokico shocks, front and rear sway bars\r\n6. New FR1 wheels and tires\r\n\r\nLocated in Farmingdale New York. (516) 672-6428','2018-04-20 02:04:20','14.99','00','preferred','',NULL),(19,'Cummins swapped Silverado',NULL,7500,15,2,6,4,4,1,4,NULL,2,2,1,1,NULL,'2000','Chevrolet','Silverado',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',11,'01 Silverado 12v Cummins swap straight axle swap 5speed trans app gauges work 4x4 in the floor truck is daily drive able I drive it everyday.do not have to sell clean title in hand only reason for selling is to help fund school\r\n\r\nLocated in Little Rock Arkansas. (501) 547-2406','2018-04-20 02:06:21','14.99','00','preferred','',NULL),(20,'1994 318ti ls swap',NULL,7500,15,2,3,1,5,1,2,NULL,5,1,3,1,NULL,'1994','BMW','318ti',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',10,'1994 318ti, low mile 4.8 ls swap, 700r4 rebuilt with shift kit. Roll bar, buckets, interior is pretty stripped down not a lot a luxuries other than windows that roll up and down, lowered on dual stage coil overs. Clean swap, lots more, runs and drives awesome lots of fun and gets lots of looks, asking $7500 have over 9k in receipts for parts, would be willing to entertain trade offers for something cool. Race car drift car track car stanced bimmer crawler wheeler hot rod street rod lowered bagged.\r\n\r\nLocated near Seattle. (360) 350-9936','2018-04-20 02:08:25','14.99','00','preferred','',NULL),(21,'1992 Mustang GT Twin Turbo',NULL,14500,15,2,3,1,5,1,2,NULL,3,2,3,1,NULL,'1992','Ford','Mustang',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',18,'MUST SELL ASAP!!! ***PRICE REDUCED***Car was purchased in San Diego in 1992 and driven directly from the dealership to JBA speed shop where 2 German engineers were flown in to assist in the installation of the twin Spearco turbos. Car has 55,811 original miles. Interior is in mint condition with the gray cloth. Power drivers seat which was a rare option. All add ons are documented and inside binder that goes with the car as well as large bin full of new parts and original parts. List of upgrades to car are too long to list but some are higher ratio rear end, professionally lowered suspension, line locking brakes, cobra chrome intake, 60 lb. injectors, and the list goes on and on. Spent $10,000 at ROD\'s Cars here in Kennewick in upgrades. This car was mentioned in Super Ford Magazine back many years ago and that also is in the binder. Hate to sell it but downsizing and needs to go to a good home. I have had a few offers but no one has shown me the money. Please serious buyers only. Last mechanic to look at it said it was the nicest fox body he has seen. A lot more receipts and documentation is available upon request. I had trouble uploading more photos.\r\n\r\nContact Eric @ (419) 271-8280\r\n\r\nLocated in Kennewick, Washington.','2018-04-20 02:42:58','14.99','00','preferred','',NULL),(22,'Nissan Skyline gtr r32',NULL,27000,15,2,3,2,4,1,2,NULL,3,2,3,1,NULL,'1994','Nissab','GTR',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'\'94 Gt-r r32 skyline \r\nClean title \r\nTwin turbo \r\nRb26\r\n50kmiles \r\nOregon title \r\nClean \r\nTons of upgrades \r\nBride seats 400hp tune\r\nIt\'s a beast\r\n(Look in picture)\r\nCash is king... trades ok of value \r\nLifted truck Subaru Sti jeep something cool.\r\n408685399seven or email\r\nSti wrx g37 Tacoma fast rb26 turbo gtr nsx Porsche \r\nTwin turbo awd evo','2018-04-21 04:24:42','14.99','00','preferred','','San Jose CA'),(23,'2008 Chrysler 300c low miles cammed',NULL,18000,15,2,3,1,5,1,2,NULL,5,1,4,1,NULL,'2008','Chrysler','300c',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',11,'2008 Chrysler 300c with 5.7 hemi. Has 73,000 miles. I\'m second owner and purchased it completely stock from Kelly Cadillac in fort Wayne Indiana for $13500 a year and a half ago. I have over $10k in receipts for the parts on the car and everything was purchased new no used parts. I\'ll provide the car fax that was given to me when I purchased it. Never wrecked, never smoked in and been garage kept. Rust free and not a ding or dent anywhere. Inside is very clean with just small amount of wear on driver seat. The car has been tuned by Joshua Schwarts at hhpracing.com and needs nothing you can drive the car anywhere. Has every option but navigation. Has remote start, sunroof, heated seats, Boston acoustic sound package. No trades at all call 41nine...78nine.,,,084four. Here\'s a list of the major things done to it. Not pictured are the summer wheels brand new 18\" black bravado performance wheels with new nitto tires(nt05 in rear and 555 in front). There\'s a few extra things that will go with the car like stock parts if you want them and some parts to put a procharger on the car which was what I was going to do.\r\n\r\n+ tuned by hhp racing\r\n+ custom hhp racing camshaft\r\n+ diablosport intune tuner\r\n+ non mds mopar lifters\r\n+ manley pushrods\r\n+ upgraded valve springs\r\n+ crank is pinned for procharger\r\n+ srt timing set\r\n+ fti 3200 triple plate 9.5\" converter\r\n+ bbk shorty header\r\n+ jba high flow cats\r\n+ flowmaster catback\r\n+ afe stage 2 intake\r\n+ egr delete\r\n+ fore fuel rails with front crossover\r\n+ blue top solenoids in transmission\r\n+ getrag 3.06 differential\r\n+ 392 axles and hubs\r\n+ diablosport catchcan\r\n+ 180 degree thermostat\r\n+ no esp mod\r\n+ aem digital boost gauge installed overhead\r\n+ innovate wideband installed overhead\r\n+ ziebart rust preventive underbody and in all body panels\r\n+ all Windows lightly tinted as well as windshield\r\n+ eibach prokit lowering kit','2018-04-21 04:28:35','14.99','00','preferred','','Defiance, Ohio'),(24,'Mitsubishi 3000GT-VR4 Twin Turbo Low Miles Mint Condition',NULL,14500,15,2,3,2,4,1,2,NULL,3,2,3,1,NULL,'1991','Mitsubishi','3000gt VR4',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',23,'Adult owned 91\' Mitsubishi 3000GT-VR4, Twin Turbo, AWD, AWS coupe with Active Aero in mint condition with only 54K original miles. Car has been metucuously maintained, always stored in climate controlled building no rain or snow. All fluids changed annually with Amsoil full synthetic oils. The Mitsubishi recommended 60K major service work has already been completed. Upgrades include: Momo shifter & boot, Eclipse CD Stereo and EQ, MTX 12\" subs in custom enclosure, MTX 5400 Amp, Viper alarm system, Razo aluminum racing pedals, FET turbo timer, HKS valve controler, Centerforce clutch,..too many options to list everything. Additionally car has NEW Continental performance tires, front & rear brakes, optima gel battery, starter, power steering lines, timing belt, hydraulic belt tensioners, serpentine belt, v-belt, speed sensor, water pump, thermostats, gaskets, all damper pulleys, magnacore wires, iridium plugs, fuel filter, etc.. Over $3700 invested in service work with all OEM parts and less than 5K miles since major service. You will not find a nicer VR4 anywhere that has never been raced and abused with this low of miles. Serious inquiries please call or text to set up an appointment.\r\n300zx, Toyota Supra, Chevy Camaro, Ford Mustang, Corvette, Subaru wrx, GTO','2018-04-21 04:33:53','14.99','00','preferred','','Ohio'),(25,'2010 CAMMED LS3 Silverado',NULL,15000,15,2,8,1,5,1,2,NULL,5,1,1,1,NULL,'2010','Chevrolet','Silverado 1500',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',7,'This is a Cammed 2010 Silverado LS3 swapped (out of 2012 grand sport w/3500 miles) 2WD with a 2013 Denali Interior (Dash,console, woodgrain steering wheel, front and rear seats) I love this truck and drove it very little in the last 2 and half years. Have had a daily so i never really used it. The transmission is about shot its slipping (4L60) was gonna swap it to 4L80 but i never did it. It currently has a 3200 stall and 1st pulls great and shifts into 3rd and overdrive selectively.\r\n\r\nIve invested mid 20k in this truck and willing to sell it because I don\'t drive it very much. Someone will get a great deal on it I don\'t have the space to build it like I did.\r\n\r\nThe chassis has 135,056 miles\r\nThe motor has 11,840 miles on it\r\n\r\n\r\nTruck has these parts;\r\n- 2012 LS3 Grandsport long block (3500 miles)\r\n- Escalde 6.2 intake manifold/stock Throttle body\r\n- Tuned by LMR in Houston TX\r\n- Comp Cam - not sure on specs 112LSA / chops hard\r\n- McGaughy 4/6 Drop\r\n- 4L60 SLIPPING NEEDS REPLACED\r\n- Gears 3:08 Stock\r\n- Shorty headers, no Cats, Mini Mufflers, Dumped before axel\r\n- recon tail lights\r\n- smooth bumper caps in white if you want them\r\n- tinted windshield double 25%, tinted 5% everywhere else\r\n- Camaro SS motor cover in Black \r\n- 20\" OEM Silverados wheels Chrome\r\n- Pioneer DDin heading touch screen 7\" GPS\r\n- All Trim painted metallic Purple\r\n\r\nNO THIS TRUCK WAS NOT IN HURRICANE HARVEY IT WAS NOT IN HOUSTON AT THE TIME\r\n\r\nTRUCK HAS CLEAN TITLE AS WELL\r\n\r\n281-5 zero zero-0 one five 5\r\nText or Call','2018-04-21 04:39:39','14.99','00','preferred','','Houston, Texas'),(26,'88 Toyota 4Runner rock crawler',NULL,9500,15,2,7,4,4,1,2,NULL,5,2,2,1,NULL,'1988','Toyota','4Runner',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',7,'88 4Runner \r\n230k on body\r\n12k on rebuilt 3.0l v6\r\nExhaust system and headers\r\nBrand new red top battery \r\n\r\nBilstein shocks\r\nFord 9\" rear end. locked\r\nDana 44 in the front. Locked\r\n5.38 gears\r\nSingle transfer case\r\n5speed\r\n\r\nNew 37x14.50 toyo mt\'s\r\nBrand new spare\r\nTwo brand new Sparco seats\r\nFront and rear bumpers and side rails\r\nNew front fenders\r\n\r\n$9500 OBO\r\nNO TRADES! \r\nCall or txt 970-901-2726','2018-04-22 21:37:47','14.99','00','preferred','','Crested Butte, Colorado'),(27,'1996 jeep crawler w/1 ton / 6.0l/ clone-v',NULL,22500,15,2,7,4,5,1,2,NULL,5,1,2,1,NULL,'1996','Jeep','Wrangler',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',2,'I have a fully built 96 wrangler for sale possible trade....\r\n\r\nIt\'s a 96 wrangler chassis and body\r\n\r\n2006 6.0 vortex LQ9 40k runs perfectly no leaks like new.\r\n\r\nTripple core aluminium radiator \r\n\r\nElectric cooling fans\r\n\r\nCold air intake routed under the dash along with the ecu and trans cooler\r\n\r\nFull HD build on turbo 400 trans with B&M ratchet shifter\r\nAnd big trans cooler with fan\r\n\r\nHas tripplle stick tranfercases np205 dual stick with a clone-v underdrive\r\n\r\n4\' radflow shocks and pack coil springs all around with remote resovors \r\n\r\nHigh pinion Dana 60 front with 538 trail gear gears and true track lockers\r\n\r\nRear also Dana 60 with 538 trail gear gears and true track lockers \r\n\r\nFully functional psc dual ram hydro steer with six gun hytseer\r\n\r\nSitting on 47\'s and 20\'s.\r\n\r\nPainless wiring kit \r\n\r\n3 link\r\n\r\n4 link\r\n\r\nMastercraft seats \r\n\r\nFuel cell\r\n\r\nHigh flow fuel pump\r\n\r\nAll automiter gauges\r\n\r\n6 safe switch, switch bank\r\n\r\nLocking glove box\r\n\r\nAll custom aluminium dash and center consol\r\n\r\nLight weight aluminium steering wheel \r\n\r\nfully caged front to rear...\r\n\r\nMain cage is on bushings so it\'s fully removable \r\n\r\n90%of the work is professional not back yard build\r\n\r\n27k spent at xxx Traction \r\nAnd a bunch more spent at pac fab \r\nBoth very reputable shops\r\nOver 50k Invested \r\n\r\nEverything is like new never seen the trail...\r\n\r\nCall or text with questions and offers thanks jon @831+431&#65533;3890','2018-04-22 21:46:17','14.99','00','preferred','','Boulder Creek, California '),(28,'Bagged Nissan Frontier',NULL,7300,15,2,4,1,3,1,2,NULL,5,2,1,1,NULL,'2003','Nissan','Frontier',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'I have a 2003 bagged Nissan Frontier for sale. Airlift bag kit custom notched in the back. It lays the frame on the ground. 2 compressors and 2 air tanks. Brand new compressor replaced last week by wimpy customs! Front,back, side to side. Built by wcdfab check out the build online at wcdfab.com. 9 switch controller! 5 speed standard. 22 inch lexani rims the front passenger side is missing the center cap and your normal wear on the wheels. Good tires. Bads are the a/c just went out for some reason. Custom magnaflow side exit exhaust exiting under right behind the driver door but not very load at all!! 4 cyl. Probably a few other things I\'m forgetting but feel free to call or text anytime 361-537- five eight two four! 7300 obo the truck has a lot of money into it and a cool truck for sure if you\'re into mini trucks!!!','2018-04-22 22:15:03','14.99','00','preferred','','Odessa, Texas'),(29,'Built 2009 STI',NULL,19300,15,2,3,2,3,1,2,NULL,2,2,5,1,NULL,'2009','Subaru','WRX STI',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',7,'Price is OBO!!!\r\nUp for sale or trade for a nice WRX or STI plus cash on your end! I\'ve got a 2009 STI has 2011 front bumper Reconstructed Title, no frame damage (triple checked with PRE) and no deployed airbags and no rust. 105k miles Body is 7/10 not perfect, no major damage and interior is 9/10. Mechanically is where it shines! \r\nBuild was done and Dyno\'d at PRE in September 2016 Still Have the Receipts!!!\r\n350whp on conservative Speed Density tune\r\nEngine:\r\nFull custom rotated Garrett 3071R Turbo\r\nFront mount intercooler\r\nManley forged pistons\r\nSTI forged rods\r\nACL rod bearings\r\nACL turbo main\r\nAPR headstuds\r\nHeads resurfaced\r\nGates timing belt\r\nPRE airpump delete\r\nTGV deletes\r\n1000cc Deatschwerks injectors\r\n255lph Deatschwerks Fuel pump\r\nKiller bee oil pan\r\nPerrin Air oil separator\r\nPerrin Cold air intake\r\nMishimoto Radiator\r\nHKS bov\r\n\r\nExhaust:\r\nUEL headers\r\nFull invidia N1 race turboback (very loud) comes with silencer\r\n\r\nSuspension:\r\nPerrin strut bar\r\nPerrin rear sway bar\r\nKYB Struts\r\nRacecomp springs\r\n2011 STI wheels\r\n\r\nInterior:\r\nKartboy short shifter\r\nProsport AFR gauge and Boost gauge \r\nRed underdash LEDs\r\n\r\nKey words: Subaru Impreza WRX STI built forged turbo Garrett 6 speed dyno PRE','2018-04-22 22:28:34','14.99','00','preferred','','Wilsonville, Oregon'),(30,'2004 Jetta Gli Bagged',NULL,7500,15,2,4,3,3,1,2,NULL,2,2,4,1,NULL,'2004','Volkswagon','Jetta GLI',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',10,'2004 Jetta gli 1.8t\r\n\r\nBagged on Airlift V2 setup\r\nSlam series bags new bags installed in February, have spares for rear. All new lines. System will hold air for a week\r\n\r\n3SDM wheels\r\n\r\nR32 Seats front and rear\r\n\r\nNew parts from previous owner\r\n120 amp alternator\r\nAlternator harness from Vw\r\nBattery fuse box\r\nBattery\r\nVarious coolant pipes and hoses\r\nTiming belt kit\r\nCrossed drilled rotors pads front and rear\r\nCoolant temp sensor\r\nPlugs\r\nBelt tensioner\r\nBelt\r\nCabin filter\r\nFuel pump\r\nFuel pump relay\r\nTie rods\r\nStrut bearings\r\nTransmission flush with mt90\r\nBall joints\r\nTiming belt and water pump\r\n\r\nPerformance parts\r\nEurojet race intercooler\r\nEurosport intake\r\n007 forge diverter valve\r\nTechtonics Magnaflow muffler no cat with 02 spacer\r\nDown pipe\r\nEnergy bushing dog bone\r\nUnitronic stage 2+ tune 93 octane \r\nSouthbend stage 2 clutch\r\nPioneer double din\r\nEuro switch\r\nBoost vacuum gauge\r\nAir fuel gauge not hooked up needs to be wired\r\nWindows are tinted\r\n\r\n\r\nJust bought the car back in August but when I tried to get it inspected in MA it failed, they told me I need a cat and to take the tune off and then reload it after it is inspected. I don\'t have the time or money right now to do so. It may pass without any changes in other states. It has a current Rhode Island sticker on it when I bought it from April.\r\n\r\nThe reason the price dropped is because the car stalled on me twice but I do not know how to fix it \r\n\r\nIn the month I owned it I replaced the ebrake cable and caliper.\r\n\r\nI can provide more pictures if you want just text me and ask\r\n\r\nCAR IS BACK ON ROAD\r\nSTILL UNINSPECTED\r\nCAR HAS BEEN RUNNING WELL\r\n$7500 obo Throw me an offer worst I can say is no\r\n\r\n781-853-8957','2018-04-22 23:36:05','14.99','00','preferred','','Boston'),(31,'KustomKlassifieds',NULL,1,18,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2002','Ford','',0,'unpaid',NULL,0,'72.213.42.34','41.2061','-96.0451',2,'','2018-04-22 23:40:40','8.99',NULL,'premium','',''),(32,'',NULL,1,18,2,0,0,0,0,0,NULL,0,0,0,1,NULL,'1','1','',0,'unpaid',NULL,0,'72.213.42.34','41.2061','-96.0451',1,'','2018-04-22 23:54:40','8.99',NULL,'premium','',''),(33,'Mazda Miata Supercharged',NULL,5900,15,2,3,1,3,1,2,NULL,1,2,3,1,NULL,'1992','Mazda','Miata',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',16,'Spring is coming and what better way to enjoy the nice weather than to drive a supercharged miata! \r\nFor sale is my 1992 Mazda Miata which has a Jackson Racing (Sebring) Supercharger. \r\n\r\nThe Good: \r\nNew ACT clutch (Also had flywheel resurfaced) \r\nNew Tires\r\nHard Dog Roll Bar\r\nStiffer Sway Bars\r\nLowered 2 inches on Koni shocks with adjustable stiffness \r\nFull Jackson Racing Exhaust with header\r\nCold AC\r\nGood Heat\r\nPower Windows \r\nThe roof has no leaks \r\nWorking factory radio\r\nAlways Garage Kept!\r\nExcellent maintenance \r\nOriginal BBS Wheels \r\n90,000 original miles \r\n\r\nThe Bad: \r\nMinor rust on rocker panels \r\nBack window could use some attention\r\nSome minor dings and scratches \r\n\r\nThis car needs nothing and is mechanically sound. Plus it\'s faster than all the other miatas without superchargers! I have this priced to sell, I\'m motivated but not looking for any trades. \r\nText message or phone call are good ways to get ahold of me. I\'ll also try to answer email. \r\nThanks for looking. \r\n\r\nLocated in Washington, Pennsylvania \r\n\r\n(724) 705-2267','2018-04-23 02:06:44','14.99','00','preferred','','Washington, Pennsylvania'),(34,'Supercharged Jeep',NULL,7500,15,2,7,4,4,1,2,NULL,1,1,2,1,NULL,'1991','Jeep','',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'1991 rock crawler has a V6 supercharged motor 456 gears in front and back four seater very nice jeep.or trade for something with the same value please send pictures rat rods hot rods Pro streets or daily drivers\r\n\r\n(734) 752-8974','2018-04-23 02:10:01','14.99','00','preferred','','Downriver, Michigan '),(35,'2000 Chevy truck 620hp',NULL,20000,15,2,8,1,5,1,0,NULL,1,1,1,1,NULL,'2000','Chevrolet','Silverado',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',18,'This is what you would call a sleeper it has over 620 horse power in this 6.2 ls engine bored over ls3 valves ported and polished heads it has a procharger aluminum fuel rail kit it has a blower on it all kinds of goodies I have a whole notebook of recents with this bad boy I have gotten plenty of speeding tickets in it it has air ride system so it\'s a lot of fun to drive this isn\'t for your 16 year old kid first truck it\'s super fast and super clean needs nothing but a good driver never raced older man built this truck and drove it around and to car shows won\'t be disappointed clear title tagged and inspected I am keeping the plates when it sells call or text with questions 903 271 seven six zero zero cash only no pay pal might deliver for a reasonable fee no trades you cant build this truck for what I\'m asking thanks 20 grand obo','2018-04-23 02:15:59','14.99','00','preferred','','Austin, Texas'),(36,'2001 Mustang Cobra 650hp',NULL,18750,15,2,3,1,5,1,0,NULL,1,2,3,1,NULL,'2001','Ford','Mustang Cobra',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',4,'2001 COBRA SVT\r\nThis is not a ragged racecar and definitely not a toy, this is a showroom condition vehicle and an extremely powerful car.\r\nOnly 68k original miles\r\nOnly 11k miles on the upgrades.\r\nManual Transmission\r\nPioneer Touchscreen Double Din DVD/Radio Unit\r\nPhantom Gauges\r\nTinted Windows\r\nPrewired for subwoofers with amplifier included in trunk\r\nMGW Racing Cobra Shift Knob\r\nModern Billet Polished Billet E-Brake Handle, A/C Knobs, Headlight Knob.\r\nLightweight Cowl Hood\r\nCervini\'s Stalker Front Bumper\r\nSaleen Rear Spoiler\r\nStage II Procharger\r\nPerformance Alternator\r\nFlowmaster exhaust\r\nLong Tube Headers\r\nFR500 Wheels\r\nLike new tires \r\nSimply a 650hp Cobra that runs and drives incredibly well and gets decent gas mileage with normal driving.\r\nThis car has been adult owned and driven since the day it was purchased, all services and upgrades have been done professionally, no expense has ever been spared keeping the car in peak condition. Asking $18,750 May consider trades for something that is more suitable for a family vehicle. No junk or projects. Call or text 901-290-1471 if you are interested.','2018-04-23 02:18:47','14.99','00','preferred','','Memphis, Tennessee '),(37,'2013 Mustang V6 Procharger',NULL,18000,15,2,3,1,4,1,0,NULL,1,1,3,1,NULL,'2013','Ford','Mustang',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'Silver 2013 Mustang V6 Procharger.\r\nPushing 430hp to rear wheels.\r\n52,500 miles 3rd owner. All maintenance and receipts for what has been done to it. Shelby Exhaust, Cobra seats. Rear wheel racing tires. I have the Dyno sheets too.\r\n\r\n(303) 261-2147','2018-04-23 02:21:56','14.99','00','preferred','','Franktown, Colorado'),(38,'2005 Pontiac GTO 530hp PROCHARGED + MORE',NULL,20000,15,2,3,1,5,1,2,NULL,1,2,3,1,NULL,'2005','Pontiac','GTO',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',1,'This post is just testing the waters and seeing what kind of offers come my way. 2005 GTO PBM M6 \r\n97k miles \r\n6k miles on complete D1SC Procharger kit (D1SC capable of 1,000hp) \r\nWhen offering, keep in mind that over 15k has been put into this car alone, not that I expect to get near that in return but some people like to lowball.\r\n\r\nUpgrades (I\'m sure I\'m forgetting some things):\r\n- D1 procharger w/ upgraded fuel system\r\n- Pacesetter ceramic coated long tube headers\r\n- Catless Borla exhaust W/ SLP Loudmouth Resonators.\r\n- Tick Adjustable Master Cylinder \r\n- Monster LT1-S Twin Disk Clutch\r\n- Hinson Short Throw Shifter\r\n- Eaton Trutrac Differential \r\n- GForce Engineering level 2 outer axle stubs\r\n- Anti-Wheel hop axles \r\n- Airlift drag bags\r\n- New Drilled/Slotted rotors & pads\r\n-Built tranny : billet keys 1-4, new 2nd gear, new synchros, new pads\r\nBuilt fuel system - new aftermarket pump, 60lb injectors \r\n- brand new radiator \r\n- Oil changes every 3k miles religiously \r\n- color changing interior LEDs\r\n- Double DIN Touchscreen head unit with premium sound & backup cam capable \r\n\r\nI will ignore low balls and unreasonable offers, just to not waste your time or mine. Feel free to text anytime 913-954-8344\r\n\r\nlsx civic si integra sti wrx ls2 ls1 Camaro corvette mustang supercharge SS manual 6 speed g35 g37 Infiniti','2018-04-23 02:35:14','14.99','00','preferred','','Kansas City'),(39,'1000+ HP Procharged street legal Monte Carlo',NULL,27900,15,2,2,1,5,1,3,NULL,1,1,3,1,NULL,'1972','Chevrolet ','Monte Carlo',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',24,'Motor specs:\r\n\r\n1193 HP/973 ft lbs Torque on Flywheel Dyno at 21 lb of boost\r\nARP Studded 540 cu in World Merlin II BBC de-stroked to 480 cu in\r\nBalanced and blueprinted \r\nEagle 4340 forged crank\r\n4.5035 in bore\r\n3.766 in stroke\r\n6.385 in Manley Sportsman rods with ARP 2000 bolts\r\n9.17:1 custom Diamond forged pistons\r\nTotal Seal TNT rings\r\nCustom Cam Motion Solid roller cam (as of 7/27/17)\r\nJP Performance Timing set\r\nF2 Procharger\r\nCanfield CNC Aluminum BBC heads with SDCE port\r\n1.7 Harland Sharp shaft rockers\r\n3/8\" Manley push rods\r\n2.30 Manley intake valves\r\n1.925 Manley exhaust valves\r\nIsky springs (as of 7/27/17)\r\nAccell Gen7 EFi system with 120 lb injectors and pressurized AFi Methanol Injection \r\nCustom AFi sheet metal upper plenum with cast cross ram lower intake\r\nIce 7000E Ignition and MSD Crank trigger system\r\nAccell Distributor\r\nMororso Plug wires\r\nATI Super Damper\r\n\r\n\r\nhttps://youtu.be/8qJr_IkTklc\r\nMore info: https://worcester.craigslist.org/cto/d/1000-hp-procharged-street/6544959740.html','2018-04-23 02:37:54','14.99','00','preferred','','Uxbridge, Massachusetts '),(40,'2002 Subaru WRX (STi JDM Version 8 Drivetrain + JDM Front End)',NULL,9500,15,2,3,2,3,1,2,NULL,2,2,4,1,NULL,'2002','Subaru','WRX',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',30,'I have owned this car for 11 years but its time to move on as I have too many vehicles in my stable at the moment. During my ownership, I have only used either Mobil 1 or Rotella synthetic oil in the vehicle changed every 3,000 miles. The drivetrain was replaced right around 150,000 miles with a JDM STi Version 8 complete swap (engine, transmission, front and rear diffs, hubs, brakes, etc - I have receipts for the kit) and the current odometer reading is at 165,000 miles. These engines are very desirable due to their durability, twin scroll turbocharger setup and stronger internals. The car is now a 6 speed short ratio STi transmission which replaced the factory 5 speed that originally came in the car. The car runs and drives great. The primary value in this car is the drivetrain itself. The Version 8 STi drivetrain kits are valued at $7,000 alone. This car is very unique also in that it features the fairly rare BBS 4x100 golds that are difficult to find (only made for 1 year). The JDM 2002-2003 STi front end is also a nice touch as it really gives the car a facelift especially with the OEM Subaru HID projector headlights and housings.\r\n\r\nI am selling the car as is with no warranty or any type of guarantees. All sales are final.\r\n\r\nI will not accept trades nor do I accept payments.\r\n\r\nVehicle specifications:\r\n\r\n- 2002 Subaru Impreza WRX\r\n- World Rally Blue Paint Color\r\n- 165,000 Miles on Chassis\r\n- ~90,000 Miles on Drivetrain\r\n- Version 8 JDM STi EJ207 Engine (twin scroll turbo/manifold, 2.0L, etc.)\r\n- Version 8 JDM STi 6-Speed Transmission\r\n- Version 8 JDM STi R180 Differential, Axles, Hubs\r\n- Version 8 JDM STi Brembo Brakes \r\n- Version 8 JDM STi Front Sway Bar\r\n- Version 8 JDM STi ECU (Tuned by Pullz-On Tuning)\r\n- HKS Carbon Fiber Cat Back Exhaust \r\n- STi Group N Transmission and Engine Bushings\r\n- 4x100 Subaru OEM STi BBS Gold Wheels\r\n- Toyo Proxes Tires\r\n- IA Performance EJ207 AVCS Harness \r\n- Version 7 JDM STi Front Bumper\r\n- Version 7 JDM STi OEM Projector Headlights and Ballasts\r\n- Version 7 Grille\r\n- Version 7 Hood\r\n- Prodrive Front Lip\r\n- STi Full Interior\r\n- Clarion Head Unit\r\n- Image Dynamics Front Speakers\r\n- Autometer Boost and Oil Pressure Gauges in Pillar Pod\r\n- Viper Alarm System (2 Key Fobs Included)\r\n- Tactrix Cable Included\r\n- Mishimoto Aluminum Radiator\r\n\r\nKnown Issues\r\n\r\n- Minor rust in passenger quarter panel\r\n- Small dent in trunk\r\n- Air conditioning needs recharged\r\n- Front Windshield is cracked at bottom\r\n- Airbag Light is on due to driver seat airbag being unplugged\r\n- ABS Light is on due to driver rear ABS sensor failure\r\n\r\n402-957-5478','2018-04-23 02:47:16','14.99','00','preferred','','Omaha, Nebraska'),(41,'93 rx7 FD rotary',NULL,19000,15,2,3,1,9,1,0,NULL,2,2,3,1,NULL,'1993','Mazda','RX7',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',24,'93 rx7 FD, 5 speed, touring, red ext/black interior,85k miles\r\nRuns strong, 500 miles on rebuilt rotary. Here is a list of mods:\r\nSingle turbo conversion\r\nMegan coil overs\r\nTurbo timer\r\nLarge radiator\r\nRotary works compression elbow\r\nRotary works throttle body\r\nLarge intercooler\r\nInjectors\r\nHaltech management system\r\nRx8 seats\r\nCustom wheels\r\nStreet ported rotary by defined auto\r\nR1 splitter\r\nSide skirts & rear valance\r\nGReddy BOV\r\n\r\ntrade for nice vehicle with KBB over $20k\r\n\r\nCar could use paint job. Came out of California and has acid rain damage.rebuilt title due to theft recovery.\r\n\r\nNo scammers, I don\'t need help selling, if this listing is online then it\'s still for sale! Serious buyers only.\r\n\r\n (317) 797-6221','2018-04-26 03:44:48','14.99','00','preferred','','Anderson, Indiana'),(42,'2005 PONTIAC GTO TWIN TURBOS',NULL,14000,15,2,2,1,5,1,0,NULL,3,2,3,1,NULL,'2005','Pontiac','GTO',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',12,'2005 gto 6.0 58k miles m6 little John turbo cam push rods and head studs C7 gaskets hooker turbo headers twin turbos 66mm on3s Holley high rise intake Holley efi dominator system -12 fuel lines Holley 1200 fuel pump timing chain and timing chain cover oil block plate for turbo oil lines water meth injected built tranny stage 4 monster clutch drag bags solid aluminum drive shaft battery relocate kit weld wheels Mickey tires 6 inch cowl hood 16 inch pusher fan boost and bov gauges if u don\'t use efi control screen car still needs alittle tuning and little things to be hitting the streets over 22k invested will take 14k or trade for other cars fast Camaro ls1 ls2 ls3 trans am mustang cobra lifted trucks diesels or wrangler or drag cars (573) 579-7353\r\nanytime','2018-04-26 03:49:04','14.99','00','preferred','','Dongola, Illinois '),(43,'1jz 240sx TRADES WELCOME',NULL,10000,15,2,1,1,4,1,0,NULL,2,2,3,1,NULL,'1989','Nissan','240sx',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',21,'Hey guys. Maybe toying with selling my 240sx. Looking to trade for something Awd like a wrx, or sti/evo with cash/trades on my end. \r\n\r\nSo, to make this quick, I\'ll add the main mods. \r\n\r\nFull 1jz swap\r\nWiring specialties engine harness\r\nBuilt w58 transmission (Marlin Crawler Gears and Syncros)\r\nTube front end done by \r\nRotated, single turbo set up on 7.3l power stroke turbo\r\nHood dump from turbo\r\nApexi Neo controller\r\nBoost Gauge\r\nCustom stitch Panel\r\nCustom center Bezel\r\nDash Mat\r\nFull front and rear suspension done\r\nExtended LCA, aftermarket tie rods, tension arms, etc front suspension \r\nCoilovers (yellow, not sure of brand)\r\nWide 18 inch wheels, very aggressive with good tires\r\nFront bash bar\r\nFront mount IC with BOV\r\nKoyo radiator\r\nCustom Wiring specialties wiring harness\r\nFront and rear wide body/over fenders\r\ntype x/180sx front bumper\r\nNRG Neochrome/black wheel with Quick Release. \r\n\r\n\r\nThere\'s a lottttt more to list, but for the sake of attention, here\'s the main parts. \r\n\r\n\r\nCons with the car:\r\nIt\'s pretty much a track car. Gutted interior. \r\nNeeds an alternator, car died on me the other day and runs perfect when the battery is charged, but it isn\'t charging. \r\nCould use turn signals wired up, has brake lights.\r\nCould use a bolt thru the steering column shaft, as the one that is in it sheered off. Will be trying to fix this ASAP. \r\nNeeds to have the temp sensor hooked up, I have it just needs installed.\r\nRadiator fans are only on when the main ignition switch is off. Needs to be rewired.\r\nNo windshield wipers. Just needs the motor and arms though! \r\n\r\n\r\nThe car is located in Pullman. I\'m playing with the idea of selling it, not 100% on it. Really just need a daily driver right now, more than anything.\r\n\r\nCar is in my name and registered. Feel free to message me. Thanks (425) 830-9609','2018-04-26 03:54:34','14.99','00','preferred','','Pullman, Washington'),(44,'2004 Cadillac CTS-V modded street, track, auto x car',NULL,14500,15,2,12,1,5,1,2,NULL,5,2,4,1,NULL,'2004','Cadillac','CTSV',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',12,'This is a 2004 Cadillac CTS-v with 126xxx miles. Has the LS6 Z06 Corvette motor with 6 speed manual transmission. It has the black leather and suede interior. Comes with 3 sets of wheels, 2 factory sets (1 powder coated gunmetal grey with Michelin Pilot Sports, 1 set black painted stock wheels with 255 bridgestone re71r, and 1 set of Black Team Dynamics wheels with 275 RE71Rs) Mods include: LS7 Clutch and flywheel, Creative Steel short throw shifter, RPM transmissions rebuilt and upgraded T56 (upgraded shift forks, syncros, keys) Shaft Masters aluminum driveshaft, Russel Braided steel brake flex lines, Hawk HP Plus brake pads, K&N cold air intake, Pace Setter long tube headers with cat delete and flowmaster mufflers, BMR anti wheel hop kit with control arms, toe arms, subframe bushings and pinion brace, aluminum radiator, Belltech KW V3 coilovers with upgraded 624 lbs rear springs, Hotchkiss Sway bars, tuned. Has a Leroy Engineering hidden trailer hitch installed to pull a race tire trailer. (if you subscribe to Grassroots Motorsports Magazine youll recognize this car from the Leroy Engineering add in the Oct 2017 issue) It has a new fuel pump and ignition switch via recent GM recalls. Its a great track day or auto x car and is still comfortable to drive. I have been autocrossing this for the past couple years and it is very competitive. Built it to do track days and as yet have not done one because I got hooked on autox. Comes with the factory exhaust manifolds, cats, factory shocks and springs, factory shifter, sway bars, suspension components and brake hoses. $14500 firm. Mileage will go up as I drive it occasionally until it is sold. I will be removing many of the mods that I still have the stock parts for and returning it to stock at the end of this summer if it doesnt sell. Would trade up or down for a C5 Corvette FRC or Z06, C6 Z06, or 16+ Camaro SS.\r\n(440) 339-9662','2018-04-27 03:36:18','14.99','00','preferred','','Painesville, Ohio'),(45,'1999 Mazda Miata w/ RB25DET Neo swap',NULL,17995,15,2,3,1,4,1,0,NULL,2,2,3,1,NULL,'1999','Mazda','Miata',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'I have a 1999 black Mazda Miata with a RB25DET Neo swap in it. \r\nOne of a kind car\r\nMotor is a Nissan RB25DET Neo, just completly rebuild last year\r\nMotor sits in a modded Miata front sub frame wideband The front brakes were upgraded to the VW Carrodo Big rotors using Bembo blanks and Sport brake upgrade in the rear. Everything is using stainless braided brake lines and the clutch line is also stainless braided. Suspension was upgraded to Rokkor Coilovers. Tires are Hankook Ventus RS3 225/45/15 on 15x8 Konig Wideopen Bronze wheels. There is a Garage Star wiper cowl cover installed under the hood along this beautiful engine swap.\r\nThe engine is a RB25DET Neo. Originally had approx 30k miles before installed. The engine wiring harness was built by a aerospace engineer (previous owner\'s friend) using Reychem shrink a very cool engine bulkhead connector. One twist and the engine can be pulled from the chassis, no running around and disconnecting multi plugs and such. The turbo is a Borg Warner S258 that has a clipped exhaust wheel and knife edge exhaust housing. The downpipe is 304 stainless TIG welded with heat wrap and exhaust is 3\" with a vibrant resonator. There is a Greddy Type S BOV and Tail 38mm wastegate with dump tube. The oil pan and pick up tube is custom tig welded. We got 1000cc injectors from Injector Dynamics with a aftermarket fuel rail using a Aeromotive Fuel pressure regulator. To feed the engine, there is a Denso 300LPH fuel pump in the tank. The crank and cam sensor are custom make from a Volvo and some clever welding. The ECU is a Megasquirt 3 w/ expander board. Intake manifold is from ISIS with a 90 mm q45 Throttle Body and exhaust manifold is OBX. The FMIC is from XS power. There is a Custom Oil cooler and filter relocation and big aluminum radiator with a Garage Star radiator cooling cover installed. To top it off, the air filter is KHS mushroom filter. The coils are GM LS1 truck coils with heatsinks and MSD wires and a custom made coil mount. \r\n\r\nThe drivetrain consists of a RB25DET transmission with the larger bellhousing. The driveshaft is custom and has brand new V8 Roadster axles. There is a 6-puck unsprung ceramic clutch and aluminum flywheel. The rear is a Ford Mustang Cobra IRS 8.8 aluminum housing that was rebuilt last year and has 3.73 gears from Ford Motorsports. \r\n\r\nThe interior has stock Mazdaspeed black leather seats, Harddog Double Diagonal roll bar with fire extinguisher a Nardi steering wheel, Pioneer head unit, HKS EGT and Boost gauge, Autometer Oil pressure and Water Temp, HKS turbo timer, Greddy boost controller and, NGK/NTK wideband.\r\n\r\nAsking 17995.00 or best offer\r\nCall 407 six two zero 0648','2018-04-27 03:40:05','14.99','00','preferred','','Oviedo, Florida'),(46,'2004 Mazda MX-5 Miata Mazdaspeed',NULL,9000,15,2,3,1,3,1,2,NULL,2,2,3,1,NULL,'2004','Mazda','Miata',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',7,'Car has stock turbo and engine is stock. Great running car and has never giving me any trouble. Modifications are small and I do have the stock shock tower brace and carpet for the truck and spare. Mods are a custom fiberglass hood with vents, a Muzi 55mm Aluminum radiator (stock one started leaking), Boss Frog Double Hoop Roll Bar and door bars (has gone out of business), and a aftermarket LED third brake light. There is also a metal cowl cover install to hide the wiper arms and relocate the washer fluid tank. I also installed the Mazdaspeed J-spec Wing from Japan. This wing was a factory option from Mazdaspeed Japan and sold in Japan only. I found one on a Japan auction site and had it imported and installed. This is a option you rarely see in the US. Makes this car a bit special. There was only one other Miata in the Central Florida area with this wing on a Blue 10AE. Has a carbon fiber hood that is painted to match the car.\r\n\r\nThe bad, \r\nPaint is slightly scratched on nose and you can see cracks in the hood from the fiberglass resin cracking under the paint. The front strut bar is removed because of hood fitment. Car doesn\'t have the windshield wash nozzles installed. I installed a Isuzu tank in the cowl area and moved the wiring and tubing to that area but the pump quit working soon after the cowl was installed. I suspect the plug came off the pump. The wiring was never cut, just popped the pins out of the plug and fed them through the hole and plugged everything in. It was working before I finished the install of the cowl and the pump quick working. The trunk has a leak. I suspect the truck seal but I never replaced it. All the carboard and trunk carpet was removed before it was ruined. The seat reclining handles were removed because of the door bar fitment. I have the pieces to go back in if door bars are removed.\r\n\r\nAsking 9000.00 Call 407-62zero- zero648','2018-04-27 03:43:47','14.99','00','preferred','','Oviedo, Florida'),(47,'2006 Infiniti G35 Sedan PowerLab Single Turbo',NULL,9500,15,2,3,1,4,1,2,NULL,2,1,4,1,NULL,'2006','Infinity','G35',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',18,'Beautiful 2006 Infiniti G35 sedan with PowerLab single turbo and a ton of other nice upgrades. All mods were professionally done by IntensePower. Car is has been well maintained (only synthetic oil) and has dynoed 330rwhp at low boost (since it is my daily driver) by UMS tuning. Car has is fully loaded including automatic transmission, leather, sunroof, pw, pdl, pb, cold ac, tilt, alarm etc etc. Car has 149k miles and runs awesome.\r\n\r\n\r\nUPGRADES:\r\n\r\nPerformance:\r\nPowerLab Single Turbo kit with Precision 6262 Dual Ball Bearing Turbo\r\nTiAL Wastegate and Blow Off Valve\r\nOsirus ECU Tune by UMS Tuning\r\nDeatchwerks 600cc injectors\r\nWalbro Pump\r\nSnowperformance Meth Injection\r\nForged Aluminum Radiator\r\nPolished Upper intake manifold\r\nPlenum Spacer\r\nRare JDM Polished Engine Cover\r\n\r\nDrivertain:\r\nFresh fluid and filter flush. Warranty on tranny.\r\nDual High Capacity Durale Tranny Coolers with Twin High Flow Fans\r\nAutometer Transmission Temp Gauge\r\n\r\nInterior:\r\nSwitched complete black leather interior from the tan (willow) color. \r\nAll seats carpet and door panels replaced with black ones. Leather is almost new shape. \r\nAEM Tru-Boost Boost Controller\r\nAEM Wideband\r\nCustom center console dual gauge pod\r\n\r\nBrakes:\r\nAkebono Big Brake Kit Front and Rear\r\nBrakes Custom Coated High Temp Gold\r\nStillen Braided Brake Lines\r\n\r\nSuspension/Wheels:\r\nKsport Control Pro Coilovers\r\nStillen Front and Rear Sway Bars\r\nMRR 19\" Staggered Wheels\r\nNewer 245/35-19 and 275/30-19 Tires\r\nWhiteline Bushings\r\nFront upper adjustable camber arms\r\nCamber Arms\r\n\r\n\r\nExterior:\r\nNice original silver paint\r\nNismo Style Urethane Front Bumper - some scratches at bottom\r\nCustom APR Carbon Fiber Diffuser\r\nCustom PowerLab one-off Dual Rear Exhaust with Quad Tips\r\n\r\nOver $25K Spent in upgrades. I know what my car is worth so please don\'t insult me. Probably forgetting a lot of stuff. Please don\'t waste my time if you are not serious or cannot afford it. Already have new car so not looking for any trades. If ad is still up the car is available.','2018-04-27 03:47:35','14.99','00','preferred','','Chandler, Arizona');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribe_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe`
--

LOCK TABLES `subscribe` WRITE;
/*!40000 ALTER TABLE `subscribe` DISABLE KEYS */;
INSERT INTO `subscribe` VALUES (1,'a@a'),(2,'a@e'),(3,'a@eee'),(4,'kustomklassifieds@gmail.com'),(5,'kustomklassifieds@gmail.com');
/*!40000 ALTER TABLE `subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `expiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `sell_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaction_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (0,NULL,NULL,NULL,'2018-01-11 17:57:56');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transmissiontype`
--

DROP TABLE IF EXISTS `transmissiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transmissiontype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transmissiontype`
--

LOCK TABLES `transmissiontype` WRITE;
/*!40000 ALTER TABLE `transmissiontype` DISABLE KEYS */;
INSERT INTO `transmissiontype` VALUES (0,'0'),(1,'Automatic'),(2,'Manual'),(3,'Other');
/*!40000 ALTER TABLE `transmissiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `phone` varchar(110) DEFAULT NULL,
  `mobile` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `sex` varchar(110) DEFAULT NULL,
  `email` varchar(110) NOT NULL,
  `type` varchar(110) DEFAULT NULL,
  `bio` varchar(1010) DEFAULT NULL,
  `facebook` varchar(110) DEFAULT NULL,
  `instagram` varchar(110) DEFAULT NULL,
  `twitter` varchar(110) DEFAULT NULL,
  `pass_reset_key` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL),(1,'a','11','adasdas','asd','asdasdasdasd','asdffasd','Male','a@a','seller',NULL,NULL,NULL,NULL,NULL),(2,'b','11','fsdfasdf','asdf','fasd','fsdafasd','Other','a@g','seller',NULL,NULL,NULL,NULL,NULL),(3,'shahad','1234','Shahad','029112126','01711389230','salimullah road','Male','shahad@gmail.com','seller',NULL,NULL,NULL,NULL,NULL),(4,'al','123','','','','','Male','alaminbbsc@gmail.com','seller',NULL,NULL,NULL,NULL,NULL),(5,'aaa','11','rtwer','241','5345','6','null','rftgrtw@rwer','seller',NULL,NULL,NULL,NULL,NULL),(6,'asdf','11','11','11','11','11','null','wqrw@etw','seller',NULL,NULL,NULL,NULL,NULL),(7,'asas','11','11','11','11','11','null','rqwer@yerty','seller',NULL,NULL,NULL,NULL,NULL),(8,'aass','11','11','11','11','11','undefined','qwer@qwr','seller',NULL,NULL,NULL,NULL,NULL),(9,'asdfghjk','11','11','11','11','11','undefined','wrqerqwe@retwert','seller',NULL,NULL,NULL,NULL,NULL),(10,'sha','11','11','11','11','11','undefined','qwer@dwert','seller',NULL,NULL,NULL,NULL,NULL),(11,'www','11','11','11','11','','undefined','fas@dfg','seller',NULL,NULL,NULL,NULL,NULL),(12,'sadi','11','Sadi Amin11','00112211','22334411','Sylhet12','undefined','sadi11@gmail.com','seller','mY bIO ','12','12','12','32115797'),(13,'admin','11','Karguy My Friend','1234567890','0987654321','USA','undefined','karguy@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(14,'dev','11','Al- Amin','12323','35434534','Dhaka','undefined','aa@aaaaa','seller',NULL,NULL,NULL,NULL,NULL),(15,'kustomklassifieds','admin','Kustom Klassifieds','0','0','Internet Land','undefined','kustomklassifieds@gmail.com','seller','Owner of Kustom Klassifieds! Show us what kind of cool ride you have for sale.',NULL,NULL,NULL,NULL),(16,'ala','11','11','11','11','11','undefined','ala@ala','seller',NULL,NULL,NULL,NULL,NULL),(17,'tester','testerpassword','tester','0','0','0','undefined','d34dsil3nce@yahoo.com','seller',NULL,NULL,NULL,NULL,NULL),(18,'tester01','ChangeMe01!','Tester','11111111','1111111','111111','undefined','testeraccountkk@yahoo.com','seller',NULL,NULL,NULL,NULL,NULL),(19,'julianstogs','Js3042611!','Julian Stogsdill','9139548344','9139548344','','undefined','jasnbf@mail.umkc.edu','seller',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicletype`
--

DROP TABLE IF EXISTS `vehicletype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicletype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicletype`
--

LOCK TABLES `vehicletype` WRITE;
/*!40000 ALTER TABLE `vehicletype` DISABLE KEYS */;
INSERT INTO `vehicletype` VALUES (0,'0'),(1,'Truck'),(2,'SUV'),(3,'Coupe'),(4,'Sedan'),(5,'Hatchback'),(6,'Motorcycle'),(7,'RTV'),(8,'Crossover'),(9,'Hybrid'),(10,'Van'),(11,'Other'),(12,'Dirt Bikes'),(13,'ATV'),(14,'Boat'),(15,'Snowmobile'),(16,'Sandrail');
/*!40000 ALTER TABLE `vehicletype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-28 20:37:31
