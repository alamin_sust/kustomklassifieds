-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cardb
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (0,'0'),(1,'Vehicle'),(2,'Parts');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1` int(11) DEFAULT NULL,
  `user2` int(11) DEFAULT NULL,
  `message` varchar(1010) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (3,12,13,'hii','2018-03-03 14:17:53'),(4,12,13,'Hello KKK','2018-03-03 14:40:38'),(5,12,13,'hii','2018-03-03 14:45:49'),(6,12,3,'kkkkk','2018-03-03 14:46:00'),(7,13,12,'yes','2018-03-03 14:46:33'),(8,13,12,'Lool','2018-03-03 14:53:22'),(9,12,3,'lloo\r\n','2018-03-03 14:57:06'),(11,17,15,'hello, you have a car for sale','2018-03-25 22:53:57'),(12,13,12,'rwerw','2018-04-19 22:04:11');
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condition_`
--

DROP TABLE IF EXISTS `condition_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `condition_` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condition_`
--

LOCK TABLES `condition_` WRITE;
/*!40000 ALTER TABLE `condition_` DISABLE KEYS */;
INSERT INTO `condition_` VALUES (0,'0'),(1,'new'),(2,'used');
/*!40000 ALTER TABLE `condition_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customtype`
--

DROP TABLE IF EXISTS `customtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customtype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customtype`
--

LOCK TABLES `customtype` WRITE;
/*!40000 ALTER TABLE `customtype` DISABLE KEYS */;
INSERT INTO `customtype` VALUES (0,'0'),(12,'Auto-cross'),(6,'Diesel'),(2,'Drag'),(1,'Drift'),(11,'Exotic'),(5,'High Risers'),(4,'Low Rider'),(7,'Offroad'),(14,'Other'),(8,'Performance Trucks'),(13,'Road Course'),(9,'Stanced Vehicles'),(3,'Street'),(10,'Vintage/Classic');
/*!40000 ALTER TABLE `customtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivetype`
--

DROP TABLE IF EXISTS `drivetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivetype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivetype`
--

LOCK TABLES `drivetype` WRITE;
/*!40000 ALTER TABLE `drivetype` DISABLE KEYS */;
INSERT INTO `drivetype` VALUES (0,'0'),(1,'RWD'),(2,'AWD'),(3,'FWD'),(4,'4WD');
/*!40000 ALTER TABLE `drivetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enginecylinders`
--

DROP TABLE IF EXISTS `enginecylinders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enginecylinders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enginecylinders`
--

LOCK TABLES `enginecylinders` WRITE;
/*!40000 ALTER TABLE `enginecylinders` DISABLE KEYS */;
INSERT INTO `enginecylinders` VALUES (0,'0'),(1,'1'),(2,'2'),(3,'4'),(4,'6'),(5,'8'),(6,'10'),(7,'12'),(8,'Electric/Hybrid'),(9,'Other');
/*!40000 ALTER TABLE `enginecylinders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueldelivery`
--

DROP TABLE IF EXISTS `fueldelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueldelivery` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueldelivery`
--

LOCK TABLES `fueldelivery` WRITE;
/*!40000 ALTER TABLE `fueldelivery` DISABLE KEYS */;
INSERT INTO `fueldelivery` VALUES (0,'0'),(1,'Fuel Injected'),(2,'Carborated'),(3,'Other');
/*!40000 ALTER TABLE `fueldelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueltype`
--

DROP TABLE IF EXISTS `fueltype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueltype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueltype`
--

LOCK TABLES `fueltype` WRITE;
/*!40000 ALTER TABLE `fueltype` DISABLE KEYS */;
INSERT INTO `fueltype` VALUES (0,'0'),(1,'E85'),(2,'Pump Gas'),(3,'Race Gas'),(4,'Diesel'),(5,'Other'),(6,'Gas & Oil');
/*!40000 ALTER TABLE `fueltype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listingfeatures`
--

DROP TABLE IF EXISTS `listingfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listingfeatures` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listingfeatures`
--

LOCK TABLES `listingfeatures` WRITE;
/*!40000 ALTER TABLE `listingfeatures` DISABLE KEYS */;
INSERT INTO `listingfeatures` VALUES (0,'0'),(1,'Listings with photos'),(2,'Listing with price');
/*!40000 ALTER TABLE `listingfeatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (0,'0'),(1,'Range feature (under x miles)'),(2,'Unlimited Distance ');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poweradders`
--

DROP TABLE IF EXISTS `poweradders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poweradders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poweradders`
--

LOCK TABLES `poweradders` WRITE;
/*!40000 ALTER TABLE `poweradders` DISABLE KEYS */;
INSERT INTO `poweradders` VALUES (0,'0'),(1,'Supercharger'),(2,'Single Turbo'),(3,' Twin Turbo'),(4,' Nitrous'),(5,' Naturally Aspirated'),(6,' Compound/other');
/*!40000 ALTER TABLE `poweradders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `condition_Id` int(11) DEFAULT NULL,
  `customtypeId` int(11) DEFAULT NULL,
  `drivetypeId` int(11) DEFAULT NULL,
  `enginecylindersId` int(11) DEFAULT NULL,
  `fueldeliveryId` int(11) DEFAULT NULL,
  `fueltypeId` int(11) DEFAULT NULL,
  `listingfeaturesId` int(11) DEFAULT NULL,
  `poweraddersId` int(11) DEFAULT NULL,
  `transmissiontypeId` int(11) DEFAULT NULL,
  `vehicletypeId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `year` varchar(110) DEFAULT NULL,
  `make` varchar(110) DEFAULT NULL,
  `model` varchar(110) DEFAULT NULL,
  `is_sold` int(11) DEFAULT '0',
  `status` varchar(110) DEFAULT 'unpaid',
  `zip` varchar(110) DEFAULT NULL,
  `feature` int(11) DEFAULT '1',
  `ip` varchar(110) DEFAULT NULL,
  `latitude` varchar(110) DEFAULT NULL,
  `longitude` varchar(110) DEFAULT NULL,
  `image_count` int(11) DEFAULT '5',
  `details` varchar(4000) DEFAULT 'Have a look at this car! It is awesome',
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `listing_fees` varchar(110) DEFAULT NULL,
  `payment_tx_id` varchar(1010) DEFAULT NULL,
  `package` varchar(110) DEFAULT 'free',
  `token` varchar(110) DEFAULT NULL,
  `place` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL,'free',NULL,NULL),(3,'Big Turbo Lexus IS300',NULL,12500,15,2,3,1,4,1,2,NULL,2,1,4,1,NULL,'2001','Lexus','IS300',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',4,'Clean title, aristo motor swap, it has all new seals new water pump new oil pump all from Toyota the turbo is a s366 with upgraded wheel and thrust washer upgrade, new style Cx racing  manifold, tial 44 wg, turbosmart trumpet BOV, custom full 3inch intercooler piping, mishimoto 3 row intercooler biggest I can fit with the stock bumper, custom 3inch down pipe to full 3inch exhaust all stainless to a greddy sp muffler for a Supra, mishimoto radiator, factory lsd, Megan coilovers, Megan rear lower control arms, walbro 255 high flow pump , Aeromotive fuel pressure regulator, factory aristo trans valve body upgrade, volk gt-7 18-9 front 18-10 rear.  Everything works in the car  including the paddle shift.','2018-03-25 18:09:10','6.73','dkbx2fzp','free',NULL,NULL),(4,'2004 LS1 GTO',NULL,8500,15,2,3,1,5,1,2,NULL,5,1,3,1,NULL,'2004','Pontiac','GTO',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'This car has headers, intake, exhaust, and 243 Heads. 99k miles and clean title in hand. For sale by Luis Alarcon and his email is lalarcon4259@gmail.com','2018-03-25 18:42:11','6.73','6zrsbzqp','free',NULL,NULL),(5,'MINT 2012 Shelby GT500',NULL,40000,15,2,3,1,5,1,2,NULL,1,2,3,1,NULL,'2012','Ford','Shelby GT500',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'','2018-03-25 18:57:50','6.93','6rvemgen','free',NULL,NULL),(7,'2001 Cammed Z06',NULL,17000,15,2,3,1,5,1,2,NULL,5,2,3,1,NULL,'2001','Chevrolet','Corvette',0,'unpaid',NULL,0,'72.213.42.34','41.2061','-96.0451',3,'I have my 2001 Z06 up for sale. It has 93k miles. At about 88k I did all the work to it adding a cam, built the head with all new valve train, added bigger injectors and put headers on the car. Got it tuned on a dunk and made 420hp/409tq I did all the work over a year ago and haven\'t had any issue. Inside of the car is red and black and is in good condition with any rips on the leather. VIDEOS OF THE CAR CAN BE FOUND ON MY INSTAGRAM @slow.roush TEXT ONLY PLEASE.','2018-03-25 21:59:30','6.73',NULL,'free',NULL,NULL),(8,'Tastefully Modded - 2013 BMW 335i M-sport',NULL,25000,15,2,3,1,4,1,1,NULL,2,1,4,1,NULL,'2013','BMW','335i M-Sport',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'2013 BMW 335i M-sport 65000miles 8sp sport automatic with QS2 Fully loaded black leather, shadow line option (black trim and exhaust tips)  $25000 ( willing to negotiate price). Runs on pump91,93 or e85, intake, intercooler, charge pipes, custom exhaust, downpipe, and JB4  So for these cars \"full bolt on\" ~350hp/470tq ish no dyno.     CONTACT: Sopaw2127@yahoo.com or 402-957-3313','2018-03-25 22:38:43','6.73','c4hqv8yn','free',NULL,NULL),(9,'2004 Volvo S60R Stage 3',NULL,6500,15,2,3,2,9,1,1,NULL,2,2,4,1,NULL,'2004','Volvo','S60R',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',5,'Selling my 2004 Volvo S60R.  Overall it\'s in good shape not perfect has some scratches here and there nothing major still looks great. Interior is probably 6/10 needs some love but nothing wrong.  Car is Stage 3 and runs strictly e85 I\'ve never dynoed it cars with the same setup and mods have made around 350+awhp and 420+awtrq and it\'s definitely all of that. Mods list in pictures.  Currently car is having a small issue with inconsistent boost tracked it down to the map sensor car does not over boost and still drives fine I will edit the post once it\'s fixed. 198500 miles.  No trades at this time if I\'ll consider them the post will say so don\'t waist my time or yours. Price is obo','2018-03-25 22:49:29','6.73','7hdpgw53','free',NULL,NULL),(10,'2005 Subaru WRX STI swapped',NULL,17500,15,2,3,2,3,1,1,NULL,2,2,4,1,NULL,'2005','Subaru','WRX',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',14,'2005 Subaru Impreza WRX Premium  Obsidian Black Pearl Premium Package sunroof, heated seats, and heated windshield wipers  Clean Title in hand 125,xxx miles   Car runs and drives like a dream and is very quick and fun to drive. As far as the condition of the car goes the interior is in good shape other than the drivers seat bolster showing wear. On the exterior there are some imperfections like rock chips and scratches as the car is 12 years old and has 125k miles on it so it isn\'t perfect but a lot better shape than 99% of the blob eyes I\'ve seen around town. A lot of quality parts have been used on this build. Also Included with the sale is a load of stock parts I have in storage. All the major work on the car has been done at Russ Garage and he can vouch that nothing on this car has been half assed. I have 4 years worth of receipts for most of the work done to the car as well. The car just had the motor completely gone through and got a new passenger side head from a previous valve issue. Also has a new set of valves in both heads. After the motor was put back together I had it on the dyno for a fresh tune as of 10/17/17 from Graham. Only issue with the car is the rear differential makes a clicking noise under acceleration when the car is cold once it warms up it becomes less noticeable. It doesn\'t affect the way the car performs so I\'ve never messed with it. My asking price is not even close to how much money I have in this car so my price is firm. Not looking for any trades other than a corvette Z06. I\'m sure I\'m missing on some details so if you have any questions feel free to message me. No test drives unless cash in hand.  Performance: Built 2.0L short block from Outfront Motorsports Manley H beam rods Weisco Pistons ARP head studs  Race bearings  Killer B oil baffle and pick up line Blouch dominator 2.5xtr turbo 10cm hot side  ETS front mount intercooler  Tgv deletes  Cobb 4 bar Map sensor  AEM AIT sensor  Tial 38mm wastegate with dump tube ID1300cc injectors Five-O Motor Sports black ops 340lph fuel pump Radium fuel pump hard wire kit 2004 STI brembo brake kit (still 5x100) 2005 STI 6 speed transmission with DCCD Pro controls  R160 3.90 final drive LSD rear differential Competition Racing Stage 3 clutch with lightweight flywheel Grimmspeed electronic boost controller Go Fast Bits adjustable Blow Off Valve  Perrin turbo inlet Perrin cold air intake Grimmspeed external wastegate up pipe  TiTek Downpipe HKS catback exhaust BC Br Coilovers Koyorad aluminum radiator  Mishimoto radiator hoses  Grimmspeed Air Oil separator  Cobb Accessport V3  Tuned on E85 by Graham  Made 451 whp/417 torque   Exterior: Wingless trunk off a 2006 impreza 6k HID headlights  STI side skirts STI corner splitters STI fog light covers STI hood scoop STI V limited replica front lip Front grill badge delete  Perrin front plate delete  18x9.5 +40 Enkei NT03 in Sbc wrapped in Bridgestone potenzas   Interior: ATI tripple gauge clock pod with carbon fiber faceplate  Innovate LC-2 wideband in red Autometer boost and oil pressure gauges in red JDM STI blacked out dash with JDM red hazard button  STI gauge cluster STI Shift boot with shift trim Cobb Red shift knob  Pioneer 7\" touch screen  Viper 5701 Remote Start/Alarm System All interior lights replaced with white LED\'s  Call or text 4o2-35o-3o79','2018-03-25 23:12:40','8.53','9tpr7e9z','free',NULL,NULL),(11,'BUILT Shelby GT500',NULL,40000,15,2,2,1,5,1,1,NULL,1,2,3,1,NULL,'2011','Ford','Shelby GT500',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'Tons of pics and videos on this page and lsx_187r on instagram  Kona Blue 2011 GT500 less than 1k miles on motor and clutch  Mod List  Engine  FGT block  9.6:1  Cryo\'d double keyed crank  Manley rods w/ ARP2000 bolts  ARP hardware  Cometic head gaskets with ARP head studs  JDM superctock cams/springs  Valve job  Bowles cleaned up  Ported lower intake  Billet OPG  Billet crank sprocket  Billet tensioner arms Upgraded tensioners/gears Cryo\'d MMR XHD secondary chains and cryo\'d primary chains 15% OD IW HD balancer  BPS Underdrive alternator pulley  New plugs  BPS pulleys  Thump tensioner  Gates belts  UPR d/s catch can  Metco p/s breather  CFM oil cap  Induction  4.0 Whipple  Variety of pullies  Whipple eliptical t body  1320junkie DD149  PMAS 149mm MAF  Fuel  Fore triple pump return system(reg after rails)  10 feed/8 return  TI auto 274 pumps  ID1700x injectors  E85  Exhaust  ARH 2\" headers with 3.5\" collector  Off road H pipe  Stock cat back  Nitrous(New motor sprayed 1x on dyno with old tvs setup)  NX kit  NO nozzle in the CAI  NOS mini progressive controller  NO purge  NO switch panel  NO heated bottle bracket  Driveline  RXT 1200 clutch  MGW gen II shifter  SS clutch line  Amsoil ATF  DSS aluminum DS  DS loop  Welded tubes on rear end  Tru trac  Moser axles  FRPP girdle  373 gears  Suspension  BMR K member  BMR a arms  BPS radiator support(no sway bar)  Viking crusader coil overs  BPS spring perches  UPR strut mounts  BMR double adj. Rod end LCA  Steeda LCA relos  BMR UCA mount  BMR adj rod end UCA  BMR rod end anti roll bar  Steeda adj panhard bar and brace  Viking Crusader double adj shocks  Viking springs  JPC moly front bumper support  Bumpstop relos  TIG vision chute mount with green stroud chute  Brakes  Aerospace front brakes  JPC line lock  Aerospace rear brakes  SS lines  Amsoil fluid  Cooling  7gal trunk tank  Rule 2000 pump  VMP dual fan/triple pass HE  170 Tstat  Aluminum coolant tank  13/14 IC  13/14 radiator fan  Wheels/tires  Racestar 17x4.5 fronts with 27x4.5x17 hoosiers  Race star sngle champion beadlock 15x10 with 29x10.5Wx15 stiffwall Hoosiers 10ish passes  Stock SVTPP wheels w/ new rear tires  Another pair of Race star 15x10 with 29x10.5Wx15 stiffwall Hoosiers  Electronics/tuning  Remote tuned by Jon Lund II  Lund Ngauge  Raptor shift light  WOT box  Braille LW battery stock location  Interior  6pt wolfe racecraft bolt in Moly cage  Corbreau fx1 seats  Corbreau harnesses  Shrader rear seat delete  Aesthetics  13/14 grills  JLT battery and brake res covers  Various PC and polished underhood  LED light bulbs  Harwood cowl hood w/ quick latches Wrinkle black powdercoated lower intake, valve covers, timing cover, t stat housing, etc.  Have the stock seats, hood, suspension pieces, and much more that will go with it and I am sure I am forgetting other stuff as well.','2018-03-31 15:50:16','6.93','q45hkh9z','free',NULL,NULL),(12,'Built 13b FD RX7',NULL,25000,15,2,3,1,9,1,0,NULL,3,0,0,1,NULL,'1993','Mazda','RX7',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',6,'-13brew with a half bridge port, goopy studs and E&J apex seals.  -haltech platinum 1000 standalone ecu  -spec stage 3+ clutch and ACT monolock collar -s4 tii diff included -walboro 485 fuel pump with a walbro 450 fuel pump -built 2 apex duel fuel pump hanger - all fuel lines are braided steel the fuel is supplied by -10 from the tank downsizing to -8 braided steel lines supplying all fuel rails with -6 return line -rising rate Fuel pressure regulator - turbos are ran with all braided steel also. the oil supply, coolant supply, and coolant return are -6 and the oil return is -10 -custom upper intake manifold with auxiliary fuel rail -720cc primary injectors, id2000 secondaries, and 760cc auxiliary injectors  -xcessive LIM -FFE secondary fuel rail -twin turbonetics t04e t3/t4 hybrid turbo with .83 ar hot sides and will include .63 ar hot sides also.  -twin tial MV-S waste gates that are recirculating to the exhaust. Have option to hook up coolant lines just never did so -HKS type-S bov -custom intake and intercooler piping by Hisetech. Intake is all 3\" and intercooler is 2\" off turbos to intercooler and 3\" from intercooler to throttle body - asp large smic custom made for the twin turbo setup (original owner Kevin owned asp) -electronic and manual boost controller hooked up to car currently so you can change from spring pressure to whatever you want manual controller by the flip of a stealthy switch inside the car. - ronnin speed works intercooler duct -all couplers are 4ply silicone couplers with stainless t-bolt clamps -greddy intake elbow - twin aem dry flow filters mounted under headlight -full custom exhaust 2 1/2\" from turbos to full 3\" cat less exhaust with a borla inline muffler and a supertrapp muffler and tip on the back,  a second axle back that is a borla muffler  - LS1 coils -10.5 mm taylor plug wires -innovative mtx wideband with 8ft lead -60mm autometer liquid filled boost gauge -60mm autometer oil pressure gauge  -rims are ccw classics 17*10 on front 17*12 on rear -tires are nitto NT555 275/40/17s in front and 315/35/17s in back 3k miles on tires. -the suspension uses gab super-r adjustable with eibach springs -the front sway bar is a 2 1/2\" 3 piece design and the back is a 2\" single piece -KTS 4 point rear strut bar -r1 front strut bar -momo steering wheel with quick release -custom sleepy eye headlight -hella projectors -battery mounted in a moroso sealed box to pass IHRA tech -15% tint all around with a stip on front windshield  Bad - secondary Injectors weren\'t staging during the tune which is why it was cutting out. Engine is pulled to clean up the bay and wiring.  This setup is built for 5-600whp. 641-660-7128','2018-04-01 01:41:38','6.93','29dtk3h5','free',NULL,NULL),(13,'2001 Cobra asking 10K',NULL,10000,15,2,3,1,5,1,2,NULL,5,2,3,1,NULL,'2001','Ford','Mustang Cobra',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',7,'Looking to sell my 2001 cobra, I am going to get Married soon and its time for something else. Always Mobil 1 syn at every 3k or sooner and meticulously maintained.  Diablosport tuned MGW shortshifter Steeda full length subframe connectors Cobra anniversary wheels (also have stock cobra wheels) 315/35/17 tires in the back Stoptech powerslot rotors H&R Sport Springs CC plates JLT cold Air intake UPR Xpipe magnaflow mufflers welded onto SLP catback inplace of resonators Real carbon side scoops Touchscreen navigation and GPS Mcloud lightweight flywheel and Mcloud Clutch good to 500HP Braum Elite Diamond racing seats (also have stock cobra seats)  Pictures show broken oil pressure gauge which was replaced also abs light which has been fixed as well.','2018-04-01 01:44:42','7.13','g4evb7hx','free',NULL,NULL),(14,'02 WRX FUllY built with 05 STI drive train',NULL,17000,15,2,3,2,3,1,2,NULL,2,2,4,1,NULL,'2002','Subaru','WRX',0,'paid',NULL,0,'72.213.42.34','41.2061','-96.0451',11,'location Fresno CA  Not sure if i want to sell the whole car or just part it out. mod list is below  i have an 02 WRX with a 2.5L fully built motor and built 2.5 heads.  car was tuned at YIMI sport.   car will pass SMOG runs great with no issues at all, just started a family. CLean title. My first car of 10+ years now must go. i do have the invoice for almost all the parts including the motor build and drive train invoice ect ect.    Willing to sell the car for 17k OBO. if not willing to part it out which im sure will be more that way. the car is on E85  shoot me over prices. willing to ship on buyer expense. No low balling. i know how much a complete STI drive train with an R180 diff goes for on here as well the Wilwood brakes would be added to the price.   the car is still be driven at the moment. i have 30k on the motor and 10k on the drive train.     im sure i have a ton more parts missing on the details here. this car was done right and i didn\'t half ass anything.     Mod list  >>>> 2002 WRX  >>>> stage2 block  >>>> EJ25 with 2.5 heads  >>>> CP Forged Pistons, pins,  >>>> and rings  >>>> ACL Race Series  >>>> Bearings  >>>> STI Forged Connecting  >>>> Rods  >>>> different Crankshaft  >>>> Balanced and Polished  >>>> crank, rods, and pistons  >>>> CNC Precision Bore and  >>>> Hone with torque plates  >>>> Clearanced, Blueprinted,  >>>> and Assembled  >>>> - Motor: fully build EJ25,  >>>> CP pistons, BC titanium valve springs, BC 272 Cams, port and  >>>> polished heads(mild), Killer Bee oil pick up,   04 STI gauge cluster   Subaru NA intake manifold by outfront MS  1000cc ID injectors,  IAG Street AOS   STI ALM front control cars  >>>> DW, IAG fuel rails and IAG lines.  >>>> 300 320lp fuel pump,   >>>> CNT up pipe with a 44mm TIAL EWG, Tial bov , Perrin crank pulley,  >>>> CSF Radiator, Mishimoto Radiator hoses, COBB access port V3, Mishimoto 19row oil cooler  >>>> COBB EBC.  >>>> - Intercooler: ETS FMIC KSTEC 70m SRI ( speed density )   >>>> - Turbo: Cobb 20g  >>>> - Transmission: 2005 STI 5x114.3  >>>> with DCCD PRO with factory wheels that works with the STI cluster , Group N mount, Kartboy Short shifter, Kartboy  >>>> shifter bushing , Kartboy pitch stop mount, COBB shift knob  >>>> whiteline roll center kit  >>>> - Clutch: ACT HD clutch and ACT fly wheel ( they have 4k on them )   >>>> clutch, Goodridge stainless steel clutch line  >>>> SEATS Sparco Chrono, NGR roll bar, NRG 4 point seat belts.  >>>> - Brakes: WILWOOD BBK 6pot front, 4 rear  >>>> - Suspension: KW ClubSports with KW solid top mounts,  ST sway bars, Kartboy F&R  >>>> endlinks , Rear tower brace bars.  >>>> - Exhaust: P&L 3in catback, Invidia down pipe, HKS EL header.     >>>> -dai yoshihara D-12c 18x9 wheels   >>>> -tires FALKEN RT615k 255x40x18  VIS Demon carbon trunk and seibon carbon front lip.','2018-04-01 01:51:09','7.93','ag8tea73','free',NULL,NULL),(15,'AAA',NULL,45,12,1,0,0,4,0,0,NULL,0,0,0,1,NULL,'234','sd','dfgewr',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-04 22:12:46','3.99',NULL,'free',NULL,'3545234543'),(16,'test new fee structure',NULL,1213,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'','2018-04-04 22:29:41','0.0',NULL,'free',NULL,NULL),(17,'erwte',NULL,11,12,1,5,2,3,2,4,NULL,4,1,10,1,NULL,'123412','sd','A 10',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'rter','2018-04-04 22:32:47','8.99','c05mezv2','premium',NULL,NULL),(18,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-04 22:57:08','0.0',NULL,'free',NULL,NULL),(19,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-04 22:58:11','0.0',NULL,'free',NULL,NULL),(20,'',NULL,452,12,1,0,0,0,0,0,NULL,2,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-04 23:01:52','0.0',NULL,'free',NULL,'trywertwertwertwert'),(21,'',NULL,45,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:23:06','8.99',NULL,'premium','678657856aa',NULL),(22,'',NULL,123,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:26:14','14.99',NULL,'preferred','wedrwerqwerqwerqwerqwerqwerqwerqwer',NULL),(23,'',NULL,234,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:29:17','8.99',NULL,'premium','DDDD',NULL),(24,'AAAAAA',NULL,45,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:35:45','14.99',NULL,'preferred','AAAAA',NULL),(25,'',NULL,435234,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'3452345','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:38:29','0.0',NULL,'free','QWER',NULL),(26,'yreyterty',NULL,123,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:39:40','14.99',NULL,'preferred','hhhh',NULL),(27,'',NULL,123,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:44:16','14.99','00','preferred','GGGGGGGG',NULL),(28,'',NULL,123,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:47:48','0.0',NULL,'free','nnnn',NULL),(29,'',NULL,123,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:50:33','0.0',NULL,'free','lllllll',NULL),(30,'',NULL,45,15,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-15 22:52:04','14.99','00','preferred','X CCCCC',NULL),(31,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-20 19:48:34','0.0',NULL,'free','',NULL),(32,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-20 19:49:36','0.0',NULL,'free','',NULL),(33,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'','2018-04-20 19:58:15','0.0',NULL,'free','','asdasfdasfasdfasd \' \'\'ewrqe \' werewq\'r\' werq \'\'');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribe_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe`
--

LOCK TABLES `subscribe` WRITE;
/*!40000 ALTER TABLE `subscribe` DISABLE KEYS */;
INSERT INTO `subscribe` VALUES (1,'a@a'),(2,'a@e'),(3,'a@eee'),(4,'kustomklassifieds@gmail.com'),(5,'kustomklassifieds@gmail.com');
/*!40000 ALTER TABLE `subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `expiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `sell_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaction_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (0,NULL,NULL,NULL,'2018-01-11 17:57:56');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transmissiontype`
--

DROP TABLE IF EXISTS `transmissiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transmissiontype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transmissiontype`
--

LOCK TABLES `transmissiontype` WRITE;
/*!40000 ALTER TABLE `transmissiontype` DISABLE KEYS */;
INSERT INTO `transmissiontype` VALUES (0,'0'),(1,'Automatic'),(2,'Manual'),(3,'Other');
/*!40000 ALTER TABLE `transmissiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `phone` varchar(110) DEFAULT NULL,
  `mobile` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `sex` varchar(110) DEFAULT NULL,
  `email` varchar(110) NOT NULL,
  `type` varchar(110) DEFAULT NULL,
  `bio` varchar(1010) DEFAULT NULL,
  `facebook` varchar(110) DEFAULT NULL,
  `instagram` varchar(110) DEFAULT NULL,
  `twitter` varchar(110) DEFAULT NULL,
  `pass_reset_key` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL),(1,'a','11','adasdas','asd','asdasdasdasd','asdffasd','Male','a@a','seller',NULL,NULL,NULL,NULL,NULL),(2,'b','11','fsdfasdf','asdf','fasd','fsdafasd','Other','a@g','seller',NULL,NULL,NULL,NULL,NULL),(3,'shahad','1234','Shahad','029112126','01711389230','salimullah road','Male','shahad@gmail.com','seller',NULL,NULL,NULL,NULL,NULL),(4,'al','123','','','','','Male','alaminbbsc@gmail.com','seller',NULL,NULL,NULL,NULL,NULL),(5,'aaa','11','rtwer','241','5345','6','null','rftgrtw@rwer','seller',NULL,NULL,NULL,NULL,NULL),(6,'asdf','11','11','11','11','11','null','wqrw@etw','seller',NULL,NULL,NULL,NULL,NULL),(7,'asas','11','11','11','11','11','null','rqwer@yerty','seller',NULL,NULL,NULL,NULL,NULL),(8,'aass','11','11','11','11','11','undefined','qwer@qwr','seller',NULL,NULL,NULL,NULL,NULL),(9,'asdfghjk','11','11','11','11','11','undefined','wrqerqwe@retwert','seller',NULL,NULL,NULL,NULL,NULL),(10,'sha','11','11','11','11','11','undefined','qwer@dwert','seller',NULL,NULL,NULL,NULL,NULL),(11,'www','11','11','11','11','','undefined','fas@dfg','seller',NULL,NULL,NULL,NULL,NULL),(12,'sadi','11','Sadi Amin11','00112211','22334411','Sylhet12','undefined','sadi11@gmail.com','seller','mY bIO ','12','12','12','32115797'),(13,'admin','11','Karguy My Friend','1234567890','0987654321','USA','undefined','karguy@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(14,'dev','11','Al- Amin','12323','35434534','Dhaka','undefined','aa@aaaaa','seller',NULL,NULL,NULL,NULL,NULL),(15,'kustomklassifieds','admin','Kustom Klassifieds','0','0','Internet Land','undefined','kustomklassifieds@gmail.com','seller','Owner of Kustom Klassifieds! Show us what kind of cool ride you have for sale.',NULL,NULL,NULL,NULL),(16,'ala','11','11','11','11','11','undefined','ala@ala','seller',NULL,NULL,NULL,NULL,NULL),(17,'tester','testerpassword','tester','0','0','0','undefined','d34dsil3nce@yahoo.com','seller',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicletype`
--

DROP TABLE IF EXISTS `vehicletype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicletype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicletype`
--

LOCK TABLES `vehicletype` WRITE;
/*!40000 ALTER TABLE `vehicletype` DISABLE KEYS */;
INSERT INTO `vehicletype` VALUES (0,'0'),(1,'Truck'),(2,'SUV'),(3,'Coupe'),(4,'Sedan'),(5,'Hatchback'),(6,'Motorcycle'),(7,'RTV'),(8,'Crossover'),(9,'Hybrid'),(10,'Van'),(11,'Other'),(12,'Dirt Bikes'),(13,'ATV'),(14,'Boat'),(15,'Snowmobile'),(16,'Sandrail');
/*!40000 ALTER TABLE `vehicletype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-21  3:15:56
