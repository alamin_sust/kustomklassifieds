-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cardb
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (0,'0'),(1,'Vehicle'),(2,'Parts');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1` int(11) DEFAULT NULL,
  `user2` int(11) DEFAULT NULL,
  `message` varchar(1010) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (1,13,12,'','2018-03-03 12:17:13'),(2,12,3,'','2018-03-03 14:09:22'),(3,12,13,'hii','2018-03-03 14:17:53'),(4,12,13,'Hello KKK','2018-03-03 14:40:38'),(5,12,13,'hii','2018-03-03 14:45:49'),(6,12,3,'kkkkk','2018-03-03 14:46:00'),(7,13,12,'yes','2018-03-03 14:46:33'),(8,13,12,'Lool','2018-03-03 14:53:22'),(9,12,3,'lloo\r\n','2018-03-03 14:57:06');
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condition_`
--

DROP TABLE IF EXISTS `condition_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `condition_` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condition_`
--

LOCK TABLES `condition_` WRITE;
/*!40000 ALTER TABLE `condition_` DISABLE KEYS */;
INSERT INTO `condition_` VALUES (0,'0'),(1,'new'),(2,'used');
/*!40000 ALTER TABLE `condition_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customtype`
--

DROP TABLE IF EXISTS `customtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customtype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customtype`
--

LOCK TABLES `customtype` WRITE;
/*!40000 ALTER TABLE `customtype` DISABLE KEYS */;
INSERT INTO `customtype` VALUES (0,'0'),(12,'Auto-cross'),(6,'Diesel'),(2,'Drag'),(1,'Drift'),(11,'Exotic'),(5,'High Risers'),(4,'Low Rider'),(7,'Offroad'),(14,'Other'),(8,'Performance Trucks'),(13,'Road Course'),(9,'Stanced Vehicles'),(3,'Street'),(10,'Vintage/Classic');
/*!40000 ALTER TABLE `customtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivetype`
--

DROP TABLE IF EXISTS `drivetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivetype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivetype`
--

LOCK TABLES `drivetype` WRITE;
/*!40000 ALTER TABLE `drivetype` DISABLE KEYS */;
INSERT INTO `drivetype` VALUES (0,'0'),(1,'RWD'),(2,'AWD'),(3,'FWD'),(4,'4WD');
/*!40000 ALTER TABLE `drivetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enginecylinders`
--

DROP TABLE IF EXISTS `enginecylinders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enginecylinders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enginecylinders`
--

LOCK TABLES `enginecylinders` WRITE;
/*!40000 ALTER TABLE `enginecylinders` DISABLE KEYS */;
INSERT INTO `enginecylinders` VALUES (0,'0'),(1,'1'),(2,'2'),(3,'4'),(4,'6'),(5,'8'),(6,'10'),(7,'12'),(8,'Electric/Hybrid'),(9,'Other');
/*!40000 ALTER TABLE `enginecylinders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueldelivery`
--

DROP TABLE IF EXISTS `fueldelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueldelivery` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueldelivery`
--

LOCK TABLES `fueldelivery` WRITE;
/*!40000 ALTER TABLE `fueldelivery` DISABLE KEYS */;
INSERT INTO `fueldelivery` VALUES (0,'0'),(1,'Fuel Injected'),(2,'Carborated'),(3,'Other');
/*!40000 ALTER TABLE `fueldelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fueltype`
--

DROP TABLE IF EXISTS `fueltype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fueltype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fueltype`
--

LOCK TABLES `fueltype` WRITE;
/*!40000 ALTER TABLE `fueltype` DISABLE KEYS */;
INSERT INTO `fueltype` VALUES (0,'0'),(1,'E85'),(2,'Pump Gas'),(3,'Race Gas'),(4,'Diesel'),(5,'Other'),(6,'Gas & Oil');
/*!40000 ALTER TABLE `fueltype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listingfeatures`
--

DROP TABLE IF EXISTS `listingfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listingfeatures` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listingfeatures`
--

LOCK TABLES `listingfeatures` WRITE;
/*!40000 ALTER TABLE `listingfeatures` DISABLE KEYS */;
INSERT INTO `listingfeatures` VALUES (0,'0'),(1,'Listings with photos'),(2,'Listing with price');
/*!40000 ALTER TABLE `listingfeatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (0,'0'),(1,'Range feature (under x miles)'),(2,'Unlimited Distance ');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poweradders`
--

DROP TABLE IF EXISTS `poweradders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poweradders` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poweradders`
--

LOCK TABLES `poweradders` WRITE;
/*!40000 ALTER TABLE `poweradders` DISABLE KEYS */;
INSERT INTO `poweradders` VALUES (0,'0'),(1,'Supercharger'),(2,'Single Turbo'),(3,' Twin Turbo'),(4,' Nitrous'),(5,' Naturally Aspirated'),(6,' Compound/other');
/*!40000 ALTER TABLE `poweradders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `condition_Id` int(11) DEFAULT NULL,
  `customtypeId` int(11) DEFAULT NULL,
  `drivetypeId` int(11) DEFAULT NULL,
  `enginecylindersId` int(11) DEFAULT NULL,
  `fueldeliveryId` int(11) DEFAULT NULL,
  `fueltypeId` int(11) DEFAULT NULL,
  `listingfeaturesId` int(11) DEFAULT NULL,
  `poweraddersId` int(11) DEFAULT NULL,
  `transmissiontypeId` int(11) DEFAULT NULL,
  `vehicletypeId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `year` varchar(110) DEFAULT NULL,
  `make` varchar(110) DEFAULT NULL,
  `model` varchar(110) DEFAULT NULL,
  `is_sold` int(11) DEFAULT '0',
  `status` varchar(110) DEFAULT 'unpaid',
  `zip` varchar(110) DEFAULT NULL,
  `feature` int(11) DEFAULT '1',
  `ip` varchar(110) DEFAULT NULL,
  `latitude` varchar(110) DEFAULT NULL,
  `longitude` varchar(110) DEFAULT NULL,
  `image_count` int(11) DEFAULT '5',
  `details` varchar(1010) DEFAULT 'Have a look at this car! It is awesome',
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `listing_fees` varchar(110) DEFAULT NULL,
  `payment_tx_id` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(5,'neymar',NULL,234,3,2,1,2,2,2,1,2,1,1,9,1,2,NULL,NULL,NULL,0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(6,'coutinho',NULL,234123,3,2,3,2,4,2,3,1,3,2,13,1,2,NULL,NULL,NULL,0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(7,'Sketch',NULL,1200,3,1,2,3,4,2,3,2,2,1,11,1,2,'2017','C','AVB',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(8,'Bird',NULL,123,3,2,4,3,2,3,3,1,4,1,8,1,1,'11','sd','fhgd',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(9,'Coutaaaaa',NULL,1112,3,1,4,3,2,1,3,1,2,1,3,1,2,'6556','65','456',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(10,'aaaaaaaaaaaa',NULL,22,3,2,11,1,5,1,3,1,3,2,3,1,1,'2','2342','234',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(11,'ert',NULL,234,12,2,4,3,4,2,4,1,3,2,3,1,1,'234','234','234',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(12,'tyu',NULL,67,3,2,4,2,2,2,3,1,2,1,2,2,1,'57','675','567',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(13,'neyyyyyy',NULL,123,3,2,3,4,5,1,2,1,3,1,2,1,2,'52345','2345234','345',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(14,'cccoooummmm',NULL,123567000000,3,2,3,1,3,3,2,1,3,2,11,2,1,'tetemm','etrtertmmm','567511111111',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(15,'erter',NULL,514,12,2,5,3,4,3,3,1,2,2,5,2,1,'534','345','34523',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(16,'iiiiii',NULL,467,12,2,4,3,4,2,4,2,3,1,13,1,2,'74567456','4756','567',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(17,'tytnn',NULL,2345460,3,1,11,3,3,3,1,1,2,2,12,1,2,'523456345','3452344365','3545234564356',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(18,'njr',NULL,1111,3,2,3,2,3,2,2,2,2,2,1,1,1,'1111','1111','1111',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(19,'ccccccccccccccc',NULL,53,14,2,3,2,6,2,4,1,2,2,11,1,2,'678','hfgh','567',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(20,'Lamborghini',NULL,123123,12,2,9,3,7,2,4,2,3,2,11,1,1,'2018','Dhk','2011',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(21,'test',NULL,123,15,1,4,3,6,3,4,1,3,2,4,1,2,'2012','toyota','2010',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(22,'yrtu',NULL,35434,15,2,4,3,2,3,4,2,2,2,10,1,1,'567','urtyu','uty',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(23,'test',NULL,90,15,2,3,4,6,1,3,1,4,2,3,1,2,'2020','Ford','Ford',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(24,'cccc',NULL,34123,12,1,5,1,4,2,2,1,2,1,2,1,1,'234213','rerqe','23423',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(26,'kk',NULL,234,12,1,0,0,0,0,0,0,0,0,0,0,0,'243','243','ljkl',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(27,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,NULL,0,'11','sd','',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(28,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,NULL,0,'2017','345','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(29,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,0,'2017','sd','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(30,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,0,'2017','345','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(31,'',NULL,12,12,1,0,0,0,0,0,NULL,0,0,0,1,0,'412','432','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(32,'NNNN',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,0,'11','C','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(33,'MAR',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,0,'2017','345','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(34,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,0,'234','sd','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(35,'bread',NULL,1231,12,1,3,3,4,2,3,NULL,2,2,8,1,1,'23541234','dfsd','dfgewr',0,'unpaid',NULL,1,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(36,'',NULL,12,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'12','121','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(37,'',NULL,123,12,2,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:20:31',NULL,NULL),(38,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'unpaid',NULL,3,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 18:25:09',NULL,NULL),(39,'',NULL,555,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'555','555','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',5,'Have a look at this car! It is awesome','2018-03-03 20:20:42',NULL,NULL),(40,'',NULL,444,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'444','44','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-03 20:23:12',NULL,NULL),(41,'tt1',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 11:21:03','673',NULL),(42,'qwer',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','dfgewr',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 11:30:59','673',NULL),(43,'asd',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 11:33:30','474',NULL),(44,'alamin_sust',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 11:38:39','474',NULL),(45,'alamin_sust11',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 11:41:50','399',NULL),(46,'werwe',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',6,'Have a look at this car! It is awesome','2018-03-17 11:54:54','494',NULL),(47,'Md. Al- Amin',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 11:57:33','673',NULL),(48,'werwe',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 12:00:35','598',NULL),(49,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','523','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 12:03:39','598',NULL),(50,'fasdfasd',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 12:11:09','673',NULL),(51,'werwe',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 13:01:57','6.73',NULL),(52,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 13:20:15','6.73',NULL),(53,'Md. Al- Amin',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 14:22:00','4.74',NULL),(54,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 14:26:27','3.99',NULL),(55,'werwe',NULL,34,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'5345','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 14:42:28','6.73','p05qpny2'),(56,'werwe',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 14:45:34','6.73','pxxm9kb3'),(57,'Md. Al- Amin',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 14:47:22','6.73','cvp6b33v'),(58,'tyr',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'ryr','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 14:50:13','6.73','2d0d75k4'),(59,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 14:55:37','6.73','efyg03ek'),(60,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 15:00:26','6.73','0vfbr9m5'),(61,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 15:03:45','6.73','6ercbnp1'),(62,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 15:10:04','6.73','2e6gdjpm'),(63,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 15:15:53','3.99','md4chrvs'),(64,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 15:20:45','3.99','g382r66f'),(65,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 15:26:39','3.99','3hq7sed9'),(66,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 15:36:10','3.99','mc6rw6a5'),(67,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 15:39:42','3.99','93mqyghr'),(68,'alamin_sust',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 15:47:53','3.99','p19gxney'),(69,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 15:50:20','3.99','4t39590r'),(70,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:06:36','3.99','bqq6r3n0'),(71,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:09:35','3.99','bkjyssjv'),(72,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:10:33','3.99','f1grkbs0'),(73,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:11:05','3.99','164az5jk'),(74,'werwe',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:12:32','3.99','2zgrq5wd'),(75,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:14:19','3.99','35tzg8p0'),(76,'',NULL,45,1,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:15:54','3.99','11k9ff5x'),(77,'',NULL,234,16,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:17:50','3.99','2gpyta5z'),(78,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:21:44','3.99','rm7v23we'),(79,'',NULL,123,16,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:24:16','3.99','fvnrqj3b'),(80,'',NULL,45,16,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',2,'Have a look at this car! It is awesome','2018-03-17 16:26:20','3.99','r8f145da'),(81,'',NULL,123,16,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:31:37','3.99',NULL),(82,'',NULL,123,16,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:32:38','3.99','q1v9r11s'),(83,'',NULL,123,2,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 16:56:23','3.99','27zyrxd6'),(84,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 17:03:18','3.99',NULL),(85,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 17:07:03','3.99',NULL),(86,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 17:16:02','3.99','fw92rdgj'),(87,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 17:20:08','3.99',NULL),(88,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',6,'Have a look at this car! It is awesome','2018-03-17 17:34:13','6.93',NULL),(89,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 17:35:50','5.98','ps7nry8k'),(90,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','C','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-17 17:36:46','4.74','055rkdn7'),(91,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 17:46:28','5.98',NULL),(92,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 18:23:22','3.99',NULL),(93,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 18:33:04','4.74',NULL),(94,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 18:40:07','4.74',NULL),(95,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 18:45:04','6.73',NULL),(96,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','523','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 18:48:15','3.99','fjwbt4k4'),(97,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 19:45:07','3.99','65wdh5px'),(98,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 19:53:53','3.99',NULL),(99,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 19:55:46','3.99',NULL),(100,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 19:56:11','3.99',NULL),(101,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 19:56:41','3.99',NULL),(102,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 19:57:24','3.99',NULL),(103,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 19:59:43','3.99','0772k5bc'),(104,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-20 20:01:44','3.99',NULL),(105,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:06:50','3.99',NULL),(106,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','11','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:08:55','3.99','ncv9rp8k'),(107,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:14:28','3.99',NULL),(108,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:18:40','4.74',NULL),(109,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:20:51','6.73','fsy1qa4h'),(110,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:22:58','5.98',NULL),(111,'',NULL,123,12,2,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','523','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:26:20','3.99',NULL),(112,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:29:21','3.99','n6n9qxqa'),(113,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 14:59:47','3.99','4w9pz5mq'),(114,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:18:24','3.99',NULL),(115,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:18:58','6.73',NULL),(116,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:21:12','5.98',NULL),(117,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:23:10','5.98',NULL),(118,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','C','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:26:05','4.74',NULL),(119,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:34:06','4.74',NULL),(120,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'234','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:34:35','4.74','ht5abvsn'),(121,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:40:05','4.74','b42vfz3j'),(122,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 17:42:01','4.74','cm8grx2e'),(123,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 20:06:39','3.99',NULL),(124,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 20:35:21','3.99',NULL),(125,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 20:39:38','3.99',NULL),(126,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 20:45:32','3.99',NULL),(127,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 20:48:32','3.99',NULL),(128,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 20:50:21','3.99',NULL),(129,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 20:57:07','3.99','c58vngrx'),(130,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 21:52:07','3.99','7f63q8qt'),(131,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','C','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-21 21:55:27','5.98','7awwv2ry'),(132,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','523','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 07:05:21','3.99',NULL),(133,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:15:53','3.99','2ardyeej'),(134,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:39:08','3.99','dr2asbvd'),(135,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:41:35','3.99',NULL),(136,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:41:36','3.99',NULL),(137,'',NULL,234,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:41:36','3.99','ezkzy4vh'),(138,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:48:51','3.99',NULL),(139,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:51:54','3.99','34zfn1ta'),(140,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:54:14','5.98','hqzhpkdz'),(141,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:57:40','4.74',NULL),(142,'',NULL,45,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 10:58:51','4.74','7kgdxb5s'),(143,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 11:05:02','5.98','e2w7dsf7'),(144,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 11:12:55','3.99','rf9sd27g'),(145,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'unpaid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 11:15:36','5.98',NULL),(146,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'2017','345','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 11:15:55','5.98','nvpy9fx3'),(147,'',NULL,123,12,1,0,0,0,0,0,NULL,0,0,0,1,NULL,'11','sd','',0,'paid',NULL,0,'204.12.241.178','39.1472','-94.5735',1,'Have a look at this car! It is awesome','2018-03-23 11:17:21','3.99','jhw0sw6m');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribe_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe`
--

LOCK TABLES `subscribe` WRITE;
/*!40000 ALTER TABLE `subscribe` DISABLE KEYS */;
INSERT INTO `subscribe` VALUES (1,'a@a'),(2,'a@e'),(3,'a@eee'),(4,'kustomklassifieds@gmail.com'),(5,'kustomklassifieds@gmail.com');
/*!40000 ALTER TABLE `subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `sell_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaction_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (0,NULL,NULL,NULL,'2018-01-11 17:57:56');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transmissiontype`
--

DROP TABLE IF EXISTS `transmissiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transmissiontype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transmissiontype`
--

LOCK TABLES `transmissiontype` WRITE;
/*!40000 ALTER TABLE `transmissiontype` DISABLE KEYS */;
INSERT INTO `transmissiontype` VALUES (0,'0'),(1,'Automatic'),(2,'Manual'),(3,'Other');
/*!40000 ALTER TABLE `transmissiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `phone` varchar(110) DEFAULT NULL,
  `mobile` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `sex` varchar(110) DEFAULT NULL,
  `email` varchar(110) NOT NULL,
  `type` varchar(110) DEFAULT NULL,
  `bio` varchar(1010) DEFAULT NULL,
  `facebook` varchar(110) DEFAULT NULL,
  `instagram` varchar(110) DEFAULT NULL,
  `twitter` varchar(110) DEFAULT NULL,
  `pass_reset_key` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL),(1,'a','11','adasdas','asd','asdasdasdasd','asdffasd','Male','a@a','seller',NULL,NULL,NULL,NULL,NULL),(2,'b','11','fsdfasdf','asdf','fasd','fsdafasd','Other','a@g','seller',NULL,NULL,NULL,NULL,NULL),(3,'shahad','1234','Shahad','029112126','01711389230','salimullah road','Male','shahad@gmail.com','seller',NULL,NULL,NULL,NULL,NULL),(4,'al','123','','','','','Male','alaminbbsc@gmail.com','seller',NULL,NULL,NULL,NULL,NULL),(5,'aaa','11','rtwer','241','5345','6','null','rftgrtw@rwer','seller',NULL,NULL,NULL,NULL,NULL),(6,'asdf','11','11','11','11','11','null','wqrw@etw','seller',NULL,NULL,NULL,NULL,NULL),(7,'asas','11','11','11','11','11','null','rqwer@yerty','seller',NULL,NULL,NULL,NULL,NULL),(8,'aass','11','11','11','11','11','undefined','qwer@qwr','seller',NULL,NULL,NULL,NULL,NULL),(9,'asdfghjk','11','11','11','11','11','undefined','wrqerqwe@retwert','seller',NULL,NULL,NULL,NULL,NULL),(10,'sha','11','11','11','11','11','undefined','qwer@dwert','seller',NULL,NULL,NULL,NULL,NULL),(11,'www','11','11','11','11','','undefined','fas@dfg','seller',NULL,NULL,NULL,NULL,NULL),(12,'sadi','11','Sadi Amin11','00112211','22334411','Sylhet12','undefined','sadi11@gmail.com','seller','mY bIO ','12','12','12','32115797'),(13,'admin','11','Karguy My Friend','1234567890','0987654321','USA','undefined','karguy@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(14,'dev','11','Al- Amin','12323','35434534','Dhaka','undefined','aa@aaaaa','seller',NULL,NULL,NULL,NULL,NULL),(15,'kustomklassifieds','admin','Kustom Klassifieds','0','0','0','undefined','kustomklassifieds@gmail.com','seller',NULL,NULL,NULL,NULL,NULL),(16,'ala','11','11','11','11','11','undefined','ala@ala','seller',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicletype`
--

DROP TABLE IF EXISTS `vehicletype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicletype` (
  `id` int(11) NOT NULL,
  `name` varchar(110) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicletype`
--

LOCK TABLES `vehicletype` WRITE;
/*!40000 ALTER TABLE `vehicletype` DISABLE KEYS */;
INSERT INTO `vehicletype` VALUES (0,'0'),(1,'Truck'),(2,'SUV'),(3,'Coupe'),(4,'Sedan'),(5,'Hatchback'),(6,'Motorcycle'),(7,'RTV'),(8,'Crossover'),(9,'Hybrid'),(10,'Van'),(11,'Other'),(12,'Dirt Bikes'),(13,'ATV'),(14,'Boat'),(15,'Snowmobile'),(16,'Sandrail');
/*!40000 ALTER TABLE `vehicletype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-25 15:12:18
