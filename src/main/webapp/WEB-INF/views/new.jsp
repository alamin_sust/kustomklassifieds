<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="com.kustomklassifieds.web.util.WebUtils" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.kustomklassifieds.web.util.LocationUtils" %>
<%@ page import="com.mchange.v2.beans.StateBeanImporter" %>
<%@ page import="com.kustomklassifieds.web.util.Statics" %>

<style>
    .button {
        cursor: pointer;
        font-weight: 500;
        left: 3px;
        line-height: inherit;
        position: relative;
        text-decoration: none;
        text-align: center;
        border-style: solid;
        border-width: 1px;
        border-radius: 3px;
        -webkit-appearance: none;
        -moz-appearance: none;
        display: inline-block;
    }

    .button--small {
        padding: 10px 20px;
        font-size: 0.875rem;
    }

    .button--green {
        outline: none;
        background-color: #64d18a;
        border-color: #64d18a;
        color: white;
        transition: all 200ms ease;
    }

    .button--green:hover {
        background-color: #8bdda8;
        color: white;
    }

</style>


<meta charset="utf-8">
<script src="https://js.braintreegateway.com/web/dropin/1.10.0/js/dropin.min.js"></script>

<!-- HEADER -->
<%@ include file="header.jsp" %>
<!-- END HEADER -->


<form id="payment-form" method="post" action="checkout">
    <div id="dropin-container"></div>
    <input id="my-nonce-input" name="payment_method_nonce" type="hidden" required/>
    <input id="amount" name="amount" value="${totalAmount}" type="hidden" required/>
    <input id="productId" name="productId" value="${productId}" type="hidden" required/>
    <input type="submit" value="Purchase" class="button button--small button--green bbb"/>
</form>

<!-- FOOTER -->
<%@ include file="footer.jsp" %>
<!-- END FOOTER -->

<button id="submit-button">Request payment method</button>
<script>
    var button = document.querySelector('#submit-button');
    var form = document.querySelector('#payment-form');
    var hiddenNonceInput = document.querySelector('#my-nonce-input');

    braintree.dropin.create({
        authorization: <%if(Statics.IS_PRODUCTION){%>'production_tn7y3497_q7nqt6y76mx3kqm7'<%}else{%>'sandbox_hnfp98s7_tsvr5m595q9m83bn'<%}%>,
        container: '#dropin-container',
        paypal: {
            flow: 'checkout',
            amount: '${totalAmount}',
            currency: 'USD'
        },
        paypalCredit: {
            flow: 'checkout',
            amount: '${totalAmount}',
            currency: 'USD'
        },
        applePay: {
            displayName: 'My Store',
            paymentRequest: {
                total: {
                    label: 'My Store',
                    amount: '${totalAmount}'
                }
            }
        },
    }, function (createErr, instance) {
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
                // Submit payload.nonce to your server
                if (requestPaymentMethodErr) {
                    // handle error
                    return;
                }
                hiddenNonceInput.value=payload.nonce;
                form.submit();

            });
        });
    });
</script>