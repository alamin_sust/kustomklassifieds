<%@page import="com.kustomklassifieds.web.util.Constants" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.kustomklassifieds.Util.Utils" %>
<%@ page import="static com.kustomklassifieds.web.util.WebUtils.fixApoS" %>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="resources/css/chatCss.css" rel="stylesheet" type="text/css"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://use.fontawesome.com/45e03a14ce.js"></script>


<!-- HEADER -->
<%@ include file="header.jsp" %>
<!-- END HEADER -->

<%
    if(session.getAttribute("id")==null || session.getAttribute("id").equals("")) {
        response.sendRedirect("loginRegister");
    }

    try{

    if(request.getParameter("sender")!=null && !request.getParameter("sender").equals("")&&request.getParameter("receiver")!=null && !request.getParameter("receiver").equals("")&& request.getParameter("message")!=null && !request.getParameter("message").trim().equals("")) {
        String qUpd="insert into chat (user1,user2,message) values ("+fixApoS(request.getParameter("sender"))
                +","+fixApoS(request.getParameter("receiver"))+",'"+fixApoS(request.getParameter("message"));
        Statement stUpd = db.connection.createStatement();

        stUpd.executeUpdate(qUpd);
    }

    if(request.getParameter("chatTo")!=null && !request.getParameter("chatTo").equals("")) {
        String q="select COUNT(*) cnt from chat where user1="+fixApoS(request.getParameter("chatTo"))+" or user2="+fixApoS(request.getParameter("chatTo"));
        Statement st = db.connection.createStatement();

        ResultSet rs=st.executeQuery(q);

        if(rs.next() && rs.getInt("cnt")==0) {
            String qUpd="insert into chat (user1,user2,message) values ("+session.getAttribute("id")+","+fixApoS(request.getParameter("chatTo"))+",'')";
            Statement stUpd = db.connection.createStatement();

            stUpd.executeUpdate(qUpd);
        }

    }







%>

<div class="main_section">
    <div class="container">
        <div class="chat_container panel panel-default">
            <div class="col-sm-3 chat_sidebar">
                <div class="row">

                    <div class="member_list">
                        <ul class="list-unstyled">

                            <%
                                String myId = session.getAttribute("id").toString();
                                String q="select * from chat where user1="+myId+" or user2="+myId;
                                Statement st = db.connection.createStatement();

                                ResultSet rs=st.executeQuery(q);



                                Map<Integer,Boolean> mpp = new HashMap<>();
                                String sender = "";
                                int cnt=0;
                            while (rs.next()) {

                                String qU="";
                                Statement stU = db.connection.createStatement();

                                String chatTo="";

                                if(rs.getString("user1").equals(myId)) {
                                    if(!mpp.containsKey(rs.getInt("user2"))) {
                                        mpp.put(rs.getInt("user2"),true);
                                    } else continue;
                                    qU="SELECT * from user where id="+rs.getString("user2");
                                    chatTo=rs.getString("user2");
                                } else {
                                    if(!mpp.containsKey(rs.getInt("user1"))) {
                                        mpp.put(rs.getInt("user1"),true);
                                    } else continue;
                                    qU="SELECT * from user where id="+rs.getString("user1");
                                    chatTo=rs.getString("user1");
                                }

                                ResultSet rsU=stU.executeQuery(qU);
                                rsU.next();



                            %>

                            <li class="left clearfix" onclick="location.href = 'chat?chatTo=<%=chatTo%>'">
                     <span class="chat-img pull-left">
                     <img src="resources/img/user2.jpg"
                          alt="User Avatar" class="img-circle">
                     </span>
                                <div class="chat-body clearfix" <%if(/*cnt==0||*/(request.getParameter("chatTo")!=null
                                && fixApoS(request.getParameter("chatTo")).equals(chatTo))){%>style="color: blue"<%}%> >

                                    <div class="header_sec">
                                        <strong class="primary-font"><%=rsU.getString("username")%></strong>
                                        <%--<%if(rs.getString("message")!=null && !rs.getString("message").equals("")){%>
                                        <strong class="pull-right">
                                        <%=rs.getString("datetime")%></strong>
                                        <%}%>--%>
                                    </div>
                                    <%--<div class="contact_sec">
                                        <strong class="primary-font"><%=rs.getString("message")%></strong> &lt;%&ndash;<span
                                            class="badge pull-right">3</span>&ndash;%&gt;
                                    </div>--%>
                                </div>
                            </li>
                            <%cnt++;}%>
                        </ul>
                    </div>
                </div>
            </div>
            <!--chat_sidebar-->


            <div class="col-sm-9 message_section">
                <div class="row">
                    <%--<div class="new_message_head">
                        <div class="pull-left">
                            <button><i class="fa fa-plus-square-o" aria-hidden="true"></i> New Message</button>
                        </div>
                        <div class="pull-right">
                            <div class="dropdown">
                                <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-cogs" aria-hidden="true"></i> Setting
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Profile</a></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--new_message_head-->--%>

                    <div class="chat_area">
                        <ul class="list-unstyled">
                            <%
                            rs.beforeFirst();
                            cnt=0;
                            while (rs.next()) {

                                String qU="";
                                Statement stU = db.connection.createStatement();

                                Boolean iAmSender = false;
                                if(rs.getString("user1").equals(myId)) {
                                    qU="SELECT * from user where id="+rs.getString("user2");
                                    iAmSender=true;
                                } else {
                                    qU="SELECT * from user where id="+rs.getString("user1");
                                }

                                ResultSet rsU=stU.executeQuery(qU);
                                rsU.next();
                                sender = iAmSender?session.getAttribute("username").toString():rsU.getString("username");
                                String senderId = rs.getString("user1");
                                String receiverId = rs.getString("user2");
                                String chatTo = fixApoS(request.getParameter("chatTo"));

                                if(chatTo!=null && (chatTo.equals(senderId)|| chatTo.equals(receiverId))
                                        && myId!=null && (myId.equals(senderId)|| myId.equals(receiverId))
                                        && !myId.equals(chatTo) && rs.getString("message")!=null && !rs.getString("message").equals("")) {

                            %>

                            <li class="left clearfix">
                     <span class="chat-img1 pull-left">
                     <img src="resources/img/user2.jpg"
                          alt="User Avatar" class="img-circle">
                     </span>
                                <div class="chat-body1 clearfix">
                                    <p><b><%=sender%></b><span style="float: right"><%=rs.getString("datetime")%></span></p>
                                    <p><%=rs.getString("message")%></p>
                                    <%--<div class="chat_time pull-right"><%=rs.getString("datetime")%></div>--%>
                                </div>
                            </li>
                    <%cnt++;}}%>



                        </ul>
                    </div><!--chat_area-->
                    <div class="message_write">
                        <form method="post" action="chat?chatTo=<%=fixApoS(request.getParameter("chatTo"))%>">
                        <textarea class="form-control" name="message" placeholder="type a message" required></textarea>
                            <input type="hidden" name="sender" value="<%=myId%>">
                            <input type="hidden" name="receiver" value="<%=request.getParameter("chatTo")%>">
                        <div class="clearfix"></div>
                        <div class="chat_bottom"><%--<a href="#" class="pull-left upload_btn"><i class="fa fa-cloud-upload"
                                                                                             aria-hidden="true"></i>
                            Add Files</a>--%>
                            <button type="submit" class="pull-right btn btn-success">
                                Send</button></div>
                        </form>
                    </div>
                </div>
            </div> <!--message_section-->
        </div>
    </div>
</div>
<%

               } catch(Exception e) {
                System.out.println(e);
         } finally {
           }

%>

<!-- FOOTER -->
<%@ include file="footer.jsp" %>
<!-- END FOOTER -->
