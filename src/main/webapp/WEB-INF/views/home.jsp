<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="com.kustomklassifieds.web.util.WebUtils" %>
<%@ page import="java.util.List" %>


<!-- HEADER -->
<%@ include file="header.jsp" %>
<!-- END HEADER -->

<!--Slider-->
<%if (session.getAttribute("successMsg") != null) {%>
<div class="alert alert-success">
    <Strong><%=session.getAttribute("successMsg")%></Strong>
</div>
<%}%>
<%if (session.getAttribute("errorMsg") != null) {%>
<div class="alert alert-danger">
    <Strong><%=session.getAttribute("errorMsg")%></Strong>
</div>
<%}
    session.setAttribute("successMsg", null);
    session.setAttribute("errorMsg", null);

%>
<div class="callbacks_container">
    <ul class="rslides" id="slider3">
        <li>

            <div class="slider-info">
                <h3>Kustom <span style="color: #00e0c3">Klassifieds</span> </h3>
            </div>
        </li>
        <li>

            <div class="slider-info">
                <h3>Made for auto <span style="color: #00e0c3">enthusiast.</span></h3>
            </div>
        </li>
        <li>

            <div class="slider-info">
                <h3>Find your <span style="color: #00e0c3">perfect ride.</span> </h3>
            </div>
        </li>
    </ul>
</div>

<div class="row" style="margin-top: 20px; margin-bottom: 20px;">

    <div class="col col-sm-6">
                                        <span class="input-group-btn">
                                            <a class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto; transition: 0.5s all; color:white;" href="products?category=1"><span class="glyphicon glyphicon-plus"></span>&nbsp; <b>Buy Vehicle</b></a>
                                        </span>
    </div>
    <div class="col col-sm-6">
                                        <span class="input-group-btn">

                                            <%if(session.getAttribute("username")!=null && !session.getAttribute("username").toString().equals("")){%>
                                            <a class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto; transition: 0.5s all; color: black;
    background-color: yellow;" href="addProduct"><span class="glyphicon glyphicon-plus"></span>&nbsp; <b>Sell Vehicle</b></a>
                                            <%} else {%>
                                            <a class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto; transition: 0.5s all; color: black;
    background-color: yellow;" href="loginRegister"><span class="glyphicon glyphicon-minus"></span>&nbsp; <b>Sell Vehicle</b></a>
                                            <%}%>
                                        </span>
    </div>

<!--//Slider-->


<!-- Team -->
<div class="team-agile">
    <div class="mis-stage">
        <!-- The element to select and apply miSlider to - the class is optional -->
        <h3 style="color: #00e0c3; font-size: 40px; padding-bottom: 15px;padding-top: 15px;">Featured Klassifieds</h3>
        <ol class="mis-slider">

            <%

                Statement stProds = db.connection.createStatement();
                String qProds = "select * from product where id>0 and is_active=1 and status='paid' and ownerId!=26 and is_featured=1 order by id desc";
                ResultSet rsProds = stProds.executeQuery(qProds);

                int iter=0,count = 0,prodCount=0;

                while (rsProds.next()) {
                    prodCount++;
                }
                rsProds.beforeFirst();

                List<Integer> selectedNums = WebUtils.getRandomNumbers(prodCount, 7);


                while(rsProds.next() && count<7) {
                    if(!selectedNums.contains(iter)) {
                        iter++;
                        continue;
                    }
                    iter++;
                    count++;
            %>
            <!-- The slider element - the class is optional -->
            <li class="mis-slide">
                <!-- A slide element - the class is optional -->
                <a href="productDetails?sellerId=<%=rsProds.getString("ownerId")%>&productId=<%=rsProds.getString("id")%>" class="mis-container">
                    <!-- A slide container - this element is optional, if absent the plugin adds it automatically -->
                    <figure>
                        <!-- Slide content - whatever you want -->
                        <figcaption><%=rsProds.getString("name")%></figcaption>
                        <figcaption>$<%=rsProds.getString("price")%></figcaption>
                        <img src="resources/img/products/<%=rsProds.getString("ownerId")%>_<%=rsProds.getString("id")%>.jpg" alt="image" class="" />

                    </figure>
                </a> </li>
            <%}%>
        </ol>
    </div>
</div>
<!-- //Team -->




</div>
<div class="thim-click-to-bottom">
    <a href="#about" class="scroll">
        <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
    </a>
</div>
</div>
<!-- //banner -->

<!-- Main -->

<form action="products" method="get">
    <div class="form-group col col-sm-8 col-sm-offset-2">
        <div class="row"><select class="form-control" name="customtype">Kustom Type
            <option value="">--Select Type--</option>
            <%                                            Statement stType = db.connection.createStatement();
                String qType = "select * from customtype t where t.id>0 and 0<(select count(*) from product p WHERE p.customtypeId = t.id)";
                ResultSet rsType = stType.executeQuery(qType);
                while (rsType.next()) {
            %>
            <option value="<%=rsType.getString("id")%>"><%=rsType.getString("name")%></option>
            <%}%>
            <option value="">Any</option>
        </select></div>
        <div class="row"><input type="number" class="form-control" name="distance" placeholder="distance within (in miles)"/></div>
        <br>
        <div class="row">

            <div class="col col-sm-8">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto; transition: 0.5s all; color:white;" type="button">
                                                <span class="glyphicon glyphicon-search"></span> <b>Search</b>
                                            </button>
                                        </span>
            </div>
            <div class="col col-sm-4">
                                        <span class="input-group-btn">

                                            <a class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto; transition: 0.5s all; color:white;" href="customSearch"><span class="glyphicon glyphicon-plus"></span>&nbsp; <b>More Filters</b></a>

                                        </span>
            </div>
        </div>
    </div>
</form>


<!-- About -->
<div class="banner-bottom agile" id="about">
    <div class="container">
        <h2 class="tittle-one wow fadeInDown">About Us</h2>
        <div class="bottom-grids agileinfo">
            <div class="col-md-6 bottom-grid fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                <img src="resources/img/cars/14.jpg" alt="">
            </div>
            <div class="col-md-6 bottom-grid grid-one wow fadeInRight animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                <h4>Looking for a customized vehicle?</h4>
                <p>At Kustom Klassifieds, we strive to help you find the right buyer for your vehicle. Modified and customized vehicles can be tough to sell on traditional platforms, there just wasn't a simple way to search for those kinds of vehicles. Kustom Klassifieds takes care of that issue and puts your vehicle in front of an audience of automotive enthusiasts.</p>
                <%--<h4>What does our future look like</h4>
                <p>As we grow every day, we continue to make improvements to our website and find unique automotive content to share with you. We are passionate about growing our filters to match every auto enthusiast?s needs, as well as making our website one of the easiest to use on the market.</p>
                --%><p>If you have feedback for us, please let us know at <a href="mailto:KustomKlassifieds@gmail.com">KustomKlassifieds@gmail.com</a></p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //About -->

<div class="banner-bottom agile" id="about">
    <div class="container">
<div class="embed-responsive embed-responsive-16by9">
    <%--<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/r6zIGXun57U?rel=0" allowfullscreen></iframe>--%>
    <iframe width="854" height="480" src="https://www.youtube.com/embed/bvLXOkZ95uU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
    </div>
</div>
<!--count-->
<%
    Statement stCntUser = db.connection.createStatement();
    String qCntUser = "select count(*) as cnt from user where id>0";
    ResultSet rsCntUser = stCntUser.executeQuery(qCntUser);
    rsCntUser.next();

Statement stCntProduct = db.connection.createStatement();
String qCntProduct = "select count(*) as cnt from product where id>0";
ResultSet rsCntProduct = stCntProduct.executeQuery(qCntProduct);
rsCntProduct.next();

    Statement stCntSold = db.connection.createStatement();
    String qCntSold = "select count(*) as cnt from product where id>0 and is_sold=1";
    ResultSet rsCntSold = stCntSold.executeQuery(qCntSold);
    rsCntSold.next();
%>
<%--<div class="count-agileits">
    <canvas id="myCanvas"></canvas>
    <div class="count-grids">
        <div class="count-bgcolor-w3ls">
            <div class="col-md-4 count-grid">
                <i class="fa fa-users" aria-hidden="true"></i>
                <div class="count hvr-bounce-to-bottom">
                    <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='<%=rsCntUser.getString("cnt")%>' data-delay='.5' data-increment="1"><%=rsCntUser.getString("cnt")%></div>
                    <span></span>
                    <h5>Number of Users</h5>
                </div>
            </div>
            <div class="col-md-4 count-grid">
                <i class="fa fa-truck" aria-hidden="true"></i>
                <div class="count hvr-bounce-to-bottom">
                    <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='<%=rsCntProduct.getString("cnt")%>' data-delay='.5' data-increment="1"><%=rsCntProduct.getString("cnt")%></div>
                    <span></span>
                    <h5>Number of Listings</h5>
                </div>
            </div>
            <div class="col-md-4 count-grid">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                <div class="count hvr-bounce-to-bottom">
                    <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='<%=rsCntSold.getString("cnt")%>' data-delay='.5' data-increment="1"><%=rsCntSold.getString("cnt")%></div>
                    <span></span>
                    <h5>Successfull Agreements</h5>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel panel-default">
            <div class="fb-like row" style="height: 50px;" data-href="https://www.facebook.com/kustomklassifieds" data-send="true" data-width="450" data-show-faces="true"></div>

        </div>
    </div>
</div>
<!--count-->--%>

<!-- Services -->
<%--<div class="why-choose-agile">
    <div class="container">
        <h3 class="w3l_head">Best Services</h3>
        <div class="why-choose-agile-grids-top">
            <div class="col-md-4 agileits-w3layouts-grid">
                <div class="wthree_agile_us">
                    <div class="col-xs-9 agile-why-text">
                        <h4>Provide Best Quality Vehicles</h4>
                        <p>Our team works 24/7 to provide the best quality vehicles for you</p>
                    </div>
                    <div class="col-xs-3 agile-why-text">
                        <div class="wthree_features_grid hvr-rectangle-out">
                            <i class="fa fa-truck" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="wthree_agile_us">
                    <div class="col-xs-9 agile-why-text">
                        <h4>100% Genuine Sellers</h4>
                        <p>We guarantee 100% authentic sellers</p>
                    </div>
                    <div class="col-xs-3 agile-why-text">
                        <div class="wthree_features_grid hvr-rectangle-out">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="wthree_agile_us">
                    <div class="col-xs-9 agile-why-text">
                        <h4>Clear-cut agreements</h4>
                        <p>Clear-cut agreements between sellers and buyers all the way</p>
                    </div>
                    <div class="col-xs-3 agile-why-text">
                        <div class="wthree_features_grid hvr-rectangle-out">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="col-md-4 agileits-w3layouts-grid img">
                <img src="resources/images/abs.jpg" alt=" " class="img-responsive" />
            </div>
            <div class="col-md-4 agileits-w3layouts-grid">
                <div class="wthree_agile_us">
                    <div class="col-xs-3 agile-why-text">
                        <div class="wthree_features_grid hvr-rectangle-out">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-xs-9 agile-why-text two">
                        <h4>24/7 Service</h4>
                        <p>Out team works 24/7 to provide fastest response</p>
                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="wthree_agile_us">
                    <div class="col-xs-3 agile-why-text">
                        <div class="wthree_features_grid hvr-rectangle-out">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-xs-9 agile-why-text two">
                        <h4>Full Product Support</h4>
                        <p>We stand beside our buyers and sellers all the time to ensure the best service</p>
                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="wthree_agile_us">
                    <div class="col-xs-3 agile-why-text">
                        <div class="wthree_features_grid hvr-rectangle-out">
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-xs-9 agile-why-text two">
                        <h4>Priority Service</h4>
                        <p>We provide priority service to our premium customers</p>
                    </div>

                    <div class="clearfix"> </div>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>--%>
<!-- //services -->


<!-- //Main -->




<!-- FOOTER -->
<%@ include file="footer.jsp" %>
<!-- END FOOTER -->
