<!DOCTYPE html>
<html>
<style>
    body, html {
        height: 100%;
        margin: 0;
    }

    .bgimg {
        background-image: url('resources/img/cars/24.jpg');
        height: 100%;
        background-position: center;
        background-size: cover;
        position: relative;
        color: white;
        font-family: "Courier New", Courier, monospace;
        font-size: 40px;


    }

    .topleft {
        position: absolute;
        top: 0;
        left: 16px;
    }

    .bottomleft {
        position: absolute;
        bottom: 0;
        left: 16px;
    }

    .middle {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
    }

    hr {
        margin: auto;
        width: 40%;
    }
</style>
<body>

<div class="bgimg">
    <div class="topleft">
        <p><img src="resources/img/logo-white-t.png" style="width: 150px; height: 35px;"></p>
    </div>
    <div class="middle">
        <h1>COMING SOON</h1>
        <hr>
        <p><b>KustomKlassifieds</b></p>
    </div>
    <div class="bottomleft">
        <p>Loading...</p>
    </div>
</div>

</body>
</html>
