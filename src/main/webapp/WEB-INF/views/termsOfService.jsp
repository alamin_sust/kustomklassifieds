<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>


        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <div class="container-fluid text-center">    
            <div class="row content panel">
                <div class="col-sm-2 sidenav">

                </div>
                <div class="col-sm-8 text-left" >

                    <div class="row" style="margin-top: 30px; margin-left: 10px; margin-right: 10px;">
                        <h2 class="text-center">Terms of Service</h2>
                        <br>
                        <p>
                            Effective Date: October 28, 2017   
                        </p>     
                        <p>
                            Welcome to the website of Kustom Klassifieds LLC ("Kustom Klassifieds" "we," "us," or "our"). Our services (Services) allow you to buy and sell vehicles and parts with auto-enthusiasts around the country.
                        </p>
                        <p>
                            By accessing this website, you (the "User", or "you") represent and warrant 
                            that you have read, understood, and agree (1) to be bound by the following Terms
                            & Conditions ("Agreement"); (2) that you are above the age of majority and have
                            the right, authority, and capacity to abide by this Agreement; and (3) that you
                            agree to comply with all applicable laws and regulations concerning your access
                            and use of the website.
                        </p>
                        <br>
                        <h2>Access and Use of Website</h2>
                        <br>
                        <div>
                            <li>You cannot access or use the www.KustomKlassifieds.com website ("Website") 
                                for any illegal or unauthorized purpose. International users agree 
                                to comply with all local laws regarding privacy, online conduct and 
                                acceptable content.
                            </li>
                            <li>
                                You must not modify, adapt, or hack into the Website or modify another
                                website so as to falsely imply that it is associated with Website. 
                            </li>
                            <li>
                                You must not transmit any worms or viruses or
                                any code of a destructive nature.
                            </li>
                            <li>
                                You must not transmit any worms or viruses or 
                                any code of a destructive nature.
                            </li>
                        </div>
                        <br>
                        <h2>Ownership, Proprietary Information, and Intellectual Property.</h2>
                        <br>
                        <h3>Kustom Klassifieds Intellectual Property</h3>
                        <br>
                        <p>Except for content linked to or from an external source, all other information and materials that appear as part of this website (including text, graphics, images, illustrations, designs, icons, photographs, video clips, audio clips, interfaces, software, logos, titles, and names and collectively, ("Intellectual Property") are the property of Kustom Klassifieds. </p>
                        <p>The website as a whole and all of the Intellectual Property are protected by copyright, trade dress, and trademark laws of the United States, as well as international treaties, conventions, and the laws of other countries, as applicable. Except for that information which is in the public domain or for which you have been given written permission to use by Kustom Klassifieds you may not use, reproduce, copy, modify, publish, transmit, distribute, perform, display, download, license, enter into a database, create derivative works from, reverse engineer, transfer, or sell any Intellectual Property, information, software, or products obtained from or through this website, in whole or in part. </p>
                        <p>Your proprietary information</p>
                        <p>We agree that we have no rights to the data, documents, information or material that you submit in the course of using our site and Services ("Submitted Content"). When submitting data, you, and not us, shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all Submitted Content. We shall not be responsible or liable for the deletion, correction, destruction, damage, loss, or failure to store any Submitted Content. By using the Services, each user represents and warrants to us that it has the legal right to submit the Submitted Content. You agree to indemnify, defend and hold us harmless from any liability caused by your submission of Submitted Content in violation of a third party?s copyright or other rights of ownership. We reserve the right to withhold, remove and/or discard Submitted Content without notice for any breach, including without limitation, non-payment of fees. In addition, we reserve the right to withhold, remove and/or discard any Submitted Content submitted to us, which, in our sole opinion, is pornographic, inappropriate or otherwise in violation of our internal policies for publishable material. WE MAKE NO REPRESENTATION OR WARRANTY THAT ANY DATA SUBMITTED TO US WILL BE RETAINED FOR ANY PARTICULAR PERIOD OF TIME, AND YOU AGREE TO MAINTAIN ON YOUR SERVERS DUPLICATE COPIES OF ALL SUBMITTED CONTENT. </p>
                        <p>User Contributions</p>
                        <p>You agree not to post or store on the Site any software, information, data, databases, music, audio, video or audiovisual files, photographs, images, documents, text, digital files or other material ("Material") that violates or infringes anyone's intellectual property rights (including copyrights, trademarks, trade secrets, patents, publicity rights or (to the extent protectable) confidential ideas) or that violates U.S. law or that is obscene, obscene as to minors, child pornography, defamatory, racist, lewd, lascivious, filthy, excessively violent, harassing, or otherwise objectionable. By posting Material to this Site, you grant us a perpetual, irrevocable, nonexclusive, royalty free worldwide license to reproduce, adapt, distribute, perform (either publicly or by digital audio transmission) or publicly display all or any portion of the Material on the Site (though we will obtain your permission before doing so with any Material that is not publically available on our Site). You further represent and warrant that you own all rights, titles, and interests to such Material in full and without restrictions. You expressly agree that we may remove, disable, or restrict access to or the availability of any Material from the Site (including, but not limited to, Material that you have posted or stored) that we believe, in good faith and in our sole discretion, to violate the Terms of Service (whether or not we are actually correct in our assessment) or that is the subject of a notification duly sent to us pursuant to the Digital Millennium Copyright Act. If you believe that we have acted mistakenly with respect to certain material, you may contact us using one of the contact methods provided at the end of these Terms of Service, in which case we may investigate the matter further, though we retain full right and sole discretion as to whether or not to remove the material. Under no circumstances may we be held liable for removing, disabling, or restricting access to or the availability of Material. The provisions of this section are intended to implement these Terms of Service and are not intended to impose a contractual obligation on us to undertake, or refrain from undertaking any particular course of conduct. </p>
                        <p>Digital Millennium Copyright Act Compliance</p>
                        <p>We respect the intellectual property rights of others and expect users of the Site to do the same. In our sole and absolute discretion, we may terminate the accounts of users who infringe the intellectual property rights of others. You may contact our agent as indicated below if you believe that a work protected by a U.S. copyright that you own has been posted or stored on the Site without authorization. To file a copyright infringement notification with us, you will need to send a written communication that includes substantially the following (please consult your legal counsel or see Section 512(c)(3) of the Digital Millennium Copyright Act to confirm these requirements): i. A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive intellectual property right that is allegedly infringed? ii. Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site? iii. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the service provider to locate the material. Providing URLs in the body of your notification is the best way to help us locate content quickly? iv. Information reasonably sufficient to permit us to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted? v. A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law? and vi. A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed. Please note that the information provided in your legal notice may be forwarded to the person who provided the allegedly infringing content. Under Section 512(f) any person who knowingly materially misrepresents that material or activity is infringing may be subject to liability for damages, so please do not make false claims. After submitting a copyright infringement notification, copyright owners may realize that they misidentified content or that they may otherwise change their mind. As a result, we will honor retractions of copyright claims from the party who originally submitted them. To retract a notification, please send a statement of retraction, the complete and specific URL of the content in question, and an electronic signature to the email address we provide at the end of these Terms of Service. Please note that under Section 512(f) of the Copyright Act, any person who knowingly materially misrepresents that material or activity was removed or disabled by mistake or misidentification may be subject to liability. It is our policy to terminate, in appropriate circumstances, the access rights of repeat infringers Termination We reserve the right to terminate these Terms of Service with immediate effect at any time and for any reason. Expiration or termination (for any reason) of these Terms of Service, shall not affect any accrued rights or liabilities which either party may then have nor shall it affect any clause which is expressly or by implication intended to continue in force after expiration or termination.</p>
                        <br>
                        <h3>Linked content</h3>
                        <br>
                        <p>Third party content that is linked to or from Website belongs to such third parties or users, as applicable.</p>
                        <p>If you believe that your work has been copied in a way that constitutes copyright infringement, you should send written notification thereof, in accordance with the provisions of the Digital Millennium Copyright Act, to our designated agent. Please refer to ? REPORTING ? section for contact information.</p>
                        <br>
                        <h2>Limited License; Personal, Business, and Non-Commercial Use Limitation.</h2>
                        <br>
                        <p>You are hereby granted a non-exclusive license to view the content on the website, but only while accessing the website. Except to the extent required for the limited purpose of reviewing material on the website, electronic reproduction, adaptation, distribution, performance, or display is prohibited. Commercial use by you of any of the content for profit is strictly prohibited. Use of any of our trademarks as metatags on other websites also is strictly prohibited.</p>
                        <p>You agree that you are only authorized to visit, view, and retain a copy of pages of the website for your own personal use, and that you shall not duplicate, download, publish, modify, or otherwise distribute the material on the website for any commercial use, or for any purpose other than as described in this Agreement. You also agree not to deep-link to the website for any purpose, unless specifically authorized by Kustom Klassifieds.</p>
                        Unauthorized Use of the Website
                        <p>You shall not use any automatic or manual conduct, device, process, software, program, algorithm, methodology or routine, including but not limited to a "robot," "spider" or other similar process or functionality to interfere or attempt to interfere with, or impose an unreasonable burden or load on, the operation of the website.</p>
                        You shall not use Website for any illegal, obscene, abusive, offensive, harassing, improper or objectionable purpose, to sell or offer to sell any goods or services, to conduct or forward surveys, contests, or chain letters, any way we determine to be inappropriate or for any purpose that is prohibited by the terms and conditions of this Agreement. Illegal and/or unauthorized uses of the website, including, but not limited to, unauthorized framing of or linking to the website or unauthorized use of any robot, spider, or other automated device on the website, will be investigated and will be subject to appropriate legal action, including, without limitation, civil, criminal, and injunctive redress. </p>
                        <br>
                        <h2>Third Party Services</h2>
                        <br>
                        <p>From time to time, Kustom Klassifieds may use services, including but not limited to analytics and advertising, provided by persons or entities other than us ("Third-Parties"). We do not control these services and make no representations regarding, and are not liable or responsible for the accuracy, completeness, timeliness, reliability or availability of, any of such services. If you choose to access any Third-Parties site, you do so at your own risk. Any link from our website to a Third-Party website does not imply sponsorship, affiliation, or endorsement of the content on that Third-Party website or the operator or operations of that site. You are solely responsible for determining the extent to which you use any content at any Third-Party websites to which you might link from our website, or which may download or connect with through our website. If you believe we have provided a link to a site that contains infringing or illegal content or services, we ask that you notify us so that we may evaluate whether in our sole discretion to disable it.</p>
                        <p>WE ARE NOT RESPONSIBLE OR LIABLE FOR ANY LOSS OR DAMAGE OF ANY SORT INCURRED AS A RESULT OF ANY DEALINGS WITH ANY THIRD-PARTY WEBSITE OR MERCHANT OR OPERATOR OF SUCH A THIRD-PARTY WEBSITE.</p>
                        <br>
                        <h2>Linking Sources to Our Website</h2>
                        <br>
                        <p>If Kustom Klassifieds authorizes you to deep-link your website or a service offered by your website to Kustom Klassifieds in addition to and notwithstanding anything to the contrary, you understand and agree that (1) Kustom Klassifieds has no obligation to continue to provide or make the service available; (2) all conditions, disclaimers and limitations on use set forth in this Agreement remain in effect; (3) all intellectual property rights relating to Kustom Klassifieds and its technology, including all ownership rights, remain the exclusive property of Kustom Klassifieds; (4) you will be solely responsible for the data and content that you will download and/or publish on your website; and (5) Kustom Klassifieds may terminate at any time in its sole discretion your access.</p>
                        <p>Kustom Klassifieds reserves the right, but not the obligation, to remove any linked source if it contains or features any of the content, at our discretion, we find to be illegal, obscene, abusive, offensive, harassing, improper or objectionable purpose, selling or offering to sell any goods or services, conducting or forwarding surveys, contests, or chain letters, inappropriate or for any purpose that is prohibited by the terms and conditions of this Agreement.</p>
                        <br>
                        <h2>Reporting</h2>
                        <br>
                        <p>If you see objectionable content or have any questions about this Agreement, please contact Kustom Klassifieds at kustomklassifieds@gmail.com.</p>
                        <br>
                        <h2>Offline Conduct</h2>
                        <br>
                        <p>Although Kustom Klassifieds cannot monitor the conduct of its users off the website, it is a violation of this Agreement to use any information obtained from our website in violation of this terms of use.</p>
                        <br>
                        <h2>Violation of the Terms</h2>
                        <br>
                        <p>You agree that monetary damages may not provide a sufficient remedy to Kustom Klassifieds for violations of the terms of this Agreement, and you consent to injunctive or other equitable relief for such violations.</p>
                        <br>
                        <h2>DISCLAIMERS AND LIMITATIONS</h2>
                        <br>
                        <div>
                            <ol type="a">
                                <li>    Disclaimer. YOUR USE OF THE WEBSITE IS AT YOUR SOLE RISK. WE MAKE NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS SITE'S CONTENT, THE CONTENT OF ANY SITE LINKED TO THIS SITE, CONTRIBUTIONS, INFORMATION OR ANY OTHER ITEMS OR MATERIALS ON THIS SITE OR LINKED TO BY THIS SITE. WE ASSUME NO LIABILITY OR RESPONSIBILITY FOR ANY (A) ERRORS, MISTAKES OR INACCURACIES OF CONTENT AND MATERIALS, (B) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SITE, (C) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION STORED THEREIN, (D) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THIS SITE, (E) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE, WHICH MAY BE TRANSMITTED TO OR THROUGH THIS SITE BY ANY THIRD PARTY, AND/OR (F) ANY ERRORS OR OMISSIONS IN ANY CONTENT AND MATERIALS OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THIS SITE. SOME STATES OR JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF CERTAIN WARRANTIES, OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF YOU RESIDE IN ONE OF THESE STATES OR JURISDICTIONS, THE ABOVE LIMITATIONS OR EXCLUSIONS MAY NOT APPLY TO YOU.</li>
                                <li>    Limitation of Liability. IN NO EVENT SHALL WE BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY INDIRECT, CONSEQUENTIAL, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING LOST PROFIT DAMAGES ARISING FROM YOUR USE OF THIS SITE, CONTRIBUTIONS, MATERIALS OR ANY OTHER CONTENT THEREIN. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED IN THIS AGREEMENT, OUR LIABILITY TO YOU IN RESPECT OF ANY LOSS OR DAMAGE SUFFERED BY YOU AND ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, WHETHER IN CONTRACT, TORT OR FOR BREACH OF STATUTORY DUTY OR IN ANY OTHER WAY SHALL NOT EXCEED $50.</li>
                            </ol>
                        </div>
                        <br>
                        <h2>Indemnity</h2>
                        <br>
                        <p>You agree to indemnify, defend, and hold harmless Kustom Klassifieds its agents and affiliates, and their officers, directors and employees from and against any and all claims, actions, demands, liabilities, costs and expenses, including, without limitation, reasonable attorneys' fees, resulting from your breach of any provision of this Agreement, or any warranty you provide herein, or otherwise arising in any way out of your use of the website. Kustom Klassifieds reserves the right to take exclusive control and defense of any such claim otherwise subject to indemnification by you, in which event you will cooperate fully with Kustom Klassifieds in asserting any available defenses. </p>
                        <br>
                        <h2>Changes to this Agreement</h2>
                        <br>
                        <p>Kustom Klassifieds reserves the right, in its sole discretion, to modify, suspend, or terminate this Agreement and the website and/or any portion thereof, and/or your account, password at any time for any reason with or without notice to you. Please review this Agreement periodically for changes. Your continued use of this website constitutes your acceptance and agreement to be bound by these changes without limitation, qualification or change. If at any time you do not accept these changes, you must immediately discontinue access to the website. </p>
                        <br>
                        <h2>Modification/ Termination of website</h2>
                        <br>
                        <p>In the event of termination, you will still be bound by your obligations under this Agreement and any additional terms, including the warranties made by you, and by the disclaimers and limitations of liability. Additionally, Kustom Klassifieds shall not be liable to you or any third-party for any termination of your access. </p>
                        <br>
                        <h2>Arbitration</h2>
                        <br>
                        <p>You agree to resolve any claims relating to these Terms or the Services through final and binding arbitration, except as set forth under Exceptions to Agreement to Arbitrate below. </p>
                        <p>Arbitration Procedures. The American Arbitration Association (AAA) will administer the arbitration under its Commercial Arbitration Rules. The arbitration will be held in Dover, Delaware unless we agree otherwise. </p>
                        <p>Class Action Waiver</p>
                        <p>You agree NOT to bring a claim as a plaintiff or a class member in a class, consolidated, or representative action. You agree not to bring any class arbitrations, class actions, private attorney general actions, and consolidation with other arbitrations. </p>
                        <br>
                        <h2>General Legal Provisions</h2>
                        <br>
                        <p>This Agreement, your rights and obligations, our rights and obligations, and all actions contemplated by this Agreement will be governed by the laws of the United States of America and the State of Delaware, including Delaware rules concerning conflicts and choice of law, as if this Agreement were a contract wholly entered into and wholly performed within the State of Delaware. You hereby consent to the exclusive jurisdiction and venue of courts in the County of Dover, Delaware, USA, in all disputes arising out of or relating to this website. Access to our website is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including, without limitation, this paragraph. In the event of any legal action to enforce or interpret the terms of this Agreement, the prevailing party in such action shall be entitled to recover attorneys' fees and costs. </p>
                        <p>No joint venture, partnership, employment, or agency relationship exists between you and us as a result of this Agreement or use of our website. </p>
                        <p>If any provision in this Agreement is invalid or unenforceable under applicable law, including, but not limited to, the warranty disclaimers and liability limitations set forth above, the remaining provisions will continue in full force and effect, and the invalid unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision. </p>
                        <p>Our performance of this Agreement is subject to existing laws and legal process, and nothing contained in this Agreement is in derogation of our right to comply with law enforcement requests or requirements. </p>
                        <p>All rights not expressly granted herein are hereby reserved. </p>
                        <p>This Agreement is the entire and final agreement regarding access to our website. A printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.</p>
                        <br><br>
                    </div>
                </div>
                <div class="col-sm-2 sidenav">

                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->