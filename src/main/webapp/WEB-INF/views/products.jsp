<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="com.kustomklassifieds.web.util.WebUtils" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.kustomklassifieds.web.util.LocationUtils" %>
<%@ page import="static com.kustomklassifieds.web.util.WebUtils.fixApoS" %>
<%@ page import="com.kustomklassifieds.Util.Utils" %>

<!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%
            int pageNumber = request.getParameter("page")==null?1: Integer.parseInt(request.getParameter("page"));

            if((session.getAttribute("username")==null || session.getAttribute("username").toString().equals(""))
                    && (request.getParameter("myVehicles")!=null&&!request.getParameter("myVehicles").equals(""))){
                response.sendRedirect("loginRegister");
            } else {

            /*if (*//*(request.getParameter("myVehicles") != null && request.getParameter("myParts") != null) && *//*(session.getAttribute("id") == null || session.getAttribute("id").toString().equals(""))) {
                response.sendRedirect("loginRegister");
            }*/


            Statement st = db.connection.createStatement();
            String q = "select * from product where id>0 and status='paid' and is_active=1 and ownerId!=26";


//            String productCategory = fixApoS(request.getParameter("searchCategory"));
            String searchStr = "";

//            if (productCategory != null && !productCategory.equals("")) {
//                q += " and categoryId='" + productCategory + "'";
//                searchStr += " Category: " + productCategory + ".";
//            }

            String extraParams[] = {"year", "make", "model"};

            for(String param: extraParams) {
            if (request.getParameter(param) != null && !request.getParameter(param).equals("")) {
                    q += " and "+param+"='" + fixApoS(request.getParameter(param))+"'";
                }
            }
            
            Constants constants = new Constants();

            List<String> tables = constants.getDB_TABLES();
            Map<String, String> tableLabelMap = constants.getDBMap();

            for (int i = 0; i < tables.size(); i++) {
                String table = tables.get(i);
                String tableLabel = tableLabelMap.get(table);
                if (request.getParameter(table) != null && !request.getParameter(table).equals("")) {
                    q += " and " + table + "Id=" + fixApoS(request.getParameter(table));
                    Statement stCriteria = db.connection.createStatement();
                    String qCriteria = "select * from " + table + " where id=" + fixApoS(request.getParameter(table));
                    ResultSet rsCriteria = stCriteria.executeQuery(qCriteria);
                    rsCriteria.next();
                    searchStr += " " + tableLabel + ": " + rsCriteria.getString("name") + ".";
                }
            }

            if (session.getAttribute("id") != null && !session.getAttribute("id").toString().equals("")) {
                if(session.getAttribute("username")!=null && !session.getAttribute("username").toString().equals("admin")) {


                    if (request.getParameter("myVehicles") != null && fixApoS(request.getParameter("myVehicles")).equals("true")) {
                        q += " and categoryId=1 ";
                        q+=" and ownerId=" + session.getAttribute("id");
                    }

                    if (request.getParameter("myParts") != null && fixApoS(request.getParameter("myParts")).equals("true")) {
                        q += " and categoryId=2 ";
                        q+=" and ownerId=" + session.getAttribute("id");
                    }

                }
            }

            if ((request.getParameter("categoryId") != null && !request.getParameter("categoryId").equals("")) ) {
                q += " and categoryId="+fixApoS(request.getParameter("categoryId"));
            }

            if ((request.getParameter("zipCode") != null && !request.getParameter("zipCode").equals("")) ) {
                q += " and zip_code='"+fixApoS(request.getParameter("zipCode"))+"'";
            }

            //q+=" and rownum>"+(pageNumber*12)+" and rownum<="+((pageNumber+1)*12);

            String sortAppend = " order by ";

            String sortByUploadTimeStr = WebUtils.isNullOrEmpty(request.getParameter("sortByUploadTime"))?"":" datetime"+(request.getParameter("sortByUploadTime").equals("1")?" desc":" asc");
            String sortByPriceStr = WebUtils.isNullOrEmpty(request.getParameter("sortByPrice"))?"":" price"+(request.getParameter("sortByPrice").equals("1")?" asc":" desc");
            String sortByNameStr = WebUtils.isNullOrEmpty(request.getParameter("sortByName"))?"":" name"+(request.getParameter("sortByName").equals("1")?" asc":" desc");

            if(!sortByUploadTimeStr.equals("")) {
                sortAppend += sortByUploadTimeStr;
            } else if(!sortByPriceStr.equals("")) {
                sortAppend += sortByPriceStr;
            } else if(!sortByNameStr.equals("")) {
                sortAppend += sortByNameStr;
            } else {
                sortAppend += " datetime desc ";
            }

            String pageStr = " LIMIT "+((pageNumber-1)*12)+",12";
            String nextPageStr = " LIMIT "+((pageNumber)*12)+",12";

            ResultSet rs = st.executeQuery(q+sortAppend+pageStr);

            Statement nextPageSt = db.connection.createStatement();
            ResultSet nextPageRs = nextPageSt.executeQuery(q+sortAppend+nextPageStr);

            boolean nextPageExists = nextPageRs.next();

        %>

<%if (session.getAttribute("successMsg") != null) {
%>
<div class="alert alert-success">
    <Strong><%=session.getAttribute("successMsg")%></Strong>
</div>
<%}%>
<%if (session.getAttribute("errorMsg") != null) {%>
<div class="alert alert-danger">
    <Strong><%=session.getAttribute("errorMsg")%></Strong>
</div>
<%}
    session.setAttribute("successMsg", null);
    session.setAttribute("errorMsg", null);

%>




<!-- Team -->
<div class="team-agile" style="background-color: white;margin-bottom: 10px;margin-top: 10px;">
    <div class="w3-agile-he">
        <h3 style="color: #00e0c3;">Featured Klassifieds</h3>
    </div>
    <div class="mis-stage">
        <!-- The element to select and apply miSlider to - the class is optional -->
        <ol class="mis-slider">

            <%

                Statement stProds = db.connection.createStatement();
                String qProds = q;
                ResultSet rsProds = stProds.executeQuery(qProds+" and is_featured=1 "+sortAppend);

                int iter=0,count = 0,prodCount=0;

                while (rsProds.next()) {
                    prodCount++;
                }
                rsProds.beforeFirst();

                List<Integer> selectedNums = WebUtils.getRandomNumbers(prodCount, 7);


                while(rsProds.next() && count<7) {
                    if(!selectedNums.contains(iter)) {
                        iter++;
                        continue;
                    }
                    iter++;
                    count++;
            %>
            <!-- The slider element - the class is optional -->
            <li class="mis-slide">
                <!-- A slide element - the class is optional -->
                <a href="productDetails?sellerId=<%=rsProds.getString("ownerId")%>&productId=<%=rsProds.getString("id")%>" class="mis-container">
                    <!-- A slide container - this element is optional, if absent the plugin adds it automatically -->
                    <figure>
                        <!-- Slide content - whatever you want -->
                        <figcaption><%=rsProds.getString("name")%></figcaption>
                        <figcaption>$<%=rsProds.getString("price")%></figcaption>
                        <img src="resources/img/products/<%=rsProds.getString("ownerId")%>_<%=rsProds.getString("id")%>.jpg" alt="image" class="" />

                    </figure>
                </a> </li>
            <%}%>
        </ol>
    </div>
</div>
<!-- //Team -->

<form action="products" method="get">
    <div class="form-group col col-sm-12">
        <div class="row col-sm-6 col-sm-offset-3"><select class="form-control" name="customtype">Kustom Type
            <option value="">--Select Type--</option>
            <%                                            Statement stType = db.connection.createStatement();
                String qType = "select * from customtype where id>0";
                ResultSet rsType = stType.executeQuery(qType);
                while (rsType.next()) {
            %>
            <option value="<%=rsType.getString("id")%>"><%=rsType.getString("name")%></option>
            <%}%>
        </select></div>
        <div class="row col-sm-6 col-sm-offset-3"><input type="number" class="form-control" name="distance" placeholder="distance within (in miles)"/></div>
        <br>
        <div class="row col-sm-6 col-sm-offset-3">

            <div class="col col-sm-8">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto; transition: 0.5s all; color:white;" type="button">
                                                <span class="glyphicon glyphicon-search">  <b>Search</b></span>
                                            </button>
                                        </span>
            </div>
            <div class="col col-sm-4">
                                        <span class="input-group-btn">

                                            <a class="btn btn-danger col col-sm-12" href="customSearch" style="display:table; margin:0 auto; transition: 0.5s all; color:white;"><span class="glyphicon glyphicon-plus"> &nbsp;<b>More Filters</b></span></a>

                                        </span>
            </div>
        </div>
    </div>
</form>

<%--<!-- Team -->
<div class="team-agile">
    <div class="w3-agile-he">
        <h3>Cars Nearby</h3>
    </div>
    <div class="mis-stage">
        <!-- The element to select and apply miSlider to - the class is optional -->
        <ol class="mis-slider">

            <%

                Statement stNearby = db.connection.createStatement();
                String qNearby = q;
                ResultSet rsNearby = stNearby.executeQuery(qNearby);

                List<Integer> validIndexList = new ArrayList<>();
                int index = 0,validCount=0;

                while (rsNearby.next()) {
                    if(LocationUtils.isDistanceWithinMeters(session.getAttribute("latitude")==null?null:session.getAttribute("latitude").toString(),
                            session.getAttribute("longitude")==null?null:session.getAttribute("longitude").toString(),rsNearby.getString("latitude")
                            ,rsNearby.getString("longitude"), (request.getParameter("distance")!=null&&!request.getParameter("distance").equals(""))? (Float.parseFloat(fixApoS(request.getParameter("distance")))*1.61f) :10000)) {
                        validIndexList.add(index);
                        validCount++;
                    }
                    index++;
                }
                rsNearby.beforeFirst();

                List<Integer> selectedList = WebUtils.getRandomNumbers(validCount, 7);


                index=0;
                int cnt=0;
                while(rsNearby.next() && cnt<7) {
                    if(!validIndexList.contains(index) || !selectedList.contains(validIndexList.indexOf(index))) {
                        index++;
                        continue;
                    }
                    index++;
                    cnt++;
            %>
            <!-- The slider element - the class is optional -->
            <li class="mis-slide">
                <!-- A slide element - the class is optional -->
                <a href="products?category=1" class="mis-container">
                    <!-- A slide container - this element is optional, if absent the plugin adds it automatically -->
                    <figure>
                        <!-- Slide content - whatever you want -->
                        <img src="resources/img/products/<%=rsNearby.getString("ownerId")%>_<%=rsNearby.getString("id")%>.jpg" alt="image" class="" />
                        <figcaption><%=rsNearby.getString("name")%></figcaption>
                        <figcaption>$<%=rsNearby.getString("price")%></figcaption>
                    </figure>
                </a> </li>
            <%}%>
        </ol>
    </div>
</div>
<!-- //Team -->--%>







<section id="team" class="pb-5">
    <div class="container">
        <%--<h5 class="section-title h1">OUR TEAM</h5>--%>
            <%if (!searchStr.isEmpty()) {%>
            <h3 class="well well-lg">
                Search Results of : <%=searchStr%>
            </h3>
            <%}%>
            <%if (!sortByPriceStr.equals("") || !sortByUploadTimeStr.equals("") || !sortByNameStr.equals("")) {%>
            <h3 class="well well-lg">
                <%if(!sortByUploadTimeStr.equals("")){%>
                Sort by : <%=sortByUploadTimeStr.split(" ")[(sortByUploadTimeStr.split(" ").length-1)].equals("asc")?"Recent to Old":"Old to Recent"%>
                <%}%>
                <%if(!sortByPriceStr.equals("")){%>
                Sort by : Price <%=sortByPriceStr.split(" ")[(sortByPriceStr.split(" ").length-1)].equals("asc")?"Low to High":"High to Low"%>
                <%}%>
                <%if(!sortByNameStr.equals("")){%>
                Sort by : Title <%=sortByNameStr.split(" ")[(sortByNameStr.split(" ").length-1)].equals("asc")?"A to Z":"Z to A"%>
                <%}%>
            </h3>
            <%}%>

        <div class="row">

            <div id="map" style="width: 100%; height: 450px;"></div>

            <script>
                function initMap() {
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 6,
                        center: {lat: -34.397, lng: 150.644}
                    });
                    var geocoder = new google.maps.Geocoder();


                        geocodeAddress(geocoder, map);
                }

                function geocodeAddress(geocoder, resultsMap) {

                    <%while (rs.next()){%>
                    var address = '<%=rs.getString("place")%>';
                    geocoder.geocode({'address': address}, function(results, status) {
                        if (status === 'OK') {
                            resultsMap.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: resultsMap,
                                position: results[0].geometry.location,
                                title: '<%=rs.getString("name")%>',

                            });

                            marker.addListener('click', function() {
                                /*resultsMap.setZoom(8);
                                resultsMap.setCenter(marker.getPosition());*/
                                window.location.href='productDetails?sellerId=<%=rs.getString("ownerId")%>&productId=<%=rs.getString("id")%>'
                            });

                        } else {
                            //alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                    <%
                    }
                    rs.beforeFirst();
                    %>

                }
            </script>
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBevTAR-V2fDy9gQsQn1xNHBPH2D36kck0&callback=initMap">
            </script>

            <%--<script type="text/javascript">
                var locations = [
                    ['Bondi Beach', -33.890542, 151.274856, 4],
                    ['Coogee Beach', -33.923036, 151.259052, 5],
                    ['Cronulla Beach', -34.028249, 151.157507, 3],
                    ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
                    ['Maroubra Beach', -33.950198, 151.259302, 1]
                ];

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 10,
                    center: new google.maps.LatLng(-33.92, 151.25),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;

                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
            </script>--%>
        </div>

        <div class="row" >

            <div class="col col-sm-3">
                                        <span class="input-group-btn">

                                            <a class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto;" href="products?sortByUploadTime=<%=(!WebUtils.isNullOrEmpty(request.getParameter("sortByUploadTime"))&&request.getParameter("sortByUploadTime").equals("1"))?-1:1%>"><span class="glyphicon glyphicon-sort"></span> <b>Recently Uploaded</b></a>

                                        </span>
            </div>
            <div class="col col-sm-3">
                                        <span class="input-group-btn">

                                            <a class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto;" href="products?sortByPrice=<%=(!WebUtils.isNullOrEmpty(request.getParameter("sortByPrice"))&&request.getParameter("sortByPrice").equals("1"))?-1:1%>"><span class="glyphicon glyphicon-sort"></span> <b>Price</b></a>

                                        </span>
            </div>
            <div class="col col-sm-3">
                                        <span class="input-group-btn">

                                            <a class="btn btn-danger col col-sm-12" style="display:table; margin:0 auto;" href="products?sortByName=<%=(!WebUtils.isNullOrEmpty(request.getParameter("sortByName"))&&request.getParameter("sortByName").equals("1"))?-1:1%>"><span class="glyphicon glyphicon-sort"></span> <b>A-Z</b></a>

                                        </span>
            </div>


            <%
                int itemNo=0;

                while (rs.next()) {
                    itemNo++;

            %>

            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-4" style="height: 550px;" onclick="location.href='productDetails?sellerId=<%=rs.getString("ownerId")%>&productId=<%=rs.getString("id")%>'">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p style="text-align: center; padding-top: 10px; padding-bottom: 10px; height:240px;"><img style="height: 225px; width: auto; padding-top: 15px; padding-bottom: 25px;" class="img-fluid" src="resources/img/products/<%=rs.getString("ownerId")%>_<%=rs.getString("id")%>.jpg" alt="image"></p>
                                    <h4 class="card-title"><%=rs.getString("name")%></h4>
                                    <br>

                                    <%
                                        for (int i = 0; i < tables.size(); i++) {

                                            String table = tables.get(i);
                                            String tableLabel = tableLabelMap.get(table);

                                            Statement st2 = db.connection.createStatement();
                                            String q2 = "select * from " + table + " where id=" + rs.getString(table + "Id");
                                            ResultSet rs2 = st2.executeQuery(q2);
                                            if (!rs2.next()) {
                                                continue;
                                            }
                                    %>
                                    <p class="card-text" style="padding-left:5px; padding-left:5px; padding-bottom: 5px;"><b><%=tableLabel%>:</b> <%=rs2.getString("name")%></p>
                                    <%}%>


                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br>
                                    <h4 class="card-title"><%=rs.getString("name")%></h4>
                                    <br>
                                    <p class="card-text"><%=rs.getString("details").length()>400?(rs.getString("details").substring(0,400)+"..."):rs.getString("details")%></p>

                                    <a href="productDetails?sellerId=<%=rs.getString("ownerId")%>&productId=<%=rs.getString("id")%>" style="padding-left: 5px;" class="btn btn-primary btn-sm"><%--<i class="fa fa-plus"></i>--%>Full Listing</a>
                                    <a href="profile?sellerId=<%=rs.getString("ownerId")%>" class="btn btn-primary btn-sm"><%--<i class="fa fa-plus"></i>--%>Contact Seller</a>
                                    <%if(session.getAttribute("id")!=null && session.getAttribute("username")!=null
                                            && (session.getAttribute("id").equals(rs.getString("ownerId"))|| Utils.isAdmin(session.getAttribute("username")))){%>
                                    <a href="addProduct?id=<%=rs.getString("id")%>" class="btn btn-primary btn-sm"><%--<i class="fa fa-plus"></i>--%>Update</a>
                                    <a href="profile?deleteId=<%=rs.getString("id")%>" class="btn btn-danger btn-sm"><%--<i class="fa fa-plus"></i>--%>Delete</a>
                                    <%}%>
                                    <br>

                                    <%--<ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                    </ul>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->

            <%}%>
        </div>
        <div class="row">
            <%if(pageNumber>1){%>
            <button class="btn btn-primary" style="padding-left: 50px;padding-right: 50px; text-align: center;" onclick="location.href=window.location.href.replace('&page=<%=pageNumber%>','')+'&page=<%=(pageNumber-1)%>'">Previous Page</button>
            <%}%>
            <%if(nextPageExists){%>
            <button class="btn btn-primary" style="padding-left: 50px;padding-right: 50px; text-align: center;" onclick="location.href=window.location.href.replace('&page=<%=pageNumber%>','')+'&page=<%=(pageNumber+1)%>'">Next Page</button>
            <%}%>
        </div>
    </div>
</section>
<!-- Team -->

<%}%>

    <!-- FOOTER -->
    <%@ include file="footer.jsp" %>
    <!-- END FOOTER -->