<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>


<!-- HEADER -->
<%@ include file="header.jsp" %>
<!-- END HEADER -->

<div class="container-fluid text-center">

<%if(session.getAttribute("errorMsg")!=null && !session.getAttribute("errorMsg").equals("")) {%>
    <div class="alert alert-danger">
        <%=session.getAttribute("successMsg")%>
    </div>
        <%}else{%>
        <div class="alert alert-success">
    <%=session.getAttribute("successMsg")%>
    <ul>
        <li>
            ${isSuccess}
        </li>
        <li>
            ${transaction}
        </li>
        <li>
            ${amount}
        </li>
        <%--<li>
            ${creditCard}
        </li>
        <li>
            ${customer}
        </li>--%>
    </ul>
</div>
        <%}
        session.setAttribute("successMsg", null);
            session.setAttribute("errorMsg", null);
        %>
</div>



<!-- FOOTER -->
<%@ include file="footer.jsp" %>
<!-- END FOOTER -->
