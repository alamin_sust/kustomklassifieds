<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="com.kustomklassifieds.web.util.WebUtils" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.kustomklassifieds.web.util.LocationUtils" %>

<!--// Meta tag Keywords -->

<!-- css files -->
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> <!-- Bootstrap-Core-CSS -->
<link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" /> <!-- Style-CSS -->
<link rel="stylesheet" href="resources/css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
<!-- //css files -->

<!-- online-fonts -->
<link href="//fonts.googleapis.com/css?family=Jockey+One&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Niconne&amp;subset=latin-ext" rel="stylesheet">
<!-- //online-fonts -->


<!-- HEADER -->
<%@ include file="header.jsp" %>
<!-- END HEADER -->

<%
    if(request.getParameter("sellerId")==null || request.getParameter("sellerId").equals("")
            || request.getParameter("productId")==null || request.getParameter("productId").equals("")) {
        response.sendRedirect("home");
    }

    try{
    String q="select * from product where id="+request.getParameter("productId")+" and ownerId="+request.getParameter("sellerId")+"  and is_active=1";
    Statement st = db.connection.createStatement();
    ResultSet rs = st.executeQuery(q);
    rs.next();
%>
<!-- Services -->
<div class="services serv-w3" id="services">
    <h3 class="title-w3"><%=rs.getString("name")%></h3>
    <div class=" col-md-12 section-grid-wthree one">
        <div class="services-info-w3-agileits">
            <h5 class="sub-title">$<%=rs.getString("price")%></h5>
            <p class="para-w3"><b>Date Listed: <%=rs.getString("datetime")%></b></p>
            <p class="para-w3"><b>Location: <%=rs.getString("place")%></b></p>
            <p class="para-w3">Contact Info: <%=rs.getString("contact_info")%></p>
            <p class="para-w3">Zip Code: <%=rs.getString("zip_code")%></p>
            <p class="para-w3" style="text-align: left;"><%=rs.getString("details")%></p>
        </div>
        <div class="services-img-agileits-w3layouts">
            <img style="width: 100%; height: auto;" src="resources/img/products/<%=request.getParameter("sellerId")%>_<%=request.getParameter("productId")%>.jpg" alt="service-img">
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<!-- //Services -->


<!-- Gallery -->
<div class="gallery">
    <div class="container">
        <h2>Gallery</h2>
        <div class="wthree_gallery_grids">
            <div id="jzBox" class="jzBox">
                <div id="jzBoxNextBig"></div>
                <div id="jzBoxPrevBig"></div>
                <img style="" src="#" id="jzBoxTargetImg" alt=" " />
                <div id="jzBoxBottom">
                    <div id="jzBoxTitle"></div>
                    <div id="jzBoxMoreItems">
                        <div id="jzBoxCounter"></div>
                        <i class="arrow-left" id="jzBoxPrev"></i>
                        <i class="arrow-right" id="jzBoxNext"></i>
                    </div>
                    <i class="close" id="jzBoxClose"></i>
                </div>
            </div>
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">

                        <%for(int i=0;i<rs.getInt("image_count");i++) {%>

                        <%if((i%4)==0){%>
                        <div class="tab_img">
                        <%}%>
                            <div class="col-md-3 agile_gallery_grids" style="height:250px">
                                <%
                                String postfix="";
                                if(i!=0) {
                                    postfix="-"+(i+1);
                                }
                                %>
                                <a href="resources/img/products/<%=request.getParameter("sellerId")%>_<%=request.getParameter("productId")%><%=postfix%>.jpg" class="jzBoxLink" title="<%=rs.getString("name")%>">
                                    <div class="view view-sixth">
                                        <img style="width: 100%; height: auto;" src="resources/img/products/<%=request.getParameter("sellerId")%>_<%=request.getParameter("productId")%><%=postfix%>.jpg" alt=" " class="img-responsive" />
                                        <div class="mask">
                                            <p>Click to open slideshow.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <%if(((i%4)==3)||(i+1)==rs.getInt("image_count")) {%>
                            <div class="clearfix"> </div>
                        </div>
                        <%}%>

                        <%}%>
                        <div class="btn btn-default" style="display: block; margin: 20px auto;"><a href="profile?sellerId=<%=request.getParameter("sellerId")%>"><b>Contact Seller</b></a></div>
                        <%if(session.getAttribute("id")!=null && session.getAttribute("id").equals(rs.getString("ownerId"))){%>
                        <div class="btn btn-default" style="display: block; margin: 0 auto;"><a href="addProduct?id=<%=rs.getString("id")%>"><b>Update</b></a></div>
                        <div class="btn btn-default" style="display: block; margin: 0 auto;"><a href="profile?deleteId=<%=rs.getString("id")%>"><b>Delete</b></a></div>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //Gallery -->
<%
    }catch (Exception e) { System.out.println(e);} finally {

    }

%>
<!-- js-scripts -->
<!-- js -->
<script type="text/javascript" src="resources/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script src="resources/js/jzBox.js"></script>
<!-- //js-scripts -->

<!-- FOOTER -->
<%@ include file="footer.jsp" %>
<!-- END FOOTER -->
