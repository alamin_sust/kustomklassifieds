<%@page import="com.kustomklassifieds.web.util.Constants" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@ page import="static com.kustomklassifieds.web.util.WebUtils.fixApoS" %>
<%@ page import="com.kustomklassifieds.Util.EncryptionUtil" %>
<%@ page import="javax.rmi.CORBA.Util" %>
<%@ page import="com.kustomklassifieds.Util.Utils" %>

<!-- HEADER -->
<%@ include file="header.jsp" %>
<!-- END HEADER -->

<%
    if (session.getAttribute("id") == null || session.getAttribute("id").toString().equals("")) {
        response.sendRedirect("loginRegister");
    }

    try {

        String qU ="";



        if(request.getParameter("editProfile1")!=null && !request.getParameter("editProfile1").equals("")) {

            qU+= "name='"+fixApoS(request.getParameter("name"))
                    +"',phone='"+fixApoS(request.getParameter("phone"))
                    +"',mobile='"+fixApoS(request.getParameter("mobile"))+"'";
        } else if(request.getParameter("editProfile2")!=null && !request.getParameter("editProfile2").equals("")) {
            qU+="email='"+fixApoS(request.getParameter("email"))+"'";
        } else if(request.getParameter("editProfile3")!=null && !request.getParameter("editProfile3").equals("")) {
            qU+="facebook='"+fixApoS(request.getParameter("facebook"))
                    +"',instagram='"+fixApoS(request.getParameter("instagram"))+"',twitter='"+fixApoS(request.getParameter("twitter"))+"'";
        } else if(request.getParameter("editProfile4")!=null && !request.getParameter("editProfile4").equals("")) {
            qU+="bio='"+fixApoS(request.getParameter("bio"))+"'";
        } else if(request.getParameter("editProfile5")!=null && !request.getParameter("editProfile5").equals("")) {
            qU+="address='"+fixApoS(request.getParameter("address"))+"'";
        } else if(request.getParameter("editProfile6")!=null && !request.getParameter("editProfile6").equals("")) {

            String cPass = request.getParameter("currentPassword");
            String pass1 = request.getParameter("password1");
            String pass2 = request.getParameter("password2");

            Statement st4 = db.connection.createStatement();
            String q4 = "select * from user where id="+session.getAttribute("id");
            ResultSet rs4=st4.executeQuery(q4);
            rs4.next();

            if(!EncryptionUtil.matchWithSecuredHash(cPass, rs4.getString("password"))) {
                session.setAttribute("errorMsg","Incorrect Password! Please Try Again.");
            } else if(!pass1.equals(pass2)) {
                session.setAttribute("errorMsg","Password Doesn't Match! Please Try Again.");
            } else {
                Statement st5 = db.connection.createStatement();
                String q5 = "update user set password='"+EncryptionUtil.generateSecuredHash(pass1)+"' where id="+session.getAttribute("id");
                st5.executeUpdate(q5);
                session.setAttribute("successMsg","Updated Successfully!");
            }

        }

        String profileId= request.getParameter("sellerId")!=null?fixApoS(request.getParameter("sellerId")):session.getAttribute("id").toString();

        if(!qU.equals("")) {
            Statement stU = db.connection.createStatement();
            qU = "update user set " + qU + " where id=" + profileId;
            stU.executeUpdate(qU);
            session.setAttribute("successMsg","Updated Successfully!");

        }

        Statement st1 = db.connection.createStatement();
        String q1 = "";
        ResultSet rs1;
        Statement st2 = db.connection.createStatement();
        String q2 = "";
        ResultSet rs2;
        Statement st3 = db.connection.createStatement();
        String q3 = "";
        ResultSet rs3;

        if(request.getParameter("deleteId")!=null && !request.getParameter("deleteId").equals("")) {
            q3="update product set is_active=0 where id="+request.getParameter("deleteId");
            if(!(session.getAttribute("username")!=null && Utils.isAdmin(session.getAttribute("username")))) {
                q3+=" and ownerId="+session.getAttribute("id");
            }
            st3.executeUpdate(q3);
            session.setAttribute("successMsg","Deleted Successfully!");
        }



        q1 = "select * from user where id=" + profileId;
        rs1 = st1.executeQuery(q1);
        rs1.next();

%>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">

        </div>


        <hr class="">
        <div class="container target">
            <%if (session.getAttribute("successMsg") != null) {%>
            <div class="alert alert-success">
                <Strong><%=session.getAttribute("successMsg")%>
                </Strong>
            </div>
            <%}%>
            <%if (session.getAttribute("errorMsg") != null) {%>
            <div class="alert alert-danger">
                <Strong><%=session.getAttribute("errorMsg")%>
                </Strong>
            </div>
            <%
                }
                session.setAttribute("successMsg", null);
                session.setAttribute("errorMsg", null);

            %>
            <div class="row">
                <div class="col-sm-10">
                    <h1 class=""><%=rs1.getString("name") + "(" + rs1.getString("username") + ")"%>
                    </h1>
                    <br>

                    <%if(request.getParameter("sellerId")!=null && !request.getParameter("sellerId").equals("")) {%>
                    <a class="btn btn-success" href="chat?chatTo=<%=profileId%>"><b>Chat with me</b></a> <%--<button type="button" class="btn btn-info"><b>Send me a message</b></button>--%>
                    <br>
                    <%}%>
                </div>
                <div class="col-sm-2"><%--<a href="#" class="pull-right"><img title="profile image"
                                                                          class="img-circle img-responsive"
                                                                          src="http://www.rlsandbox.com/img/profile.jpg"></a>--%>

                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-3">
                    <!--left col-->


                    <ul class="list-group">
                        <li class="list-group-item text-muted" contenteditable="false">Profile
                            <a data-toggle="modal" data-target="#myModal1"><span class="fa fa-edit" style="float: right;"/></a>
                        </li>
                        <li class="list-group-item text-right"><span class="pull-left"><strong
                                class="">Phone</strong></span> <%=rs1.getString("phone")%>
                        </li>
                        <li class="list-group-item text-right"><span class="pull-left"><strong
                                class="">Mobile</strong></span> <%=rs1.getString("mobile")%>
                        </li>
                        <li class="list-group-item text-right"><span class="pull-left"><strong
                                class="">Real name</strong></span><%=rs1.getString("name")%>
                        </li>
                    </ul>

                    <div class="panel panel-default">
                        <div class="panel-heading">Username <i class="fa fa-link fa-1x"></i>


                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item"><%=rs1.getString("username")%></li>
                                <li class="list-group-item"><a data-toggle="modal" data-target="#myModal6" style="cursor: pointer;">change password</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Email <i class="fa fa-link fa-1x"></i>
                            <a data-toggle="modal" data-target="#myModal2"><span class="fa fa-edit" style="float: right;"/></a>

                        </div>
                        <div class="panel-body"><a href="mailto:<%=rs1.getString("email")%>" class=""><%=rs1.getString("email")%></a>

                        </div>
                    </div>

                    <%
                        Statement stSell=db.connection.createStatement();
                        String qSell="select count(*) as cnt from deal where seller_id="+profileId;
                        ResultSet rsSell = stSell.executeQuery(qSell);
                        rsSell.next();
                    Statement stBuy=db.connection.createStatement();
                    String qBuy="select count(*) as cnt from deal where seller_id="+profileId;
                    ResultSet rsBuy = stBuy.executeQuery(qBuy);
                    rsBuy.next();
                    %>
                    <ul class="list-group">
                        <li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i>

                        </li>
                        <li class="list-group-item text-right"><span class="pull-left"><strong
                                class="">Sells</strong></span> <%=rsSell.getString("cnt")%>
                        </li>
                        <li class="list-group-item text-right"><span class="pull-left"><strong
                                class="">Buys</strong></span> <%=rsBuy.getString("cnt")%>
                        </li>
                    </ul>
                    <div class="panel panel-default">
                        <div class="panel-heading">Social Media
                            <a data-toggle="modal" data-target="#myModal3"><span class="fa fa-edit" style="float: right;"/></a>
                        </div>
                        <div class="panel-body">
                            <a class="fa fa-facebook fa-2x" href="https://www.facebook.com/<%=rs1.getString("facebook")%>"></a>
                            <a class="fa fa-instagram fa-2x" href="https://www.instagram.com/<%=rs1.getString("instagram")%>"></a>
                            <a class="fa fa-twitter fa-2x" href="https://www.twitter.com/<%=rs1.getString("twitter")%>"></a>

                        </div>
                    </div>
                </div>
                <!--/col-3-->
                <div class="col-sm-9" style="" contenteditable="false">
                    <div class="panel panel-default">
                        <div class="panel-heading"><%=rs1.getString("name")%>'s Bio
                            <a data-toggle="modal" data-target="#myModal4"><span class="fa fa-edit" style="float: right;"/></a>
                        </div>
                        <div class="panel-body"> <%=rs1.getString("bio")%>

                        </div>
                    </div>
                    <%
                        q2 = "select * from product where ownerId=" + rs1.getString("id")+" and is_active=1";
                        rs2 = st2.executeQuery(q2);

                    %>

                    <div class="panel panel-default target">
                        <div class="panel-heading" contenteditable="false">Cars I Own</div>
                        <div class="panel-body">
                            <% int cnt = 0;
                                while (rs2.next()) {

                                    if ((cnt % 3) == 0) {
                            %>
                            <div class="row">
                                <%}%>
                                <div class="col-md-4">
                                    <div class="thumbnail" style="height:500px;">
                                        <img alt="300x200"
                                             src="resources/img/products/<%=rs2.getString("ownerId")%>_<%=rs2.getString("id")%>.jpg">
                                        <div class="caption">
                                            <h3>
                                                <%=rs2.getString("name")%>
                                            </h3>
                                            <p>
                                                <b style="color: red;">$<%=rs2.getString("price")%></b>
                                            </p>
                                            <p>
                                                <a href="productDetails?sellerId=<%=rs2.getString("ownerId")%>&productId=<%=rs2.getString("id")%>"><b>Details</b></a>
                                            </p>
                                            <p>
                                                <a href="addProduct?id=<%=rs2.getString("id")%>"><b>Update</b></a>
                                            </p>
                                            <p>
                                                <a href="profile?deleteId=<%=rs2.getString("id")%>"><b>Delete</b></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <%if ((cnt % 3) == 2) {%>
                            </div>
                            <%
                                    }
                                    cnt++;
                                }
                                if ((cnt % 3) != 0) {
                            %>
                        </div>
                        <%}%>
                        <%if(session.getAttribute("username")!=null && !session.getAttribute("username").equals("")) {%>
                        <button class="btn btn-success" onclick="location.href = 'addProduct'"><b>Add a Vehicle</b></button>
                        <%}%>

                    </div>

                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"><%=rs1.getString("name")%>'s Address
                        <a data-toggle="modal" data-target="#myModal5"><span class="fa fa-edit" style="float: right;"/></a>
                    </div>
                    <div class="panel-body"><%=rs1.getString("address")%>

                    </div>
                </div>
            </div>


            <div id="push"></div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="myModal1" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <%
                    /*Statement stModal = db.connection.createStatement();
                    String qModal = "select * from user where id="+session.getAttribute("id");
                    ResultSet rsModal = stModal.executeQuery(qModal);
                    rsModal.next();*/

                    %>
                    <form method="post" action="profile">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Edit Profile Details</b></h4>
                        </div>
                        <div class="modal-body form-group">
                            Name <input class="form-control" name="name" type="text" value="<%=rs1.getString("name")%>"/><br>
                            Phone <input class="form-control" name="phone" type="text" value="<%=rs1.getString("phone")%>"/><br>
                            Mobile <input class="form-control" name="mobile" type="text" value="<%=rs1.getString("mobile")%>"/><br>
                            <input type="hidden" name="editProfile1" value="true"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><b>Change</b></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="modal fade" id="myModal6" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <%
                        /*Statement stModal = db.connection.createStatement();
                        String qModal = "select * from user where id="+session.getAttribute("id");
                        ResultSet rsModal = stModal.executeQuery(qModal);
                        rsModal.next();*/

                    %>
                    <form method="post" action="profile">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Change Password</b></h4>
                        </div>
                        <div class="modal-body form-group">
                            Current Password <input class="form-control" name="currentPassword" type="password" required/><br>
                            New Password <input class="form-control" name="password1" type="password" required/><br>
                            Confirm New Password <input class="form-control" name="password2" type="password" required/><br>
                            <input type="hidden" name="editProfile6" value="true"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><b>Change</b></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal2" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <%
                        /*Statement stModal = db.connection.createStatement();
                        String qModal = "select * from user where id="+session.getAttribute("id");
                        ResultSet rsModal = stModal.executeQuery(qModal);
                        rsModal.next();*/

                    %>
                    <form method="post" action="profile">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Edit Profile Details</b></h4>
                        </div>
                        <div class="modal-body form-group">
                            Email <input class="form-control" name="email" type="text" value="<%=rs1.getString("email")%>"/><br>
                            <input type="hidden" name="editProfile2" value="true"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><b>Change</b></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <%
                        /*Statement stModal = db.connection.createStatement();
                        String qModal = "select * from user where id="+session.getAttribute("id");
                        ResultSet rsModal = stModal.executeQuery(qModal);
                        rsModal.next();*/

                    %>
                    <form method="post" action="profile">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Edit Profile Details</b></h4>
                        </div>
                        <div class="modal-body form-group">
                            Facebook Id <input class="form-control" name="facebook" type="text" value="<%=rs1.getString("facebook")%>"/><br>
                            Instagram Id <input class="form-control" name="instagram" type="text" value="<%=rs1.getString("instagram")%>"/><br>
                            Twitter Id <input class="form-control" name="twitter" type="text" value="<%=rs1.getString("twitter")%>"/><br>
                            <input type="hidden" name="editProfile3" value="true"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><b>Change</b></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="modal fade" id="myModal4" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <%
                        /*Statement stModal = db.connection.createStatement();
                        String qModal = "select * from user where id="+session.getAttribute("id");
                        ResultSet rsModal = stModal.executeQuery(qModal);
                        rsModal.next();*/

                    %>
                    <form method="post" action="profile">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Edit Profile Details</b></h4>
                        </div>
                        <div class="modal-body form-group">
                            Bio <input class="form-control" name="bio" type="text" value="<%=rs1.getString("bio")%>"/><br>
                            <input type="hidden" name="editProfile4" value="true"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><b>Change</b></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="modal fade" id="myModal5" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <%
                        /*Statement stModal = db.connection.createStatement();
                        String qModal = "select * from user where id="+session.getAttribute("id");
                        ResultSet rsModal = stModal.executeQuery(qModal);
                        rsModal.next();*/

                    %>
                    <form method="post" action="profile">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Edit Profile Details</b></h4>
                        </div>
                        <div class="modal-body form-group">
                            Address <input class="form-control" name="address" type="text" value="<%=rs1.getString("address")%>"/><br>
                            <input type="hidden" name="editProfile5" value="true"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><b>Change</b></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>


        <script src="/plugins/bootstrap-select.min.js"></script>
        <script src="/codemirror/jquery.codemirror.js"></script>
        <script src="/beautifier.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-40413119-1', 'bootply.com');
            ga('send', 'pageview');
        </script>
        <script>
            jQuery.fn.shake = function (intShakes, intDistance, intDuration, foreColor) {
                this.each(function () {
                    if (foreColor && foreColor != "null") {
                        $(this).css("color", foreColor);
                    }
                    $(this).css("position", "relative");
                    for (var x = 1; x <= intShakes; x++) {
                        $(this).animate({left: (intDistance * -1)}, (((intDuration / intShakes) / 4)))
                            .animate({left: intDistance}, ((intDuration / intShakes) / 2))
                            .animate({left: 0}, (((intDuration / intShakes) / 4)));
                        $(this).css("color", "");
                    }
                });
                return this;
            };
        </script>
        <script>
            $(document).ready(function () {

                $('.tw-btn').fadeIn(3000);
                $('.alert').delay(5000).fadeOut(1500);

                $('#btnLogin').click(function () {
                    $(this).text("...");
                    $.ajax({
                        url: "/loginajax",
                        type: "post",
                        data: $('#formLogin').serialize(),
                        success: function (data) {
                            //console.log('data:'+data);
                            if (data.status == 1 && data.user) { //logged in
                                $('#menuLogin').hide();
                                $('#lblUsername').text(data.user.username);
                                $('#menuUser').show();
                                /*
                                 $('#completeLoginModal').modal('show');
                                 $('#btnYes').click(function() {
                                 window.location.href="/";
                                 });
                                 */
                            } else {
                                $('#btnLogin').text("Login");
                                prependAlert("#spacer", data.error);
                                $('#btnLogin').shake(4, 6, 700, '#CC2222');
                                $('#username').focus();
                            }
                        },
                        error: function (e) {
                            $('#btnLogin').text("Login");
                            console.log('error:' + JSON.stringify(e));
                        }
                    });
                });
                $('#btnRegister').click(function () {
                    $(this).text("Wait..");
                    $.ajax({
                        url: "/signup?format=json",
                        type: "post",
                        data: $('#formRegister').serialize(),
                        success: function (data) {
                            console.log('data:' + JSON.stringify(data));
                            if (data.status == 1) {
                                $('#btnRegister').attr("disabled", "disabled");
                                $('#formRegister').text('Thanks. You can now login using the Login form.');
                            } else {
                                prependAlert("#spacer", data.error);
                                $('#btnRegister').shake(4, 6, 700, '#CC2222');
                                $('#btnRegister').text("Sign Up");
                                $('#inputEmail').focus();
                            }
                        },
                        error: function (e) {
                            $('#btnRegister').text("Sign Up");
                            console.log('error:' + e);
                        }
                    });
                });

                $('.loginFirst').click(function () {
                    $('#navLogin').trigger('click');
                    return false;
                });

                $('#btnForgotPassword').on('click', function () {
                    $.ajax({
                        url: "/resetPassword",
                        type: "post",
                        data: $('#formForgotPassword').serializeObject(),
                        success: function (data) {
                            if (data.status == 1) {
                                prependAlert("#spacer", data.msg);
                                return true;
                            } else {
                                prependAlert("#spacer", "Your password could not be reset.");
                                return false;
                            }
                        },
                        error: function (e) {
                            console.log('error:' + e);
                        }
                    });
                });

                $('#btnContact').click(function () {

                    $.ajax({
                        url: "/contact",
                        type: "post",
                        data: $('#formContact').serializeObject(),
                        success: function (data) {
                            if (data.status == 1) {
                                prependAlert("#spacer", "Thanks. We got your message and will get back to you shortly.");
                                $('#contactModal').modal('hide');
                                return true;
                            } else {
                                prependAlert("#spacer", data.error);
                                return false;
                            }
                        },
                        error: function (e) {
                            console.log('error:' + e);
                        }
                    });
                    return false;
                });

                /*
                 $('.nav .dropdown-menu input').on('click touchstart',function(e) {
                 e.stopPropagation();
                 });
                 */


            });
            $.fn.serializeObject = function () {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
            var prependAlert = function (appendSelector, msg) {
                $(appendSelector).after('<div class="alert alert-info alert-block affix" id="msgBox" style="z-index:1300;margin:14px!important;">' + msg + '</div>');
                $('.alert').delay(3500).fadeOut(1000);
            }
        </script>
        <!-- Quantcast Tag -->
        <script type="text/javascript">
            var _qevents = _qevents || [];

            (function () {
                var elem = document.createElement('script');
                elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
                elem.async = true;
                elem.type = "text/javascript";
                var scpt = document.getElementsByTagName('script')[0];
                scpt.parentNode.insertBefore(elem, scpt);
            })();

            _qevents.push({
                qacct: "p-0cXb7ATGU9nz5"
            });
        </script>
        <noscript>
            &amp;amp;amp;amp;amp;amp;amp;lt;div style="display:none;"&amp;amp;amp;amp;amp;amp;amp;gt;
            &amp;amp;amp;amp;amp;amp;amp;lt;img src="//pixel.quantserve.com/pixel/p-0cXb7ATGU9nz5.gif" border="0"
            height="1" width="1" alt="Quantcast"/&amp;amp;amp;amp;amp;amp;amp;gt;
            &amp;amp;amp;amp;amp;amp;amp;lt;/div&amp;amp;amp;amp;amp;amp;amp;gt;
        </noscript>
        <!-- End Quantcast tag -->
        <div id="completeLoginModal" class="modal hide">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                <h3>Do you want to proceed?</h3>
            </div>
            <div class="modal-body">
                <p>This page must be refreshed to complete your login.</p>
                <p>You will lose any unsaved work once the page is refreshed.</p>
                <br><br>
                <p>Click "No" to cancel the login process.</p>
                <p>Click "Yes" to continue...</p>
            </div>
            <div class="modal-footer">
                <a href="#" id="btnYes" class="btn danger"><b>Yes, complete login</b></a>
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn secondary"><b>No</b></a>
            </div>
        </div>
        <div id="forgotPasswordModal" class="modal hide">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                <h3>Password Lookup</h3>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal" id="formForgotPassword">
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Email</label>
                        <div class="controls">
                            <input name="_csrf" id="token" value="CkMEALL0JBMf5KSrOvu9izzMXCXtFQ/Hs6QUY=" type="hidden">
                            <input name="email" id="inputEmail" placeholder="you@youremail.com" required=""
                                   type="email">
                            <span class="help-block"><small>Enter the email address you used to sign-up.</small></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer pull-center">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn"><b>Cancel</b></a>
                <a href="#" data-dismiss="modal" id="btnForgotPassword" class="btn btn-success"><b>Reset Password</b></a>
            </div>

        </div>
        <div id="upgradeModal" class="modal hide">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                <h4>Would you like to upgrade?</h4>
            </div>
            <div class="modal-body">
                <p class="text-center"><strong></strong></p>
                <h1 class="text-center">$4
                    <small>/mo</small>
                </h1>
                <p class="text-center">
                    <small>Unlimited plys. Unlimited downloads. No Ads.</small>
                </p>
                <p class="text-center"><img src="/assets/i_visa.png" alt="visa" width="50"> <img src="/assets/i_mc.png"
                                                                                                 alt="mastercard"
                                                                                                 width="50"> <img
                        src="/assets/i_amex.png" alt="amex" width="50"> <img src="/assets/i_discover.png" alt="discover"
                                                                             width="50"> <img src="/assets/i_paypal.png"
                                                                                              alt="paypal" width="50">
                </p>
            </div>
            <div class="modal-footer pull-center">
                <a href="/upgrade" class="btn btn-block btn-huge btn-success"><strong>Upgrade Now</strong></a>
                <a href="#" data-dismiss="modal" class="btn btn-block btn-huge"><b>No Thanks, Maybe Later</b></a>
            </div>
        </div>
        <div id="contactModal" class="modal hide">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                <h3>Contact Us</h3>
                <p>suggestions, questions or feedback</p>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal" id="formContact">
                    <input name="_csrf" id="token" value="CkMEALL0JBMf5KSrOvu9izzMXCXtFQ/Hs6QUY=" type="hidden">
                    <div class="control-group">
                        <label class="control-label" for="inputSender">Name</label>
                        <div class="controls">
                            <input name="sender" id="inputSender" class="input-large" placeholder="Your name"
                                   type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputMessage">Message</label>
                        <div class="controls">
                            <textarea name="notes" rows="5" id="inputMessage" class="input-large"
                                      placeholder="Type your message here"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Email</label>
                        <div class="controls">
                            <input name="email" id="inputEmail" class="input-large"
                                   placeholder="you@youremail.com (for reply)" required="" type="text">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer pull-center">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn"><b>Cancel</b></a>
                <a href="#" data-dismiss="modal" aria-hidden="true" id="btnContact" role="button"
                   class="btn btn-success"><b>Send</b></a>
            </div>
        </div>
        <script src="/plugins/bootstrap-pager.js"></script>
    </div>
    <div class="col-sm-2 sidenav">

    </div>
</div>
</div>

<%
    } catch (Exception e) {
        System.out.println(e);
    } finally {
        db.close();
    }


%>
<!-- FOOTER -->
<%@ include file="footer.jsp" %>
<!-- END FOOTER -->

