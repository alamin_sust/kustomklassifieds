<%@page import="com.kustomklassifieds.connection.Database"%>
<%@ page import="static com.kustomklassifieds.web.util.WebUtils.fixApoS" %>
<%@ page import="com.kustomklassifieds.Util.Utils" %>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118253363-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-118253363-1');
        </script>
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyBevTAR-V2fDy9gQsQn1xNHBPH2D36kck0&sensor=false"
                type="text/javascript"></script>

        <title>Kustom Klassifieds</title>

        <!-- Meta tag Keywords -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Auto Car Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />


        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <!-- css files -->
        <link href="resources/css/mislider.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/mislider-custom.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> <!-- Bootstrap-Core-CSS -->
        <link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" /> <!-- Style-CSS -->
        <link rel="stylesheet" href="resources/css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
        <!-- //css files -->

        <!-- online-fonts -->
        <link href="//fonts.googleapis.com/css?family=Jockey+One&amp;subset=latin-ext" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Niconne&amp;subset=latin-ext" rel="stylesheet">
        <!-- //online-fonts -->

        <style>
            /*#team {
                background: #eee !important;
            }*/

            .btn-primary:hover,
            .btn-primary:focus {
                border: none;
                background-color: #0d0d4a;
                border-color: #108d6f;
                box-shadow: none;
                outline: none;
            }

            .btn-primary {
                border: none;
                color: #fff;
                background-color: #00e0c3;
                border-color: #007b5e;
            }

            section {
                padding: 60px 0;
            }

            section .section-title {
                text-align: center;
                color: #007b5e;
                margin-bottom: 50px;
                text-transform: uppercase;
            }

            #team .card {
                border: none;
                background: #ffffff;
            }

            .image-flip:hover .backside,
            .image-flip.hover .backside {
                -webkit-transform: rotateY(0deg);
                -moz-transform: rotateY(0deg);
                -o-transform: rotateY(0deg);
                -ms-transform: rotateY(0deg);
                transform: rotateY(0deg);
                border-radius: .25rem;
            }

            .image-flip:hover .frontside,
            .image-flip.hover .frontside {
                -webkit-transform: rotateY(180deg);
                -moz-transform: rotateY(180deg);
                -o-transform: rotateY(180deg);
                transform: rotateY(180deg);
            }

            .mainflip {
                -webkit-transition: 1s;
                -webkit-transform-style: preserve-3d;
                -ms-transition: 1s;
                -moz-transition: 1s;
                -moz-transform: perspective(1000px);
                -moz-transform-style: preserve-3d;
                -ms-transform-style: preserve-3d;
                transition: 1s;
                transform-style: preserve-3d;
                position: relative;
            }

            .frontside {
                position: relative;
                -webkit-transform: rotateY(0deg);
                -ms-transform: rotateY(0deg);
                z-index: 2;
                margin-bottom: 30px;
            }

            .backside {
                position: absolute;
                top: 0;
                left: 0;
                background: white;
                -webkit-transform: rotateY(-180deg);
                -moz-transform: rotateY(-180deg);
                -o-transform: rotateY(-180deg);
                -ms-transform: rotateY(-180deg);
                transform: rotateY(-180deg);
                -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
                -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
                box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            }

            .frontside,
            .backside {
                -webkit-backface-visibility: hidden;
                -moz-backface-visibility: hidden;
                -ms-backface-visibility: hidden;
                backface-visibility: hidden;
                -webkit-transition: 1s;
                -webkit-transform-style: preserve-3d;
                -moz-transition: 1s;
                -moz-transform-style: preserve-3d;
                -o-transition: 1s;
                -o-transform-style: preserve-3d;
                -ms-transition: 1s;
                -ms-transform-style: preserve-3d;
                transition: 1s;
                transform-style: preserve-3d;
            }

            .frontside .card,
            .backside .card {
                min-height: 312px;
            }

            .backside .card a {
                font-size: 18px;
                color: #ffffff !important;
            }

            .frontside .card .card-title,
            .backside .card .card-title {
                color: #007b5e !important;
            }

            .frontside .card .card-body img {
                width: 250px;
                /*height: 180px;*/
                /*border-radius: 50%;*/
            }
        </style>
    </head>

    <body>
        <%

            Database db = new Database();
            db.connect();

             try {

            if(request.getParameter("subscribeEmail") != null && !request.getParameter("subscribeEmail").equals("")) {

            Statement stSubscribe = db.connection.createStatement();
            String qSubscribe = "insert into subscribe (email) values('"+fixApoS(request.getParameter("subscribeEmail"))+"')";
            stSubscribe.executeUpdate(qSubscribe);
            session.setAttribute("successMsg", "Successfully Subscribed!");
            }


        %>
        <!-- banner -->
        <div class="banner wthree">
            <div class="container">
                <div class="banner_top">
                    <div class="logo wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                        <h1><a class="navbar-brand" style="width:200px;" href="home"><img src="resources/img/logo-white-t.png" style="width: 150px; height: 35px;"/></a></h1>
                    </div>
                    <div class="banner_top_right wow fadeInRight animated animated" data-wow-delay=".5s" style="width: 100%; widthvisibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                        <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" style="    margin: 20px;" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only"><b>Toggle navigation</b></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav cl-effect-14" >
                                    <li><a href="home">Home</a></li>

                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Vehicles <span class="caret"></span></a>
                                        <ul class="dropdown-menu" >
                                            <li><a href="products?category=1" style="text-align:center; color: #00E0C5;">Buy</a></li>
                                            <%if(session.getAttribute("username")!=null && !session.getAttribute("username").toString().equals("")){%>
                                            <li><a href="addProduct" style="text-align:center;color: #00E0C5;">Sell</a></li>
                                            <%} else {%>
                                            <li><a href="loginRegister" style="text-align:center;color: #00E0C5;">Sell</a></li>
                                            <%}%>
                                        </ul>
                                    </li>

                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Info <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="termsOfService" style="text-align:center;color: #00E0C5;">Terms of Service</a></li>
                                            <li><a href="aboutUs" style="text-align:center;color: #00E0C5;">About Us</a></li>
                                            <li><a href="legal" style="text-align:center;color: #00E0C5;">Legal</a></li>
                                            <%--<li><a href="contactUs">Contact Us</a></li>--%>
                                        </ul>
                                    </li>

                                    <%--<li><a href="chat">Messages</a></li>--%>

                                        <%if (session.getAttribute("username") == null) {%>
                                    <li><a href="loginRegister"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                                        <%} else {%>
                                    <%if(session.getAttribute("username").toString().equals("admin")){%>
                                    <li><a href="products">All Vehicles</a></li>
                                    <%}else{%>
                                    <li><a href="products?myVehicles=true">My Vehicles</a></li>
                                    <%}%>

                                    <%if(Utils.isAdmin(session.getAttribute("username"))) {%>
                                    <li><a href="createDiscountCoupon">Create Discount Coupon</a></li>
                                    <%}%>

                                    <li><a href="profile">logged in as: <strong><%=session.getAttribute("username")%></strong></a></li>

                                    <li><a href="loginRegister?logout=true"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                                    <%}%>
                                </ul>
                            </div><!-- /.navbar-collapse -->	

                        </nav>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <!-- banner -->