<%@page import="com.kustomklassifieds.web.util.Statics"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.kustomklassifieds.connection.Database"%>
<%@ page import="java.util.Locale" %>
<%@ page import="com.kustomklassifieds.web.util.LocationUtils" %>
<%@ page import="com.kustomklassifieds.web.util.EmailUtil" %>
<%@ page import="java.util.Random" %>
<%@ page import="static com.kustomklassifieds.web.util.WebUtils.fixApoS" %>
<%@ page import="com.kustomklassifieds.Util.EncryptionUtil" %>
<%@ page import="java.security.spec.ECField" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>Login and Registration Form with HTML5 and CSS3</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="resources/css/loginRegister.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <%

            Database db = new Database();
            db.connect();

            try {
            session.setAttribute("username", null);
            session.setAttribute("id", null);
                session.setAttribute("latitude", null);
                session.setAttribute("longitude", null);
            
            session.setAttribute("successMsg", null);
            session.setAttribute("errorMsg", null);

            if(request.getParameter("logout")!=null && !request.getParameter("logout").equals("")) {
                response.sendRedirect("home");
            }

            Statement st1 = db.connection.createStatement();
            String q1 = "";
            ResultSet rs1;
            Statement st2 = db.connection.createStatement();
            String q2 = "";
            ResultSet rs2;
            Statement st3 = db.connection.createStatement();
            String q3 = "";
            ResultSet rs3;

            String successMsg = "";
            String errorMsg = "";

            String type = fixApoS(request.getParameter("type"));

            if (type != null) {
                String username = fixApoS(request.getParameter("username"));
                String password = fixApoS(request.getParameter("password"));

                q1 = "select * from user where username='" + username + "' or email='"+fixApoS(request.getParameter("email"))+"'";
                rs1 = st1.executeQuery(q1);

                if (type.equals("forgotPassConfirm")) {
                    Statement stPRC = db.connection.createStatement();
                    if(rs1.next() && rs1.getString("pass_reset_key")!=null && rs1.getString("pass_reset_key").equals(fixApoS(request.getParameter("passResetKey")))) {
                        if(fixApoS(request.getParameter("password")).equals(fixApoS(request.getParameter("password2")))) {

                            Statement stPR = db.connection.createStatement();
                            String qPR = "update user set pass_reset_key=null, password='" + EncryptionUtil.generateSecuredHash(fixApoS(request.getParameter("password"))) + "' where email='" + fixApoS(request.getParameter("email")) + "'";
                            stPR.executeUpdate(qPR);

                            session.setAttribute("username", rs1.getString("username"));
                            session.setAttribute("id", rs1.getString("id"));


                            String ip= LocationUtils.getIpByRequest(request);
                            String location = LocationUtils.getLocationByIp(ip);
                            session.setAttribute("latitude", location.split("_")[0]);
                            session.setAttribute("longitude", location.split("_")[1]);

                            session.setAttribute("successMsg", Statics.PASSWORD_RESET_SUCCESS);
                            response.sendRedirect("profile");
                        } else {
                            session.setAttribute("errorMsg",Statics.REGISTER_PASSWORD_MISSMATCH);
                        }
                    } else {
                        session.setAttribute("errorMsg",Statics.LINK_EXPIRED);
                    }
                }
                else if (type.equals("forgotPass")) {
                    if(rs1.next()) {
                        Random random = new Random();
                        int key = Math.abs(random.nextInt()) % 100000000;

                        Statement stPR = db.connection.createStatement();
                        String qPR = "update user set pass_reset_key='"+key+"' where email='"+fixApoS(request.getParameter("email"))+"'";
                        stPR.executeUpdate(qPR);

                        EmailUtil.resetPasswordSendEmail(fixApoS(request.getParameter("email")), String.valueOf(key));

                        session.setAttribute("successMsg",Statics.PASSWORD_RESET_MAIL_SENT);
                    } else {
                        session.setAttribute("errorMsg",Statics.FP_EMAIL_NOT_EXISTS);
                    }
                }
                else if (type.equals("login")) {

                    if (rs1.next()) {
                        if (EncryptionUtil.matchWithSecuredHash(password,rs1.getString("password"))) {
                            session.setAttribute("username", username);
                            session.setAttribute("id", rs1.getString("id"));


                            String ip= LocationUtils.getIpByRequest(request);
                            String location = LocationUtils.getLocationByIp(ip);
                            session.setAttribute("latitude", location.split("_")[0]);
                            session.setAttribute("longitude", location.split("_")[1]);

                            session.setAttribute("successMsg", Statics.LOGIN_SUCCEESS);
                            response.sendRedirect("profile");
                        } else {
                            session.setAttribute("errorMsg",Statics.LOGIN_MISSMATCH);
                        }
                    } else {
                        session.setAttribute("errorMsg",Statics.LOGIN_USERNAME_NOT_EXISTS);
                    }

                } else if (type.equals("register")) {
                    if (rs1.next()) {
                        session.setAttribute("errorMsg",Statics.REGISTER_USERNAME_ALREADY_EXISTS);
                    } else {
                        String pass = fixApoS(request.getParameter("password"));
                        String pass2 = fixApoS(request.getParameter("password2"));
                        if (pass!=null && pass2!=null && pass.equals(pass2)) {
                            q2 = "select max(id)+1 as mxid from user";
                            rs2 = st2.executeQuery(q2);
                            rs2.next();

                            String id = rs2.getString("mxid");
                            String name = fixApoS(request.getParameter("name"));
                            String phone = fixApoS(request.getParameter("phone"));
                            String mobile = fixApoS(request.getParameter("mobile"));
                            String address = fixApoS(request.getParameter("address"));
                            String sex = "undefined"; //fixApoS(request.getParameter("sex"));
                            String email = fixApoS(request.getParameter("email"));

                            q3 = "insert into user(id,username,password,name,phone,mobile,address,sex,email,type) values(" + id + ",'" + username + "','" + EncryptionUtil.generateSecuredHash(password) + "','" + name + "','" + phone + "','" + mobile + "','" + address + "','" + sex + "','" + email + "', 'seller')";
                            st3.executeUpdate(q3);
                            session.setAttribute("username", username);
                            session.setAttribute("id", rs2.getString("mxid"));

                            String ip= LocationUtils.getIpByRequest(request);
                            String location = LocationUtils.getLocationByIp(ip);
                            session.setAttribute("latitude", location.split("_")[0]);
                            session.setAttribute("longitude", location.split("_")[1]);


                            session.setAttribute("successMsg", Statics.REGISTER_SUCCEESS);
                            
                            response.sendRedirect("profile");
                        } else {
                            session.setAttribute("errorMsg",Statics.REGISTER_PASSWORD_MISSMATCH);
                        }
                    }
                }
            }

        %>




        <div class="container">

            <header>
<a class="navbar-brand" style="width:200px;" href="home"><img src="resources/img/logo-white-t.png" style="width: 100px; height: 27px;"/></a>
            </header>
            <section>
                <%if (session.getAttribute("successMsg")!=null) {%>
                <div class="alert alert-success" style="margin: 50px;">
                    <Strong><%=session.getAttribute("successMsg")%></Strong>
                </div>
                <%session.setAttribute("successMsg",null);}%>
                <%if (session.getAttribute("errorMsg")!=null) {%>
                <div class="alert alert-danger" style="margin: 50px;">
                    <Strong><%=session.getAttribute("errorMsg")%></Strong>
                </div>
                <%
                session.setAttribute("errorMsg",null);
                }%>
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <h1>Login and Registration Form</h1>

                        <div id="login" class="animate form">
                            <%if(request.getParameter("passResetKey")!=null && !request.getParameter("passResetKey").equals("")){%>
                            <form  action="loginRegister" method="post" autocomplete="on">
                                <h1>Reset Password</h1>
                                <p>
                                    <label for="password_r" class="youpasswd" >New password </label>
                                    <input id="password_r" minlength="2" name="password" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p>
                                    <label for="password_r_confirm" class="youpasswd" >Re-type new password </label>
                                    <input id="password_r_confirm" minlength="2" name="password2" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p>
                                    <input type="hidden" name="email" value="<%=fixApoS(request.getParameter("email"))%>"/>
                                    <input type="hidden" name="passResetKey" value="<%=fixApoS(request.getParameter("passResetKey"))%>"/>
                                    <input type="hidden" name="type" value="forgotPassConfirm"/>
                                    <input type="submit" class="btn btn-success" value="Reset Password"/>
                                </p>
                            </form>
                            <%}else{%>
                            <div>
                                <form action="loginRegister" method="post" autocomplete="on">
                                    <h1>Log in</h1>
                                    <p>
                                        <label for="username" class="uname"> Your username </label>
                                        <input id="username" name="username" required="required" type="text"
                                               placeholder="myusername"/>
                                    </p>
                                    <p>
                                        <label for="password" class="youpasswd"> Your password </label>
                                        <input id="password" name="password" minlength="2" required="required"
                                               type="password" placeholder="eg. X8df!90EO"/>
                                    </p>
                                    <!--                                <p class="keeplogin">
                                                                        <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" />
                                                                        <label for="loginkeeping">Keep me logged in</label>
                                                                    </p>-->
                                    <p class="signin button">
                                        <a href="home" style="float:left"><b>Return to Home Page</b></a>
                                        <a data-toggle="collapse" href="#collapse" style="float:right"><b>Forgot
                                            Password</b></a>
                                    </p>
                                    <p class="signin button">
                                        <input type="hidden" name="type" value="login"/>
                                        <input type="submit" value="Log in"/>
                                    </p>
                                </form>
                            </div>
                            <div id="collapse" class="panel-footer panel-collapse collapse">
                                <form action="loginRegister" method="post" autocomplete="on">
                                    <h1>Reset Password</h1>
                                    <p>
                                        <label for="emailfp" class="youmail"> Your email</label>
                                        <input id="emailfp" name="email" required="required" type="email"
                                               placeholder="mysupermail@mail.com"/>
                                    </p>
                                    <p>
                                        <input type="hidden" name="type" value="forgotPass"/>
                                        <input type="submit" class="btn btn-primary" value="Send Password Reset Link"/>
                                    </p>
                                </form>
                            </div>
                            <p class="change_link">
                                Not a member yet ?
                                <a href="#toregister" class="to_register"><b>Join us</b></a>
                            </p>



                            <%}%>
                        </div>

                        <div id="register" class="animate form">
                            <form  action="loginRegister#toregister" method="post" autocomplete="on">
                                <h1> Sign up </h1> 
                                <p> 
                                    <label for="usernamesignup" class="uname" >Your username</label>
                                    <input id="usernamesignup" name="username" required="required" type="text" placeholder="mysuperusername690" />
                                </p>
                                <p> 
                                    <label for="emailsignup" class="youmail" > Your email</label>
                                    <input id="emailsignup" name="email" required="required" type="email" placeholder="mysupermail@mail.com"/> 
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" >Your password </label>
                                    <input id="passwordsignup" minlength="2" name="password" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" >Please confirm your password </label>
                                    <input id="passwordsignup_confirm" minlength="2" name="password2" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>

                                <p> 
                                    <label for="usernamesignup" class="uname" >Your name</label>
                                    <input id="usernamesignup"name="name" type="text" placeholder="name" required/>
                                </p>

                                <p> 
                                    <label for="usernamesignup" class="uname" >Your phone</label>
                                    <input id="usernamesignup"name="phone" type="text" placeholder="phone" required/>
                                </p>

                                <p> 
                                    <label for="usernamesignup" class="uname">Your mobile</label>
                                    <input id="usernamesignup" name="mobile" type="text" placeholder="mobile" required/>
                                </p>

                                <%--<p>
                                    <label for="usernamesignup" class="uname" >Your address</label>
                                    <input id="usernamesignup" name="address" type="text" placeholder="address" />
                                </p>--%>

<!--                                <p> 
                                    <label for="usernamesignup" class="uname" >Your sex</label>
                                    <select name="sex" class="form-control">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </p>-->

                                <p class="signin button"> 
                                    <a href="home"><b>Return to Home Page</b></a>
                                </p>
                                <p class="signin button"> 
                                    <input type="hidden" name="type" value="register"/>
                                    <input type="submit" value="Sign up"/> 
                                </p>
                                <p class="change_link">  
                                    Already a member ?
                                    <a href="#tologin" class="to_register"> <b>Go and log in</b> </a>
                                </p>
                            </form>
                        </div>

                    </div>
                </div>  
            </section>
        </div>
        <%
            } catch(Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
    </body>
</html>