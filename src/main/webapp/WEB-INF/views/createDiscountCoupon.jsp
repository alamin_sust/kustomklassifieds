<%@ page import="java.sql.Statement" %>
<%@ page import="com.kustomklassifieds.web.util.WebUtils" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 21-Jul-18
  Time: 1:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- HEADER -->
<%@ include file="header.jsp" %>
<!-- END HEADER -->

<%

    String redirectStr = "";
    if(session.getAttribute("username") == null || !Utils.isAdmin(session.getAttribute("username"))) {
        response.sendRedirect("loginRegister");
    }

    Statement st1 = db.connection.createStatement();
    String q1 = "";
    ResultSet rs1;
    Statement st2 = db.connection.createStatement();
    String q2 = "";
    ResultSet rs2;
    Statement st3 = db.connection.createStatement();
    String q3 = "";
    ResultSet rs3;


    if(request.getParameter("couponName")!=null && !request.getParameter("couponName").equals("")) {
        String code = Utils.generateDiscountCouponCode();
        q1 = "insert into token (name,discount_percentage,is_active,code) values ('"+request.getParameter("couponName")
                +"', "+request.getParameter("discountPercentage")+", 1, '"+code+"')";
        st1.executeUpdate(q1);
        session.setAttribute("successMsg", "Successfully Created. COUPON: "+code);
    }


%>


<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
            <!--                    <p><a href="#">Link</a></p>
                                <p><a href="#">Link</a></p>
                                <p><a href="#">Link</a></p>-->
        </div>
        <div class="col-sm-8 well text-left" >

            <%if (session.getAttribute("successMsg") != null) {%>
            <div class="alert alert-success">
                <Strong><%=session.getAttribute("successMsg")%></Strong>
            </div>
            <%}
                session.setAttribute("successMsg", null);

            %>
            <%if (session.getAttribute("errorMsg") != null) {%>
            <div class="alert alert-danger">
                <Strong><%=session.getAttribute("errorMsg")%></Strong>
            </div>
            <%}
                session.setAttribute("errorMsg", null);

            %>

            <%--<%if (session.getAttribute("successMsg") != null) {%>
            <div class="alert alert-success">
                <Strong><%=session.getAttribute("successMsg")%></Strong>
            </div>
            <%}
                session.setAttribute("successMsg", null);

            %>--%>

            <div class="row" >

                <form action="createDiscountCoupon" method="post">

                    <div class="form-group input-group col col-sm-8 col-sm-offset-2">

                        <br>
                        <div class="row">
                            <label>Coupon Name</label><input name="couponName" type="text"class="form-control" required/>
                        </div>

                        <br>
                        <div class="row">
                            <label>Discount Percentage</label><input name="discountPercentage" type="number" class="form-control" required/>
                        </div>
                        <br>

                        <div class="row">
                                    <span class="input-group-btn">

                                        <div class="col col-sm-12">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-danger col col-sm-12">
                                                <span class="glyphicon glyphicon-search"></span> <b>Create</b>
                                            </button>
                                        </span>
                                        </div>
                                    </span>
                        </div>
                    </div>
                </form>


            </div>


            <div class="row">
                <label>Available Coupons:</label>
                <table class="table table-responsive">
                    <thead>
                        <tr>
                        <th>
                            Coupon Name
                        </th>
                        <th>
                            Code
                        </th>
                        <th>
                            Discount (%)
                        </th>
                        </tr>
                    </thead>
                    <tbody>
                <%
                    q2 = "select * from token";
                    rs2 = st2.executeQuery(q2);
                    while (rs2.next()) {
                %>

                <tr>
                    <td>
                        <%=rs2.getString("name")%>
                    </td>
                    <td>
                        <%=rs2.getString("code")%>
                    </td>
                    <td>
                        <%=rs2.getString("discount_percentage")%>
                    </td>
                </tr>

                <%}%>
                    </tbody>
                </table>
            </div>


        </div>
        <div class="col-sm-2 sidenav">
            <!--                    <div class="well">
                                    <p>ADS</p>
                                </div>
                                <div class="well">
                                    <p>ADS</p>
                                </div>-->
        </div>
    </div>
</div>


<!-- FOOTER -->
<%@ include file="footer.jsp" %>
<!-- END FOOTER -->
