
<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="com.kustomklassifieds.web.util.WebUtils" %>
<%@ page import="com.kustomklassifieds.web.util.Statics" %>
<%@ page import="com.kustomklassifieds.service.AppAuthService" %>
<%@ page import="com.kustomklassifieds.Util.Utils" %>
<%@ page import="javax.rmi.CORBA.Util" %>
<%@ page import="static com.kustomklassifieds.web.util.WebUtils.isNullOrEmpty" %>


<script>


    function changeFeature() {
        /*var e = document.getElementById ("feature");
        var val = e.options [e.selectedIndex] .value;

        document.getElementById("img6").style.display = "none";
        document.getElementById("img7").style.display = "none";
        document.getElementById("img8").style.display = "none";
        document.getElementById("img9").style.display = "none";
        document.getElementById("img10").style.display = "none";
        document.getElementById("img11").style.display = "none";
        document.getElementById("img12").style.display = "none";
        document.getElementById("img13").style.display = "none";
        document.getElementById("img14").style.display = "none";
        document.getElementById("img15").style.display = "none";
        document.getElementById("img16").style.display = "none";
        document.getElementById("img17").style.display = "none";
        document.getElementById("img18").style.display = "none";
        document.getElementById("img19").style.display = "none";
        document.getElementById("img20").style.display = "none";

        if(val === "2" || val === "3") {
            document.getElementById("img6").style.display = "block";
            document.getElementById("img7").style.display = "block";
            document.getElementById("img8").style.display = "block";
            document.getElementById("img9").style.display = "block";
            document.getElementById("img10").style.display = "block";
        }
        if(val === "3") {
            document.getElementById("img11").style.display = "block";
            document.getElementById("img12").style.display = "block";
            document.getElementById("img13").style.display = "block";
            document.getElementById("img14").style.display = "block";
            document.getElementById("img15").style.display = "block";
            document.getElementById("img16").style.display = "block";
            document.getElementById("img17").style.display = "block";
            document.getElementById("img18").style.display = "block";
            document.getElementById("img19").style.display = "block";
            document.getElementById("img20").style.display = "block";
        }*/
    }

    function addElement(parentId, elementTag, elementId, html) {
        // Adds an element to the document
        var p = document.getElementById(parentId);
        var newElement = document.createElement(elementTag);
        newElement.setAttribute('id', elementId);
        newElement.innerHTML = html;
        p.appendChild(newElement);
    }
    var fileId = 0; // used by the addFile() function to keep track of IDs
    var totalAmount=0;
    var featuredAmount=0;
    var packageAmount=0;
    var pkg='free';

    function resetAddPhotoButton() {
        if(pkg==='free' && fileId>=9) {
            document.getElementById("addPhotoButton").style.display = "none";
        } else if(pkg==='premium' && fileId>=19) {
            document.getElementById("addPhotoButton").style.display = "none";
        } else if(pkg==='preferred' && fileId>=49) {
            document.getElementById("addPhotoButton").style.display = "none";
        } else {
            document.getElementById("addPhotoButton").style.display = "block";
        }
    }

    function removeElement() {
        // Removes an element from the document

        var element = document.getElementById('file'+fileId);
        fileId--;

        /*if(fileId>=4) {
            totalAmount-=0.2;
            totalAmount = totalAmount.toFixed(2);
            document.getElementById("totalAmount").value=totalAmount;
        }*/

        element.parentNode.removeChild(element);

        resetAddPhotoButton();
    }


    function addFile() {
        fileId++; // increment fileId to get a unique ID for the new element
        var html = '<input type="file" id="file'+fileId+'" name="file'+fileId+'" /> ' ;
        addElement('files', 'p', 'file-' + fileId, html);
        /*if(fileId>4) {
            totalAmount+=0.2;
            totalAmount = totalAmount.toFixed(2);
            document.getElementById("totalAmount").value=totalAmount;
        }*/
        resetAddPhotoButton();
    }

    function getFileCount() {
        return fileId;
    }

    function adddays(val) {
        totalAmount+=val;
        totalAmount = totalAmount.toFixed(2);
        document.getElementById("totalAmount").value=totalAmount;
    }



    function changeTotalAmount() {
        totalAmount= packageAmount + featuredAmount;
        totalAmount = totalAmount.toFixed(2);
        document.getElementById("totalAmount").value=totalAmount;
    }

    function changePackageAmount(val) {
        packageAmount = val;
        changeTotalAmount();
    }

    function changeFeaturedAmount(val) {
        featuredAmount = val;
        changeTotalAmount();
    }

    function changeFeatured(val) {
        if(val === true) {
            changeFeaturedAmount(3.99);
        } else {
            changeFeaturedAmount(0);
        }
    }

    function prioritySearch(val) {
        totalAmount+=val;
        totalAmount = totalAmount.toFixed(2);
        document.getElementById("totalAmount").value=totalAmount;
    }

    function removeExtraPhotos(val) {
        while (fileId >= val) {
            removeElement();
        }
    }

    function changePackage(val) {
        if (val === 'free') {
            removeExtraPhotos(10);
            changePackageAmount(0);
            pkg = 'free';
            resetAddPhotoButton();
        } else if (val === 'premium') {
            removeExtraPhotos(20);
            changePackageAmount(8.99);
            pkg = 'premium';
            resetAddPhotoButton();
        } else {
            removeExtraPhotos(50);
            changePackageAmount(14.99);
            pkg = 'preferred';
            resetAddPhotoButton();
        }
    }
</script>

        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%

            String redirectStr = "";
            if(redirectStr.equals("") && WebUtils.isNullOrEmpty(session.getAttribute("username"))) {
                redirectStr="loginRegister";
            }

            Statement st1 = db.connection.createStatement();
            String q1 = "";
            ResultSet rs1;
            Statement st2 = db.connection.createStatement();
            String q2 = "";
            ResultSet rs2;
            Statement st3 = db.connection.createStatement();
            String q3 = "";
            ResultSet rs3;

            String successMsg = "";
            String errorMsg = "";
            
            Constants constants = new Constants();
            
            String name = "";
            String price = "";
            String year = "";
            String make = "";
            String model = "";
            String place = "";
            String contactInfo = "";
            String zipCode = "";
            String token = "";
            String feature = "";
            String ownerId = "";
            String details = "";

            Statement st = db.connection.createStatement();
            ResultSet rs = null;
            boolean isUpdating = false;
            if(request.getParameter("id")!=null&&!request.getParameter("id").equals("")) {
                isUpdating = true;
                String q = "select * from product where id=" + request.getParameter("id");
                rs = st.executeQuery(q);
                rs.next();

                name = rs.getString("name");
                price = rs.getString("price");
                year = rs.getString("year");
                make = rs.getString("make");
                model = rs.getString("model");
                place = rs.getString("place");
                contactInfo = rs.getString("contact_info");
                zipCode = rs.getString("zip_code");
                token = rs.getString("token");
                feature = rs.getString("feature");
                ownerId = rs.getString("ownerId");
                details = rs.getString("details");

                if (session.getAttribute("id") != null
                        && session.getAttribute("username") != null && !Utils.isAdmin(session.getAttribute("username"))
                        && !session.getAttribute("id").equals(ownerId)) {
                    session.setAttribute("errorMsg", "Insufficient Privilege! Try Again");
                    if(redirectStr.equals("")) {
                        redirectStr = "home";
                    }
                }
            }


            /*if(redirectStr.equals("") && !WebUtils.isNullOrEmpty(session.getAttribute("successMsg"))) {
                redirectStr="products?myVehicles=true";
            }*/

            if(!WebUtils.isNullOrEmpty(session.getAttribute("errorMsg"))) {
                session.setAttribute("successMsg",null);
            }

            if(!redirectStr.equals("") && WebUtils.isNullOrEmpty(session.getAttribute("errorMsg"))) {

                response.sendRedirect(redirectStr);


            }




        %>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
<!--                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>-->
                </div>
                <div class="col-sm-8 well text-left" >

                    <%if (session.getAttribute("successMsg") != null) {%>
                    <div class="alert alert-success">
                        <Strong><%=session.getAttribute("successMsg")%></Strong>
                    </div>
                    <%}
                        session.setAttribute("successMsg", null);

                    %>
                    <%if (session.getAttribute("errorMsg") != null) {%>
                    <div class="alert alert-danger">
                        <Strong><%=session.getAttribute("errorMsg")%></Strong>
                    </div>
                    <%}
                        session.setAttribute("errorMsg", null);

                    %>

                    <%--<%if (session.getAttribute("successMsg") != null) {%>
                    <div class="alert alert-success">
                        <Strong><%=session.getAttribute("successMsg")%></Strong>
                    </div>
                    <%}
                        session.setAttribute("successMsg", null);

                    %>--%>
                    
                    <div class="row" >





                        <form action="addProduct" method="post"  enctype="multipart/form-data">
                            
                            <div class="form-group input-group col col-sm-8 col-sm-offset-2">



                                <%--<div class="row">
                                    <label>Vehicle Image 1</label><input name="file" type="file" class="form-control" required=""/>
                                </div>
                                <%if(request.getParameter("id")!=null&&!request.getParameter("id").equals("")) {%>
                                <img src="resources/img/products/<%=ownerId%>_<%=request.getParameter("id")%>.jpg" width="150px;" height="100px;" class="img-responsive img-circle center-block" />
                                <%}%>
                                <div class="row">
                                    <label>Vehicle Image 2</label><input name="file2" type="file" class="form-control"/>
                                </div>
                                <div class="row">
                                    <label>Vehicle Image 3</label><input name="file3" type="file" class="form-control"/>
                                </div>
                                <div class="row">
                                    <label>Vehicle Image 4</label><input name="file4" type="file" class="form-control"/>
                                </div>
                                <div class="row">
                                    <label>Vehicle Image 5</label><input name="file5" type="file" class="form-control"/>
                                </div>

                                <div id="img6" class="row">
                                    <label>Vehicle Image 6</label><input name="file6" type="file" class="form-control"/>
                                </div>
                                <div id="img7" class="row">
                                    <label>Vehicle Image 7</label><input name="file7" type="file" class="form-control"/>
                                </div>
                                <div id="img8" class="row">
                                    <label>Vehicle Image 8</label><input name="file8" type="file" class="form-control"/>
                                </div>
                                <div id="img9" class="row">
                                    <label>Vehicle Image 9</label><input name="file9" type="file" class="form-control"/>
                                </div>
                                <div id="img10" class="row">
                                    <label>Vehicle Image 10</label><input name="file10" type="file" class="form-control"/>
                                </div>


                                <div id="img11" class="row">
                                    <label>Vehicle Image 11</label><input name="file11" type="file" class="form-control"/>
                                </div>
                                <div id="img12" class="row">
                                    <label>Vehicle Image 12</label><input name="file12" type="file" class="form-control"/>
                                </div>
                                <div id="img13" class="row">
                                    <label>Vehicle Image 13</label><input name="file13" type="file" class="form-control"/>
                                </div>
                                <div id="img14" class="row">
                                    <label>Vehicle Image 14</label><input name="file14" type="file" class="form-control"/>
                                </div>
                                <div id="img15" class="row">
                                    <label>Vehicle Image 15</label><input name="file15" type="file" class="form-control"/>
                                </div>
                                <div id="img16" class="row">
                                    <label>Vehicle Image 16</label><input name="file16" type="file" class="form-control"/>
                                </div>
                                <div id="img17" class="row">
                                    <label>Vehicle Image 17</label><input name="file17" type="file" class="form-control"/>
                                </div>
                                <div id="img18" class="row">
                                    <label>Vehicle Image 18</label><input name="file18" type="file" class="form-control"/>
                                </div>
                                <div id="img19" class="row">
                                    <label>Vehicle Image 19</label><input name="file19" type="file" class="form-control"/>
                                </div>
                                <div id="img20" class="row">
                                    <label>Vehicle Image 20</label><input name="file20" type="file" class="form-control"/>
                                </div>



                                <br>--%>


                                <%--<div class="row">
                                    <label>Feature:</label> <select class="form-control" id="feature" name="feature" onchange="changeFeature();" required>
                                    <option value="3" &lt;%&ndash;<%if(feature.equals("3")){%>selected<%}%>&ndash;%&gt;    >Preferred</option>
                                    <option value="2" &lt;%&ndash;<%if(feature.equals("2")){%>selected<%}%>&ndash;%&gt;    >Advanced</option>
                                    <option value="1" &lt;%&ndash;<%if(feature.equals("1")){%>selected<%}%>&ndash;%&gt;    >Standard</option>


                                </select>
                                </div>--%>

                                    <%if(isNullOrEmpty(request.getParameter("id"))){%>

                                    <div class="row">
                                        <h2 style="text-align: center; padding-bottom: 20px;">Listing Options</h2>
                                    </div>

                                    <div class="row">

                                    <div class = "col-sm-3 panel panel-success" style=" margin: 5px">

                                        <div class = "panel-body" style="margin-left: 15px;">
                                            <ul>
                                                <li>10 Photos</li>
                                                <li>Active 30 days</li>
                                            </ul>
                                        </div>

                                        <div class="panel-footer">
                                            <input type="radio" name="package" value="free" onchange="javascript:changePackage('free');" checked required><span>&nbsp;Stage I</span><br>Free</input>
                                        </div>
                                    </div>
                                    <div class = "col-sm-4 panel panel-primary" style="margin: 5px">

                                        <div class = "panel-body" style="margin-left: 15px;">
                                            <ul>
                                                <li><b>20 Photos</b></li>
                                                <li><b>Active 60 days</b></li>
                                            </ul>
                                        </div>

                                        <div class="panel-footer">
                                            <input style="width:15px;height:15px;border-radius:50%;" type="radio" name="package" value="premium" onchange="javascript:changePackage('premium');" required><b style="color: #1b6d85; font-size: large" >&nbsp;Stage II</b><br><b>$8.99</b></input>
                                        </div>
                                    </div>
                                    <div class = "col-sm-4 panel panel-danger" style="margin: 5px">

                                        <div class = "panel-body panel-danger" style="margin-left: 15px;">
                                            <ul>
                                                <li><b>50 Photos</b></li>
                                                <li><b>Active up to 365 days</b></li>
                                            </ul>
                                        </div>

                                        <div class="panel-footer">
                                            <input type="radio" style="width:20px;height:20px;border-radius:50%;" name="package" value="preferred" onchange="javascript:changePackage('preferred');" required><b style="color: red; font-size: x-large;">&nbsp;Stage X</b><br><b>$14.99</b></input>
                                        </div>
                                    </div>
                                    </div>
                                    <br>

                                <div class="row">

                                    <p><b>Upload Image(s)</b></p>
                                        <div id="files" class="form-group">
                                            <p><input class="form-control" type="file" name="file" /></p>
                                        </div>
                                            <div class="form-group">
                                        <p>
                                            <input id="addPhotoButton" class="form-control btn btn-primary small" type="button" value="Add File" onclick="addFile();"/>
                                            <input class="form-control btn btn-primary small" type="button" value="Remove File" onclick="removeElement();" /></p>
                                        </div>



                                </div>

                                <br>
                                    <%}%>

                                <%if(isNullOrEmpty(request.getParameter("id"))){%>
                                    <div class="row">
                                        <label>Featured Classified (+3.99$)</label> &nbsp;<input type="checkbox" name="isFeatured" value="1" onchange="changeFeatured(this.checked)"/>
                                    </div>
                                <%}%>
                                <br>
                                <div class="row">
                                    <label>Listing Title</label><input name="name" value="<%=name%>" type="text" class="form-control"/>
                                </div>
                                <br>
                                <div class="row">
                                <label>Price</label><label style="color: red;">*</label><input name="price" value="<%=price%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Year</label><label style="color: red;">*</label><input name="year" value="<%=year%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Make</label><label style="color: red;">*</label><input name="make" value="<%=make%>" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Model</label><input name="model" value="<%=model%>" type="text" class="form-control" />
                                </div>
                                <br>
                                <div class="row">
                                    <label>Location</label><input name="place" value="<%=place%>" type="text" class="form-control" />
                                </div>
                                <br>

                                    <div class="row">
                                        <label>Contact Info</label><input name="contactInfo" value="<%=contactInfo%>" type="text" class="form-control" />
                                    </div>
                                    <br>

                                    <div class="row">
                                        <label>Zip Code</label><input name="zipCode" value="<%=zipCode%>" type="text" class="form-control" />
                                    </div>
                                    <br>


                                <%for (int i = 0; i < constants.getDB_TABLES().size(); i++) {%>
                                <div class="row">
                                    <label><%=constants.getSEARCH_CRITERIA().get(i)%></label>

                                    <%if(constants.getDB_TABLES().get(i).equals("condition_")){%><label style="color: red;">*</label><%}%>
                                    <select class="form-control" name="<%=constants.getDB_TABLES().get(i)%>" <%if(constants.getDB_TABLES().get(i).equals("condition_")){%>required=""<%}%>>
                                        <option value="">--Select--</option>
                                        <%
                                            Statement stType = db.connection.createStatement();
                                            String qType = "select * from " + constants.getDB_TABLES().get(i) + " where id>0";
                                            ResultSet rsType = stType.executeQuery(qType);
                                            while (rsType.next()) {

                                                String selectedStr = " ";
                                                if(isUpdating && rs.getString(constants.getDB_TABLES().get(i)+"id").equals(rsType.getString("id"))) {
                                                    selectedStr = " selected";
                                                }

                                        %>
                                        <option value="<%=rsType.getString("id")%>"  <%=selectedStr%>><%=rsType.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <br>
                                <%}%>

                                <%if(session.getAttribute("username")!=null && Utils.isAdmin(session.getAttribute("username"))){%>
                                <div class="row">
                                    <label>Owner</label><label style="color: red;">*</label>
                                    <select class="form-control" name="ownerId" required="">
                                        <option value="">--Select--</option>
                                        <%
                                            Statement stO = db.connection.createStatement();
                                            String qO = "select * from user where id>0 and type='seller'";
                                            ResultSet rsO = stO.executeQuery(qO);
                                            while (rsO.next()) {
                                                String selectedStr = " ";
                                                if(isUpdating && rs.getString("ownerId").equals(rsO.getString("id"))) {
                                                    selectedStr = " selected";
                                                }
                                        %>
                                        <option value="<%=rsO.getString("id")%>" <%=selectedStr%>><%=rsO.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <br>
                                <%} else if(session.getAttribute("id")!=null && !session.getAttribute("id").equals("")){%>
                                <input type="hidden" name="ownerId" value="<%=session.getAttribute("id")%>"/>
                                <%}%>

                                    <%--<div class="row">
                                        <label>Search Criteria </label>
                                    </div> <div class="row">    <input  type="radio" name="searchFeature" value="false" onchange="javascript:prioritySearch(-1.99);" checked required> default</input>
                                        </div><div class="row">   <input type="radio" name="searchFeature" value="true" onchange="javascript:prioritySearch(1.99);" required> sponsored search result ($1.99)</input>
                                    </div>
                                    <br>--%>
                                    <%if(isNullOrEmpty(request.getParameter("id"))){%>
                                    <div class="row">
                                        <label>Discount Token</label><input name="token" value="<%=token%>" type="text" class="form-control" />
                                    </div>
                                    <br>
                                    <%}%>

                                    <div class="row">
                                        <label>Description</label><textarea name="details" type="text" rows="15" class="form-control"><%=details%></textarea>
                                    </div>

                                    <br>

                                    <%if(isNullOrEmpty(request.getParameter("id"))){%>
                                    <div class="row">
                                        <label>Total Amount ($) </label> <input readonly id="totalAmount" name="totalAmount" value="0.00" type="text" class="alert alert-default" />
                                    </div>
                                    <br>
                                    <%}%>

                                <div class="row">
                                    <span class="input-group-btn">
                                        <%if(request.getParameter("id")!=null && !request.getParameter("id").equals("")){%>
                                        <input type="hidden" name="productId" value="<%=request.getParameter("id")%>"/>
                                        <input type="hidden" name="update" value="yes"/>
                                        <div class="col col-sm-12">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-danger col col-sm-12">
                                                <span class="glyphicon glyphicon-search"></span> <b>Update</b>
                                            </button>
                                        </span>
                                        </div>
                                        <%} else {%>


                                        <div class="col col-sm-12">
                                        <span class="input-group-btn">
                                            <input type="hidden" name="insert" value="yes"/>
                                            <button type="submit" class="btn btn-danger col col-sm-12">
                                                <span class="glyphicon glyphicon-search"></span> <b>Pay and List</b>
                                            </button>
                                        </span>
                                        </div>
                                        <%}%>
                                        
                                    </span>
                                </div>        
                            </div>
                        </form>


                    </div>



                </div>
                <div class="col-sm-2 sidenav">
<!--                    <div class="well">
                        <p>ADS</p>
                    </div>
                    <div class="well">
                        <p>ADS</p>
                    </div>-->
                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->
