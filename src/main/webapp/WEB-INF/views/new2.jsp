<script src="https://js.braintreegateway.com/web/dropin/1.4.0/js/dropin.js"></script>

<form id="payment-form" method="post" action="checkouts">

    <div id="bt-dropin"></div>

    <input type="hidden" id="nonce" name="payment_method_nonce" />
    <button class="button" type="submit"><span><b>Test Transaction</b></span></button>
</form>


<script>
    /*<![CDATA[*/
    var form = document.querySelector('#payment-form');
    var client_token = [[${clientToken}]];

    braintree.dropin.create({
        authorization: 'sandbox_g42y39zw_348pk9cgf3bgyw2b',
        container: '#bt-dropin',
        paypal: {
            flow: 'checkout',
            amount: '5.00',
            currency: 'USD'
        },
        paypalCredit: {
            flow: 'checkout',
            amount: '10.00',
            currency: 'USD'
        },
        applePay: {
            displayName: 'My Store',
            paymentRequest: {
                total: {
                    label: 'My Store',
                    amount: '19.99'
                }
            }
        }
    }, function (createErr, instance) {
        form.addEventListener('submit', function (event) {
            event.preventDefault();

            instance.requestPaymentMethod(function (err, payload) {
                if (err) {
                    console.log('Error', err);
                    return;
                }

                // Add the nonce to the form and submit
                document.querySelector('#nonce').value = payload.nonce;
                form.submit();
            });
        });
    });
    /*]]>*/
</script>
</body>
</html>
