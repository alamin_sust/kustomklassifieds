<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="static com.kustomklassifieds.web.util.WebUtils.fixApoS" %>

<!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%
            Statement st = db.connection.createStatement();
            String q = "select * from product where id="+fixApoS(request.getParameter("productId"));
            ResultSet rs = st.executeQuery(q);
            rs.next();

        %>


        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
<!--                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>-->
                </div>
                <div class="col-sm-8 text-left" >

                    <br>


                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Payment Details
                                    </h3>
                                    <div class="checkbox pull-right">
                                        <label>
                                            <input type="checkbox" />
                                            Remember
                                        </label>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form role="form">
                                        <div class="form-group">
                                            <div><label for="cardNumber">
                                                    CARD NUMBER
                                                </label>
                                            </div>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="cardNumber" placeholder="Valid Card Number"
                                                       required autofocus />
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-7 col-md-7">
                                                <div class="form-group">
                                                    <div>
                                                        <label for="expityMonth">
                                                            EXPIRY DATE
                                                        </label>
                                                    </div>
                                                    <div class="col-xs-6 col-lg-6 pl-ziro">
                                                        <input type="text" class="form-control" id="expityMonth" placeholder="MM" required />
                                                    </div>
                                                    <div class="col-xs-6 col-lg-6 pl-ziro">
                                                        <input type="text" class="form-control" id="expityYear" placeholder="YY" required /></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-md-5 pull-right">
                                                <div class="form-group">
                                                    <label for="cvCode">
                                                        CV CODE</label>
                                                    <input type="password" class="form-control" id="cvCode" placeholder="CV" required />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="#"><span class="badge pull-right"><span class="glyphicon glyphicon-usd"></span><%=rs.getString("price")%></span> <b>Final Payment</b></a>
                                </li>
                            </ul>
                            <br/>
                            <a href="paymentComplete" class="btn btn-success btn-lg btn-block" role="button"><b>Pay</b></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 sidenav">
<!--                    <div class="well">
                        <p>ADS</p>
                    </div>
                    <div class="well">
                        <p>ADS</p>
                    </div>-->
                </div>

            </div>


            <!-- FOOTER -->
            <%@ include file="footer.jsp" %>
            <!-- END FOOTER -->