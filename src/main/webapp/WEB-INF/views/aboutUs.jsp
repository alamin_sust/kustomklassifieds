<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>


        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <div class="container-fluid text-center">    
            <div class="row content panel">
                <div class="col-sm-2 sidenav">

                </div>
                <div class="col-sm-8 text-left" >

                    <div class="row" style="margin-top: 30px; margin-left: 10px; margin-right: 10px;">

                        <h2 class="text-center">About Us</h2>
                        <br>

                        <p>Looking for a customized vehicle? Looking for a part for your ride? Look no further.</p> 

                        <p>At Kustom Klassifieds, we strive to help you find the right buyer for your vehicle. Modified and customized vehicles can be tough to sell on traditional platforms, there just wasn't a simple way to search for those kinds of vehicles. Kustom Klassifieds takes care of that issue and puts your vehicle in front of an audience of automotive enthusiasts. </p>

                        <br>
                        <h2>What does our future look like</h2>
                        <br>

                        <p>As we grow every day, we continue to make improvements to our website and find unique automotive content to share with you. We are passionate about growing our filters to match every auto enthusiast?s needs, as well as making our website one of the easiest to use on the market. </p>

                        <p>If you have feedback for us, please let us know at <a href="mailto:KustomKlassifieds@gmail.com">KustomKlassifieds@gmail.com</a>.</p>

<br>
                    </div>
                </div>
                <div class="col-sm-2 sidenav">

                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->