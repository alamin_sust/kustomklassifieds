<%@page import="com.kustomklassifieds.web.util.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>

        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%
            Constants constants = new Constants();

        %>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
<!--                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>-->
                </div>
                <div class="col-sm-8 well text-left" >

                    <div class="row" style="margin-top: 30px;">
                        <form action="products" method="get">
                            <div class="form-group input-group  col col-sm-8 col-sm-offset-2">
                                <h2 class="text-center">Custom Search</h2>
                                <div class="row">
                                    <label>Year</label><input name="year" type="text" class="form-control" />
                                </div>
                                <br>
                                <div class="row">
                                    <label>Make</label><input name="make" type="text" class="form-control" />
                                </div>
                                <br>
                                <div class="row">
                                    <label>Model</label><input name="model" type="text" class="form-control" />
                                </div>
                                <br>
                                <div class="row">
                                    <label>Zip Code</label><input name="zipCode" type="text" class="form-control" />
                                </div>
                                <br>
                                <div class="row"><label>Location</label><input type="number" class="form-control" name="distance" placeholder="distance within (in miles)"/></div>
                                <br>
                                <%for (int i = 0; i < constants.getDB_TABLES().size(); i++) {%>
                                <div class="row">
                                    <label><%=constants.getSEARCH_CRITERIA().get(i)%></label>
                                    <select class="form-control" name="<%=constants.getDB_TABLES().get(i)%>">
                                        <option value="">--Select--</option>
                                        <%
                                            Statement stType = db.connection.createStatement();
                                            String qType = "select * from "+constants.getDB_TABLES().get(i)+" where id>0";
                                            ResultSet rsType = stType.executeQuery(qType);
                                            while (rsType.next()) {
                                        %>
                                        <option value="<%=rsType.getString("id")%>"><%=rsType.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                    <br>
                                <%}%>
                                
                                <div class="row">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary col col-sm-12" type="button">
                                            <span class="glyphicon glyphicon-search"></span> <b>Search</b>
                                        </button>
                                    </span>
                                </div>        
                            </div>
                        </form>
                    </div>



                </div>
                <div class="col-sm-2 sidenav">
<!--                    <div class="well">
                        <p>ADS</p>
                    </div>
                    <div class="well">
                        <p>ADS</p>
                    </div>-->
                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->
