<!-- Footer -->
<div class="footer w3ls">
    <div class="container">
        <div class="footer-main">
            <div class="footer-top">
                <div class="col-md-4 ftr-grid fg1">



                    <h4>What does our future look like</h4>
                    <p style="text-align: left;">As we grow every day, we continue to make improvements to our website and find unique automotive content to share with you. We are passionate about growing our filters to match every auto enthusiasts' needs, as well as making our website one of the easiest to use on the market.</p>




                    <a href="aboutUs" style="width: 100%;">Learn More</a>
                </div>
                <div class="col-md-4 ftr-grid fg2">
                    <h3>General Information</h3>
                    <div class="ftr-address" >
                        <div class="local" style="float: left; width: auto;">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="ftr-text" style="margin-left:5px; float: left; width: auto;">
                            <p>Kustom Klassifieds LLC</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <%--<div class="ftr-address">
                        <div class="local">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div class="ftr-text">
                            <p>+1 (512) 154 8176</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>--%>
                    <div class="ftr-address" >
                        <div class="local" style="float: left; width: auto;">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="ftr-text" style="margin-left:5px;float: left; width: auto;">
                            <p><a href="mailto:info@example.com">kustomklassifieds@gmail.com</a></p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-4 ftr-grid fg2">
                    <h3>Keep In Touch With Us</h3>
                    <div class="right-w3l" style="float: left; width: auto;">
                        <ul class="top-links">
                            <li><a href="https://www.facebook.com/kustomklassifieds"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/Kustom_Klassifieds"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="right-w3-2"  style="margin-left:5px; float: left; width: auto;">
                        <ul class="text-w3">
                            <li><a href="https://www.facebook.com/kustomklassifieds">Facebook</a></li>
                            <li><a href="https://www.instagram.com/Kustom_Klassifieds">Instagram</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="copyrights">
                <%--<p>&copy; 2017 Auto Car. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>--%>
                    <p>&copy; 2018 KustomKlassifieds. All Rights Reserved.</p>
            </div>
        </div>
    </div>
    <div class="newsletter-agile">
        <form action="home" method="post">
            <p>Send us Your Mail, we'll make sure You Never Miss a Thing!</p>
            <input type="email" name="subscribeEmail" required="" placeholder="Enter Your E-mail Here..">
            <input type="submit" value="Subscribe">
        </form>
    </div>
</div>

<!-- Footer -->


<!-- js-scripts -->
<!-- js -->
<script type="text/javascript" src="resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap -->
<!-- //js -->

<script src="resources/js/responsiveslides.min.js"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        // Slideshow 4
        $("#slider3").responsiveSlides({
            auto: true,
            pager:true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>



<!-- Starts-Number-Scroller-Animation-JavaScript -->
<script type="text/javascript" src="resources/js/numscroller-1.0.js"></script>
<!-- //Starts-Number-Scroller-Animation-JavaScript -->



<!-- particles-JavaScript -->
<script src="resources/js/particles.min.js"></script>
<script>
    window.onload = function() {
        Particles.init({
            selector: '#myCanvas',
            color: '#b3b3b3',
            connectParticles: true,
            minDistance: 100
        });
    };
</script>

<!-- //particles-JavaScript -->

<!-- team-plugin -->
<script src="resources/js/mislider.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(function ($) {
        var slider = $('.mis-stage').miSlider({
            //  The height of the stage in px. Options: false or positive integer. false = height is calculated using maximum slide heights. Default: false
            stageHeight: 380,
            //  Number of slides visible at one time. Options: false or positive integer. false = Fit as many as possible.  Default: 1
            slidesOnStage: false,
            //  The location of the current slide on the stage. Options: 'left', 'right', 'center'. Defualt: 'left'
            slidePosition: 'center',
            //  The slide to start on. Options: 'beg', 'mid', 'end' or slide number starting at 1 - '1','2','3', etc. Defualt: 'beg'
            slideStart: 'mid',
            //  The relative percentage scaling factor of the current slide - other slides are scaled down. Options: positive number 100 or higher. 100 = No scaling. Defualt: 100
            slideScaling: 150,
            //  The vertical offset of the slide center as a percentage of slide height. Options:  positive or negative number. Neg value = up. Pos value = down. 0 = No offset. Default: 0
            offsetV: -5,
            //  Center slide contents vertically - Boolean. Default: false
            centerV: true,
            //  Opacity of the prev and next button navigation when not transitioning. Options: Number between 0 and 1. 0 (transparent) - 1 (opaque). Default: .5
            navButtonsOpacity: 1,
        });
    });
</script>
<!-- //team-plugin -->

<!-- start-smoth-scrolling -->
<script type="text/javascript" src="resources/js/move-top.js"></script>
<script type="text/javascript" src="resources/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->


<!-- //js-scripts -->
<%
                } catch(Exception e) {
                System.out.println(e);
           } finally {
               db.close();
           }
%>
</body>
</html>



