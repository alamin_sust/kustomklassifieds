<%@page import="com.carshop.utils.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kustom Klassifieds</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="js/homeJs.js" type="text/javascript"></script>
        <link href="css/homeCss.css" rel="stylesheet" type="text/css">

    </head>
    <body>

        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
                    <p><img src="resources/img/cars/1.jpg" class="img-responsive" alt="Car"></p>
<!--                    <p><img src="resources/img/cars/2.jpg" class="img-responsive" alt="Car"></p>
                    <p><img src="resources/img/cars/3.jpg" class="img-responsive" alt="Car"></p>-->
                </div>
                <div class="col-sm-8 text-left" >

                    <div class="row" style="margin-top: 30px;">
                        <%if (session.getAttribute("successMsg") != null) {%>
                        <div class="alert alert-success">
                            <Strong><%=session.getAttribute("successMsg")%></Strong>
                        </div>
                        <%}%>
                        <%if (session.getAttribute("errorMsg") != null) {%>
                        <div class="alert alert-danger">
                            <Strong><%=session.getAttribute("errorMsg")%></Strong>
                        </div>
                        <%}%>

                        <%

                            session.setAttribute("successMsg", null);
                            session.setAttribute("errorMsg", null);

                        %>
                        <form action="products" method="get">
                            <div class="form-group input-group  col col-sm-8 col-sm-offset-2">
                                <div class="row"><select class="form-control" name="customtype">Kustom Type
                                        <option value="">--Select Type--</option>
                                        <%                                            Statement stType = db.connection.createStatement();
                                            String qType = "select * from customtype where id>0";
                                            ResultSet rsType = stType.executeQuery(qType);
                                            while (rsType.next()) {
                                        %>
                                        <option value="<%=rsType.getString("id")%>"><%=rsType.getString("name")%></option>
                                        <%}%>
                                    </select></div>
                                <div class="row"><select class="form-control" name="location">Location
                                        <option value="">--Select Location--</option>
                                        <%
                                            Statement stLocation = db.connection.createStatement();
                                            String qLocation = "select * from location where id>0";
                                            ResultSet rsLocation = stLocation.executeQuery(qLocation);
                                            while (rsLocation.next()) {
                                        %>
                                        <option value="<%=rsLocation.getString("id")%>"><%=rsLocation.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <br>
                                <div class="row">

                                    <div class="col col-sm-8">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary col col-sm-12" type="button">
                                                <span class="glyphicon glyphicon-search">  Search</span>
                                            </button>
                                        </span>
                                    </div>
                                    <div class="col col-sm-4">
                                        <span class="input-group-btn">

                                            <a class="btn btn-primary col col-sm-12" href="customSearch"><span class="glyphicon glyphicon-plus"> More Filters</span></a>

                                        </span>
                                    </div>
                                </div>        
                            </div>
                        </form>
                    </div>

                    <!--                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                             Indicators 
                                            <ol class="carousel-indicators">
                                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                            </ol>
                    
                    
                    
                                             Wrapper for slides 
                                            <div class="carousel-inner">
                    
                                                <div class="item active">
                                                    <img src="resources/img/background.jpg" alt="Los Angeles" style="width:100%;">
                                                </div>
                    
                                                <div class="item">
                                                    <img src="resources/img/background.jpg" alt="Chicago" style="width:100%;">
                                                </div>
                    
                                                <div class="item">
                                                    <img src="resources/img/background.jpg" alt="New york" style="width:100%;">
                                                </div>
                                            </div>
                    
                                             Left and right controls 
                                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>-->
                    <!--start-->
                    <div class="row">
                        
                        
                            <div class="well">
                                <div class="span12">
                                    <div class="panel panel-success" style="text-align: center;">
                                        <h1>Sponsored Search Result</h1>      
                                    </div>
                                <div id="myCarousel" class="carousel fdi-Carousel slide">
                                    <!-- Carousel items -->
                                    <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                                        <div class="carousel-inner onebyone-carosel">
                                            <%for (int i = 1; i <= Math.min(10,Constants.TOTAL_CAR_PICS); i++) {%>
                                            <div class="item <%if (i == 1) {%>active<%}%>">
                                                <div class="col-md-4">
                                                    <a href="#"><img src="resources/img/cars/<%=i%>.jpg" class="img-responsive center-block"></a>
                                                    <div class="text-center"><%=i%></div>
                                                </div>
                                            </div>
                                            <%}%>
                                        </div>
                                        <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                                        <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                                    </div>
                                    <!--/carousel-inner-->
                                </div><!--/myCarousel-->
                            </div><!--/well-->
                        </div>
                    </div>
<!--                    <div class="row">
                        
                        
                            <div class="well">
                                <div class="span12">
                                <div class="panel panel-danger" style="text-align: center;">
                                    <h1>Best Sellers</h1>      
                                </div>
                                <div id="myCarousel" class="carousel fdi-Carousel slide">
                                     Carousel items 
                                    <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                                        <div class="carousel-inner onebyone-carosel">
                                            <%for (int i = Constants.TOTAL_CAR_PICS; i >= 1; i--) {%>
                                            <div class="item <%if (i == Constants.TOTAL_CAR_PICS) {%>active<%}%>">
                                                <div class="col-md-4">
                                                    <a href="#"><img src="resources/img/cars/<%=i%>.jpg" class="img-responsive center-block"></a>
                                                    <div class="text-center"><%=i%></div>
                                                </div>
                                            </div>
                                            <%}%>
                                        </div>
                                        <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                                        <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                                    </div>
                                    /carousel-inner
                                </div>/myCarousel
                            </div>/well
                        </div>
                    </div>-->

                </div>
                <div class="col-sm-2 sidenav">
                    <div class="well">
                        <p><img src="resources/img/cars/4.jpg" class="img-responsive" alt="Car"></p>
                    </div>
<!--                    <div class="well">
                        <p><img src="resources/img/cars/5.jpg" class="img-responsive" alt="Car"></p>
                    </div>
                    <div class="well">
                        <p><img src="resources/img/cars/6.jpg" class="img-responsive" alt="Car"></p>
                    </div>-->
                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->

    </body>
</html>
