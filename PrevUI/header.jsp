<%@page import="com.kustomklassifieds.connection.Database"%>
<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style>
            /* Remove the navbar's default margin-bottom and rounded borders */ 
            .navbar {
                margin-bottom: 0;
                border-radius: 0;
            }

            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
            .row.content {height: 950px}

            /* Set gray background color and 100% height */
            .sidenav {
                padding-top: 20px;
                background-color: #f1f1f1;
                height: 100%;
            }

            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 15px;
            }

            /* On small screens, set height to 'auto' for sidenav and grid */
            @media screen and (max-width: 767px) {
                .sidenav {
                    height: auto;
                    padding: 15px;
                }
                .row.content {height:auto;} 
            }
        </style>
        
        
        
        
        <%
        
        Database db = new Database();
            db.connect();

           // try {
        %>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" style="width:200px;" href="home"><img src="resources/img/logo-white-t.png" style="width: 100px; height: 27px;"/></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar" >
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle"  href="home"><span class="glyphicon glyphicon-home"> Home</span></a>
                            
                        </li>
<!--                        <li class="dropdown">
                            <a class="dropdown-toggle"  href="addProduct"><span class="glyphicon glyphicon-plus"> Vehicles/Parts</span></a>
                        </li>-->
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Vehicles<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="products?category=1">Buy</a></li>
                                <li><a href="products?myVehicles=true">Sell</a></li>
                            </ul>
                        </li>
<!--                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Parts<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="customSearch">Buy</a></li>
                                <li><a href="products?myParts=true">Sell</a></li>
                            </ul>
                        </li>-->
                        
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Buy Something<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="products?category=1">Vehicles</a></li>
<!--                                <li><a href="products?category=2">Parts</a></li>-->
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sell Something<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="products?myVehicles=true">Vehicles</a></li>
<!--                                <li><a href="products?category=2">Parts</a></li>-->
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" href="#"></a>
                            <ul class="dropdown-menu">
                                <li><a href="termsOfService">Terms of Service</a></li>
                                <li><a href="aboutUs">About Us</a></li>
                                <li><a href="privacyPolicy">Privacy Policy</a></li>
<!--                                <li><a href="#">Buy Vehicles</a></li>
                                <li><a href="#">Buy Parts</a></li>
                                <li><a href="#">Sell Vehicles</a></li>
                                <li><a href="#">Sell Parts</a></li>-->

                            </ul>
                        </li>

                        <%if (session.getAttribute("username") == null) {%>
                        <li><a href="loginRegister"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                            <%} else {%>
                        <li><a href="profile">logged in as: <strong><%=session.getAttribute("username")%></strong></a></li>
                        <li><a href="loginRegister"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                            <%}%>
                    </ul>
                </div>
            </div>
        </nav>