<%@page import="java.util.List"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="com.carshop.utils.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kustom Klassifieds</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    </head>
    <body>

        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%
            if (session.getAttribute("id") == null || session.getAttribute("id").toString().equals("")) {
                response.sendRedirect("home");
            }

            session.setAttribute("successMsg", null);
            session.setAttribute("errorMsg", null);

            Statement st1 = db.connection.createStatement();
            String q1 = "";
            ResultSet rs1;
            Statement st2 = db.connection.createStatement();
            String q2 = "";
            ResultSet rs2;
            Statement st3 = db.connection.createStatement();
            String q3 = "";
            ResultSet rs3;

            String successMsg = "";
            String errorMsg = "";
            
            Constants constants = new Constants();
            
            if(request.getParameter("mxid")!=null && !request.getParameter("mxid").equals("")) {
                session.setAttribute("mxid", request.getParameter("mxid"));
            }
            

            if(request.getParameter("name")!=null && !request.getParameter("name").equals("")) {
                
                
                
                String values="";
                List<String> tables = constants.getDB_TABLES();
                for(int i=0;i<tables.size();i++) {
                    if(i>0) {
                        values+=",";
                    }
                    values+="'"+request.getParameter(tables.get(i))+"'";
                }
                
                q2 = "insert into product (id,name,price,year,make,model,ownerId,"
                        +constants.getCommaSeparatedTableNames()+") values("
                        +session.getAttribute("mxid").toString()+",'"
                        +request.getParameter("name")+"',"+request.getParameter("price")
                        +",'"+request.getParameter("year")+"','"+request.getParameter("make")
                        +"','"+request.getParameter("model")+"',"+session.getAttribute("id")
                        +","+values+")";
                st2.executeUpdate(q2);
                session.setAttribute("mxid", null);
                session.setAttribute("successMsg", "Product Added Successfully!");
                response.sendRedirect("home");
            
            }

        %>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
<!--                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>-->
                </div>
                <div class="col-sm-8 text-left" >
                    <div class="row" style="margin-top: 30px;">
                        <form method="POST" action="addProduct?isImageSelected=yes" enctype="multipart/form-data" >
                            <% 
                                q1="select max(id)+1 as mxid from product";
                                rs1=st1.executeQuery(q1);
                                rs1.next();
                                
                                
                                String saveFile = new String();
                                String saveFile2 = new String();
                                String contentType = request.getContentType();
                                if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                                    DataInputStream in = new DataInputStream(request.getInputStream());

                                    int formDataLength = request.getContentLength();
                                    byte dataBytes[] = new byte[formDataLength];
                                    int byteRead = 0;
                                    int totalBytesRead = 0;

                                    while (totalBytesRead < formDataLength) {
                                        byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                                        totalBytesRead += byteRead;
                                    }
                                    String file = new String(dataBytes);

                                    saveFile = file.substring(file.indexOf("filename=\"") + 10);
                                    saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                                    saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));

                                    int lastIndex = contentType.lastIndexOf("=");

                                    String boundary = contentType.substring(lastIndex + 1, contentType.length());

                                    int pos;

                                    pos = file.indexOf("filename=\"");
                                    pos = file.indexOf("\n", pos) + 1;
                                    pos = file.indexOf("\n", pos) + 1;
                                    pos = file.indexOf("\n", pos) + 1;

                                    int boundaryLocation = file.indexOf(boundary, pos) - 4;

                                    int startPos = ((file.substring(0, pos)).getBytes()).length;
                                    int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;

                                    saveFile = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\CarShop\\web\\img\\products\\" + session.getAttribute("id").toString()+"_" +rs1.getString("mxid")+ ".jpg";
                                    saveFile2 = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\CarShop\\web\\img\\products\\"+ session.getAttribute("id").toString()+"_" +rs1.getString("mxid")+ ".jpg";
                                    //saveFile = "C:/uploadDir2/" + saveFile;
                                    //out.print(saveFile);
                                    File ff = new File(saveFile);
                                    File ff2 = new File(saveFile2);

                                    try {
                                        FileOutputStream fileOut = new FileOutputStream(ff);
                                        fileOut.write(dataBytes, startPos, (endPos - startPos));
                                        fileOut.flush();
                                        fileOut.close();
                                        FileOutputStream fileOut2 = new FileOutputStream(ff2);
                                        fileOut2.write(dataBytes, startPos, (endPos - startPos));
                                        fileOut2.flush();
                                        fileOut2.close();

                                    } catch (Exception e) {
                                        out.println(e);
                                    }

                                }
                                if (request.getParameter("isImageSelected") != null && !request.getParameter("isImageSelected").equals("")) {
                                    Thread.sleep(10000);
                                }
                            %>
                            <div class="form-group input-group  col col-sm-8 col-sm-offset-2">
                                <div class="row">
                                    <label>Product Image</label>
                                    <input type="file" name="file" value="" />
                                    <input type="hidden" name="mxid" value="<%=rs1.getString("mxid")%>"/>
                                        <input type="submit" value="UPLOAD" name="submit" />
                                   
                                        <img src="resources/img/products/<%=session.getAttribute("id")%>_<%=rs1.getString("mxid")%>.jpg" width="150px;" height="100px;" class="img-responsive img-circle center-block" />
                                   
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="row" >





                        <form action="addProduct" method="post">
                            <div class="form-group input-group  col col-sm-8 col-sm-offset-2">

                                <div class="row">
                                    <label>Name</label><input name="name" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Price</label><input name="price" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Year</label><input name="year" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Make</label><input name="make" type="text" class="form-control" required=""/>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Model</label><input name="model" type="text" class="form-control" required=""/>
                                </div>
                                <br>


                                <%for (int i = 0; i < constants.getDB_TABLES().size(); i++) {%>
                                <div class="row">
                                    <label><%=constants.getSEARCH_CRITERIA().get(i)%></label>
                                    <select class="form-control" name="<%=constants.getDB_TABLES().get(i)%>" required="">
                                        <option value="">--Select--</option>
                                        <%
                                            Statement stType = db.connection.createStatement();
                                            String qType = "select * from " + constants.getDB_TABLES().get(i) + " where id>0";
                                            ResultSet rsType = stType.executeQuery(qType);
                                            while (rsType.next()) {
                                        %>
                                        <option value="<%=rsType.getString("id")%>"><%=rsType.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <br>
                                <%}%>

                                <div class="row">
                                    <span class="input-group-btn">
                                        <input type="hidden" name="mxid" value="<%=session.getAttribute("mxid")==null?rs1.getString("mxid"):session.getAttribute("mxid").toString()%>"/>
                                        <button type="submit" class="btn btn-primary col col-sm-12" type="button">
                                            <span class="glyphicon glyphicon-search">  Add</span>
                                        </button>
                                    </span>
                                </div>        
                            </div>
                        </form>


                    </div>



                </div>
                <div class="col-sm-2 sidenav">
<!--                    <div class="well">
                        <p>ADS</p>
                    </div>
                    <div class="well">
                        <p>ADS</p>
                    </div>-->
                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->

    </body>
</html>
