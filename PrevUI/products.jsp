<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.carshop.utils.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kustom Klassifieds</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    </head>
    <body>

        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%
            if ((request.getParameter("myVehicles") != null && request.getParameter("myParts") != null) && (session.getAttribute("id") == null || session.getAttribute("id").toString().equals(""))) {
                response.sendRedirect("loginRegister");
            }

            Statement st = db.connection.createStatement();
            String q = "select * from product where id>0";

//            String productCategory = request.getParameter("searchCategory");
            String searchStr = "";

//            if (productCategory != null && !productCategory.equals("")) {
//                q += " and categoryId='" + productCategory + "'";
//                searchStr += " Category: " + productCategory + ".";
//            }

            String extraParams[] = {"year", "make", "model"};

            for(String param: extraParams) {
            if (request.getParameter(param) != null && !request.getParameter(param).equals("")) {
                    q += " and "+param+"='" + request.getParameter(param)+"'";
                }
            }
            
            Constants constants = new Constants();

            List<String> tables = constants.getDB_TABLES();
            Map<String, String> tableLabelMap = constants.getDBMap();

            for (int i = 0; i < tables.size(); i++) {
                String table = tables.get(i);
                String tableLabel = tableLabelMap.get(table);
                if (request.getParameter(table) != null && !request.getParameter(table).equals("")) {
                    q += " and " + table + "Id=" + request.getParameter(table);
                    Statement stCriteria = db.connection.createStatement();
                    String qCriteria = "select * from " + table + " where id=" + request.getParameter(table);
                    ResultSet rsCriteria = stCriteria.executeQuery(qCriteria);
                    rsCriteria.next();
                    searchStr += " " + tableLabel + ": " + rsCriteria.getString("name") + ".";
                }
            }

            if (session.getAttribute("id") != null && !session.getAttribute("id").toString().equals("")) {
                if (request.getParameter("myVehicles") != null && request.getParameter("myVehicles").equals("true")) {
                    q += " and categoryId=1 and ownerId=" + session.getAttribute("id");
                }

                if (request.getParameter("myParts") != null && request.getParameter("myParts").equals("true")) {
                    q += " and categoryId=2 and ownerId=" + session.getAttribute("id");
                }
            }

            ResultSet rs = st.executeQuery(q);

        %>


        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
                    <!--                    <p><a href="#">Link</a></p>
                                        <p><a href="#">Link</a></p>
                                        <p><a href="#">Link</a></p>-->
                </div>

                <div class="col-sm-8 text-left" >
                    <br>
                    <h3 class="well well-lg">
                        Products List

                        <%if (!searchStr.isEmpty()) {%>
                        <br><%=searchStr%>   
                        <%}%>
                    </h3>

                    <%
                        int idx = 0;

                        while (rs.next()) {

                    %>

                    <%if ((idx % 3) == 0) {%>
                    <div class="row">
                        <%}%>
                        <div class="col-sm-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><%=rs.getString("name")%></div>
                                <div class="panel-body"><img style="height:200px;" src="resources/img/products/<%=rs.getString("ownerId")%>_<%=rs.getString("id")%>.jpg" class="img-responsive" style="width:100%" alt="Image"></div>

                                <%
                                    for (int i = 0; i < tables.size(); i++) {

                                        String table = tables.get(i);
                                        String tableLabel = tableLabelMap.get(table);

                                        Statement st2 = db.connection.createStatement();
                                        String q2 = "select * from " + table + " where id=" + rs.getString(table + "Id");
                                        ResultSet rs2 = st2.executeQuery(q2);
                                        if (!rs2.next()) {
                                            continue;
                                        }
                                %>
                                <div class="panel-footer"><%=tableLabel%>: <%=rs2.getString("name")%></div>
                                <%}%>
                                <div class="btn btn-default" style="display: block; margin: 0 auto;"><a href="productDetails?productId=<%=rs.getString("id")%>">Details</a></div>
                                <%--<div class="btn btn-default" style="display: block; margin: 0 auto;"><a href="payment?productId=<%=rs.getString("id")%>">Buy</a></div>--%>

                                <div class="btn btn-default" style="display: block; margin: 0 auto;"><a href="profile?sellerId=<%=rs.getString("ownerId")%>">Contact Seller</a></div>
                            </div>
                        </div>
                        <%if ((idx % 3) == 2) {%>
                    </div>
                    <%}%>
                    <%idx++;
                        }
                        if ((idx % 3) != 0) {
                    %>
                </div>
                <%}%>

            </div>
            <div class="col-sm-2 sidenav">
                <!--                <div class="well">
                                    <p>ADS</p>
                                </div>
                                <div class="well">
                                    <p>ADS</p>
                                </div>-->
            </div>
        </div>
    </div>


    <!-- FOOTER -->
    <%@ include file="footer.jsp" %>
    <!-- END FOOTER -->

</body>
</html>
