<%@page import="com.carshop.utils.Constants"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kustom Klassifieds</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="js/profileJs.js" type="text/javascript"></script>
    </head>
    <body>

        <!-- HEADER -->
        <%@ include file="header.jsp" %>
        <!-- END HEADER -->

        <%
            if (session.getAttribute("id") == null || session.getAttribute("id").toString().equals("")) {
                response.sendRedirect("home");
            }

            Statement st1 = db.connection.createStatement();
            String q1 = "";
            ResultSet rs1;
            Statement st2 = db.connection.createStatement();
            String q2 = "";
            ResultSet rs2;
            Statement st3 = db.connection.createStatement();
            String q3 = "";
            ResultSet rs3;

            q1 = "select * from user where id=" + session.getAttribute("id");
            rs1 = st1.executeQuery(q1);
            rs1.next();

        %>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">

                </div>




                <hr class="">
                <div class="container target col-sm-8" >
                    <%if (session.getAttribute("successMsg") != null) {%>
                    <div class="alert alert-success">
                        <Strong><%=session.getAttribute("successMsg")%></Strong>
                    </div>
                    <%}%>
                    <%if (session.getAttribute("errorMsg") != null) {%>
                    <div class="alert alert-danger">
                        <Strong><%=session.getAttribute("errorMsg")%></Strong>
                    </div>
                    <%}
                        session.setAttribute("successMsg", null);
                        session.setAttribute("errorMsg", null);

                    %>
                    <div class="row">
                        <div class="col-sm-10">
                            <h1 class=""><%=rs1.getString("name") + "(" + rs1.getString("username") + ")"%></h1>

                            <a class="btn btn-success" href="mailto:<%=rs1.getString("email")%>">Send me an email</a>  <button type="button" class="btn btn-info">Send me a message</button>
                            <br>
                        </div>
                        <div class="col-sm-2"><a href="#" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.rlsandbox.com/img/profile.jpg"></a>

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3">
                            <!--left col-->
                            <ul class="list-group">
                                <li class="list-group-item text-muted" contenteditable="false">Profile</li>
                                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Joined</strong></span> 2.13.2014</li>
                                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Last seen</strong></span> Yesterday</li>
                                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Real name</strong></span> Joseph
                                    Doe</li>
                            </ul>
                            <div class="panel panel-default">
                                <div class="panel-heading">Certified from Kustom Classified?

                                </div>
                                <div class="panel-body"><i style="color:green" class="fa fa-check-square"></i> Yes, I am certified.

                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i>

                                </div>
                                <div class="panel-body"><a href="#" class="">google.com</a>

                                </div>
                            </div>

                            <ul class="list-group">
                                <li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i>

                                </li>
                                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Sells</strong></span> 125</li>
                                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Buys</strong></span> 13</li>
                            </ul>
                            <div class="panel panel-default">
                                <div class="panel-heading">Social Media</div>
                                <div class="panel-body">	<i class="fa fa-facebook fa-2x"></i>  <i class="fa fa-github fa-2x"></i> 
                                    <i class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i>  <i class="fa fa-google-plus fa-2x"></i>

                                </div>
                            </div>
                        </div>
                        <!--/col-3-->
                        <div class="col-sm-9" style="" contenteditable="false">
                            <div class="panel panel-default">
                                <div class="panel-heading">Starfox221's Bio</div>
                                <div class="panel-body"> A long description about me.

                                </div>
                            </div>
                            <%
                                q2 = "select * from product where ownerId=" + rs1.getString("id");
                                rs2 = st2.executeQuery(q2);

                            %>

                            <div class="panel panel-default target">
                                <div class="panel-heading" contenteditable="false">Cars and Parts I Own</div>
                                <div class="panel-body">
                                    <%                                                int cnt = 0;
                                        while (rs2.next()) {

                                            if ((cnt % 3) == 0) {
                                    %>
                                    <div class="row">
                                        <%}%>
                                        <div class="col-md-4">
                                            <div class="thumbnail">
                                                <img alt="300x200" src="resources/img/products/<%=rs2.getString("ownerId")%>_<%=rs2.getString("id")%>.jpg">
                                                <div class="caption">
                                                    <h3>
                                                        <%=rs2.getString("name")%>
                                                    </h3>
                                                    <p>
                                                        $<%=rs2.getString("price")%>
                                                    </p>
                                                    <p>

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <%if ((cnt % 3) == 2) {%>    
                                    </div>
                                    <%}
                                            cnt++;
                                        }
                                        if((cnt%3)!=0){
                                    %>
                                </div>
                                <%}%>
                                    <button class="btn btn-success" onclick="location.href = 'addProduct'">Add a Vehicle</button>

                                </div>

                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">Starfox221's Bio</div>
                                <div class="panel-body"> A long description about me.

                                </div>
                            </div></div>


                        <div id="push"></div>
                    </div>


                    <script src="/plugins/bootstrap-select.min.js"></script>
                    <script src="/codemirror/jquery.codemirror.js"></script>
                    <script src="/beautifier.js"></script>
                    <script>
                                        (function (i, s, o, g, r, a, m) {
                                            i['GoogleAnalyticsObject'] = r;
                                            i[r] = i[r] || function () {
                                                (i[r].q = i[r].q || []).push(arguments)
                                            }, i[r].l = 1 * new Date();
                                            a = s.createElement(o),
                                                    m = s.getElementsByTagName(o)[0];
                                            a.async = 1;
                                            a.src = g;
                                            m.parentNode.insertBefore(a, m)
                                        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                        ga('create', 'UA-40413119-1', 'bootply.com');
                                        ga('send', 'pageview');
                    </script>
                    <script>
                        jQuery.fn.shake = function (intShakes, intDistance, intDuration, foreColor) {
                            this.each(function () {
                                if (foreColor && foreColor != "null") {
                                    $(this).css("color", foreColor);
                                }
                                $(this).css("position", "relative");
                                for (var x = 1; x <= intShakes; x++) {
                                    $(this).animate({left: (intDistance * -1)}, (((intDuration / intShakes) / 4)))
                                            .animate({left: intDistance}, ((intDuration / intShakes) / 2))
                                            .animate({left: 0}, (((intDuration / intShakes) / 4)));
                                    $(this).css("color", "");
                                }
                            });
                            return this;
                        };
                    </script>
                    <script>
                        $(document).ready(function () {

                            $('.tw-btn').fadeIn(3000);
                            $('.alert').delay(5000).fadeOut(1500);

                            $('#btnLogin').click(function () {
                                $(this).text("...");
                                $.ajax({
                                    url: "/loginajax",
                                    type: "post",
                                    data: $('#formLogin').serialize(),
                                    success: function (data) {
                                        //console.log('data:'+data);
                                        if (data.status == 1 && data.user) { //logged in
                                            $('#menuLogin').hide();
                                            $('#lblUsername').text(data.user.username);
                                            $('#menuUser').show();
                                            /*
                                             $('#completeLoginModal').modal('show');
                                             $('#btnYes').click(function() {
                                             window.location.href="/";
                                             });
                                             */
                                        } else {
                                            $('#btnLogin').text("Login");
                                            prependAlert("#spacer", data.error);
                                            $('#btnLogin').shake(4, 6, 700, '#CC2222');
                                            $('#username').focus();
                                        }
                                    },
                                    error: function (e) {
                                        $('#btnLogin').text("Login");
                                        console.log('error:' + JSON.stringify(e));
                                    }
                                });
                            });
                            $('#btnRegister').click(function () {
                                $(this).text("Wait..");
                                $.ajax({
                                    url: "/signup?format=json",
                                    type: "post",
                                    data: $('#formRegister').serialize(),
                                    success: function (data) {
                                        console.log('data:' + JSON.stringify(data));
                                        if (data.status == 1) {
                                            $('#btnRegister').attr("disabled", "disabled");
                                            $('#formRegister').text('Thanks. You can now login using the Login form.');
                                        } else {
                                            prependAlert("#spacer", data.error);
                                            $('#btnRegister').shake(4, 6, 700, '#CC2222');
                                            $('#btnRegister').text("Sign Up");
                                            $('#inputEmail').focus();
                                        }
                                    },
                                    error: function (e) {
                                        $('#btnRegister').text("Sign Up");
                                        console.log('error:' + e);
                                    }
                                });
                            });

                            $('.loginFirst').click(function () {
                                $('#navLogin').trigger('click');
                                return false;
                            });

                            $('#btnForgotPassword').on('click', function () {
                                $.ajax({
                                    url: "/resetPassword",
                                    type: "post",
                                    data: $('#formForgotPassword').serializeObject(),
                                    success: function (data) {
                                        if (data.status == 1) {
                                            prependAlert("#spacer", data.msg);
                                            return true;
                                        } else {
                                            prependAlert("#spacer", "Your password could not be reset.");
                                            return false;
                                        }
                                    },
                                    error: function (e) {
                                        console.log('error:' + e);
                                    }
                                });
                            });

                            $('#btnContact').click(function () {

                                $.ajax({
                                    url: "/contact",
                                    type: "post",
                                    data: $('#formContact').serializeObject(),
                                    success: function (data) {
                                        if (data.status == 1) {
                                            prependAlert("#spacer", "Thanks. We got your message and will get back to you shortly.");
                                            $('#contactModal').modal('hide');
                                            return true;
                                        } else {
                                            prependAlert("#spacer", data.error);
                                            return false;
                                        }
                                    },
                                    error: function (e) {
                                        console.log('error:' + e);
                                    }
                                });
                                return false;
                            });

                            /*
                             $('.nav .dropdown-menu input').on('click touchstart',function(e) {
                             e.stopPropagation();
                             });
                             */





                        });
                        $.fn.serializeObject = function ()
                        {
                            var o = {};
                            var a = this.serializeArray();
                            $.each(a, function () {
                                if (o[this.name] !== undefined) {
                                    if (!o[this.name].push) {
                                        o[this.name] = [o[this.name]];
                                    }
                                    o[this.name].push(this.value || '');
                                } else {
                                    o[this.name] = this.value || '';
                                }
                            });
                            return o;
                        };
                        var prependAlert = function (appendSelector, msg) {
                            $(appendSelector).after('<div class="alert alert-info alert-block affix" id="msgBox" style="z-index:1300;margin:14px!important;">' + msg + '</div>');
                            $('.alert').delay(3500).fadeOut(1000);
                        }
                    </script>
                    <!-- Quantcast Tag -->
                    <script type="text/javascript">
                        var _qevents = _qevents || [];

                        (function () {
                            var elem = document.createElement('script');
                            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
                            elem.async = true;
                            elem.type = "text/javascript";
                            var scpt = document.getElementsByTagName('script')[0];
                            scpt.parentNode.insertBefore(elem, scpt);
                        })();

                        _qevents.push({
                            qacct: "p-0cXb7ATGU9nz5"
                        });
                    </script>
                    <noscript>
                    &amp;amp;amp;amp;amp;amp;amp;lt;div style="display:none;"&amp;amp;amp;amp;amp;amp;amp;gt;
                    &amp;amp;amp;amp;amp;amp;amp;lt;img src="//pixel.quantserve.com/pixel/p-0cXb7ATGU9nz5.gif" border="0" height="1" width="1" alt="Quantcast"/&amp;amp;amp;amp;amp;amp;amp;gt;
                    &amp;amp;amp;amp;amp;amp;amp;lt;/div&amp;amp;amp;amp;amp;amp;amp;gt;
                    </noscript>
                    <!-- End Quantcast tag -->
                    <div id="completeLoginModal" class="modal hide">
                        <div class="modal-header">
                            <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                            <h3>Do you want to proceed?</h3>
                        </div>
                        <div class="modal-body">
                            <p>This page must be refreshed to complete your login.</p>
                            <p>You will lose any unsaved work once the page is refreshed.</p>
                            <br><br>
                            <p>Click "No" to cancel the login process.</p>
                            <p>Click "Yes" to continue...</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" id="btnYes" class="btn danger">Yes, complete login</a>
                            <a href="#" data-dismiss="modal" aria-hidden="true" class="btn secondary">No</a>
                        </div>
                    </div>
                    <div id="forgotPasswordModal" class="modal hide">
                        <div class="modal-header">
                            <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                            <h3>Password Lookup</h3>
                        </div>
                        <div class="modal-body">
                            <form class="form form-horizontal" id="formForgotPassword">    
                                <div class="control-group">
                                    <label class="control-label" for="inputEmail">Email</label>
                                    <div class="controls">
                                        <input name="_csrf" id="token" value="CkMEALL0JBMf5KSrOvu9izzMXCXtFQ/Hs6QUY=" type="hidden">
                                        <input name="email" id="inputEmail" placeholder="you@youremail.com" required="" type="email">
                                        <span class="help-block"><small>Enter the email address you used to sign-up.</small></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer pull-center">
                            <a href="#" data-dismiss="modal" aria-hidden="true" class="btn">Cancel</a> 	
                            <a href="#" data-dismiss="modal" id="btnForgotPassword" class="btn btn-success">Reset Password</a>
                        </div>

                    </div>
                    <div id="upgradeModal" class="modal hide">
                        <div class="modal-header">
                            <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                            <h4>Would you like to upgrade?</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-center"><strong></strong></p>
                            <h1 class="text-center">$4<small>/mo</small></h1>
                            <p class="text-center"><small>Unlimited plys. Unlimited downloads. No Ads.</small></p>
                            <p class="text-center"><img src="/assets/i_visa.png" alt="visa" width="50"> <img src="/assets/i_mc.png" alt="mastercard" width="50"> <img src="/assets/i_amex.png" alt="amex" width="50"> <img src="/assets/i_discover.png" alt="discover" width="50"> <img src="/assets/i_paypal.png" alt="paypal" width="50"></p>
                        </div>
                        <div class="modal-footer pull-center">
                            <a href="/upgrade" class="btn btn-block btn-huge btn-success"><strong>Upgrade Now</strong></a>
                            <a href="#" data-dismiss="modal" class="btn btn-block btn-huge">No Thanks, Maybe Later</a>
                        </div>
                    </div>
                    <div id="contactModal" class="modal hide">
                        <div class="modal-header">
                            <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
                            <h3>Contact Us</h3>
                            <p>suggestions, questions or feedback</p>
                        </div>
                        <div class="modal-body">
                            <form class="form form-horizontal" id="formContact">
                                <input name="_csrf" id="token" value="CkMEALL0JBMf5KSrOvu9izzMXCXtFQ/Hs6QUY=" type="hidden">
                                <div class="control-group">
                                    <label class="control-label" for="inputSender">Name</label>
                                    <div class="controls">
                                        <input name="sender" id="inputSender" class="input-large" placeholder="Your name" type="text">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputMessage">Message</label>
                                    <div class="controls">
                                        <textarea name="notes" rows="5" id="inputMessage" class="input-large" placeholder="Type your message here"></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputEmail">Email</label>
                                    <div class="controls">
                                        <input name="email" id="inputEmail" class="input-large" placeholder="you@youremail.com (for reply)" required="" type="text">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer pull-center">
                            <a href="#" data-dismiss="modal" aria-hidden="true" class="btn">Cancel</a>     
                            <a href="#" data-dismiss="modal" aria-hidden="true" id="btnContact" role="button" class="btn btn-success">Send</a>
                        </div>
                    </div>
                    <script src="/plugins/bootstrap-pager.js"></script>
                </div>
                <div class="col-sm-2 sidenav">

                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <%@ include file="footer.jsp" %>
        <!-- END FOOTER -->

    </body>
</html>
