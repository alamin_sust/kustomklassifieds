<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="com.carshop.utils.Constants"%>
<%@page import="com.carshop.utils.Statics"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.carshop.connection.Database"%>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>Admin</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="css/loginRegister.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <%

            Database db = new Database();
            db.connect();

            try {
            Constants constants = new Constants();
            session.setAttribute("username", null);
            session.setAttribute("id", null);

            session.setAttribute("successMsg", null);
            session.setAttribute("errorMsg", null);

            Statement st1 = db.connection.createStatement();
            String q1 = "";
            ResultSet rs1;
            Statement st2 = db.connection.createStatement();
            String q2 = "";
            ResultSet rs2;
            Statement st3 = db.connection.createStatement();
            String q3 = "";
            ResultSet rs3;

            String successMsg = "";
            String errorMsg = "";

            if (request.getParameter("type") != null && request.getParameter("type").equals("insert")) {

                List<String> nameList = Arrays.asList(request.getParameter("name").split(","));
                
                for(int i=0;i<nameList.size();i++) {
                q1 = "select max(id)+1 as mxid from " + request.getParameter("tableName");
                rs1 = st1.executeQuery(q1);
                rs1.next();

                q2 = "insert into " + request.getParameter("tableName") + " (id,name) values(" + rs1.getString("mxid") + ",'" + nameList.get(i).trim() + "')";
                st2.executeUpdate(q2);
                successMsg = "Successfully Inserted into Database!";
                }
                session.setAttribute("successMsg", successMsg);

            }
            
        %>




        <div class="container">

            <header>

            </header>
            <section>

                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">



                        <div id="login" class="animate form">
                            <form  action="admin" method="post" autocomplete="on">
                                <h1>Database Inserts</h1>
                                <%if (session.getAttribute("successMsg") != null) {%>
                                <div class="alert alert-success">
                                    <Strong><%=session.getAttribute("successMsg")%></Strong>
                                </div>
                                <%}%>
                                <%if (session.getAttribute("errorMsg") != null) {%>
                                <div class="alert alert-danger">
                                    <Strong><%=session.getAttribute("errorMsg")%></Strong>
                                </div>
                                <%}%>
                                <p> 
                                    <label for="username" class="uname" >Table Name </label>
                                    <select class="form-control" name="tableName" required="">
                                        <%for (int i = 0; i < constants.getDB_TABLES().size(); i++) {%>
                                        <option value="<%=constants.getDB_TABLES().get(i)%>">
                                            <%=constants.getDB_TABLES().get(i)%>
                                        </option>
                                        <%}%>
                                    </select>
                                </p>
                                <p> 
                                    <label for="username" class="uname" > Name </label>
                                    <input id="username" name="name" required="required" type="text" placeholder="name"/>
                                </p>
                                <p class="signin button"> 
                                    <a href="home">Return to Home Page</a>
                                </p>
                                <p class="signin button"> 
                                    <input type="hidden" name="type" value="insert"/>
                                    <input type="submit" value="Insert"/> 
                                </p>

                            </form>
                        </div>



                    </div>
                </div>  
            </section>
        </div>
        <%
} catch(Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
    </body>
</html>