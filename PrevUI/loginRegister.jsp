<%@page import="com.carshop.utils.Statics"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.carshop.connection.Database"%>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>Login and Registration Form with HTML5 and CSS3</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="css/loginRegister.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <%

            Database db = new Database();
            db.connect();

            try {
            session.setAttribute("username", null);
            session.setAttribute("id", null);
            
            session.setAttribute("successMsg", null);
            session.setAttribute("errorMsg", null);

            Statement st1 = db.connection.createStatement();
            String q1 = "";
            ResultSet rs1;
            Statement st2 = db.connection.createStatement();
            String q2 = "";
            ResultSet rs2;
            Statement st3 = db.connection.createStatement();
            String q3 = "";
            ResultSet rs3;

            String successMsg = "";
            String errorMsg = "";

            String type = request.getParameter("type");

            if (type != null) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");

                q1 = "select * from user where username='" + username + "'";
                rs1 = st1.executeQuery(q1);

                if (type.equals("login")) {

                    if (rs1.next()) {
                        if (rs1.getString("password").equals(password)) {
                            session.setAttribute("username", username);
                            session.setAttribute("id", rs1.getString("id"));
                            session.setAttribute("successMsg", Statics.LOGIN_SUCCEESS);
                            response.sendRedirect("profile");
                        } else {
                            session.setAttribute("errorMsg",Statics.LOGIN_MISSMATCH);
                        }
                    } else {
                        session.setAttribute("errorMsg",Statics.LOGIN_USERNAME_NOT_EXISTS);
                    }

                } else if (type.equals("register")) {
                    if (rs1.next()) {
                        session.setAttribute("errorMsg",Statics.REGISTER_USERNAME_ALREADY_EXISTS);
                    } else {
                        String pass = request.getParameter("password");
                        String pass2 = request.getParameter("password2");
                        if (pass!=null && pass2!=null && pass.equals(pass2)) {
                            q2 = "select max(id)+1 as mxid from user";
                            rs2 = st2.executeQuery(q2);
                            rs2.next();

                            String id = rs2.getString("mxid");
                            String name = request.getParameter("name");
                            String phone = request.getParameter("phone");
                            String mobile = request.getParameter("mobile");
                            String address = request.getParameter("address");
                            String sex = "undefined"; //request.getParameter("sex");
                            String email = request.getParameter("email");

                            q3 = "insert into user(id,username,password,name,phone,mobile,address,sex,email) values(" + id + ",'" + username + "','" + password + "','" + name + "','" + phone + "','" + mobile + "','" + address + "','" + sex + "','" + email + "')";
                            st3.executeUpdate(q3);
                            session.setAttribute("username", username);
                            session.setAttribute("id", rs2.getString("mxid"));
                            session.setAttribute("successMsg", Statics.REGISTER_SUCCEESS);
                            
                            response.sendRedirect("profile");
                        } else {
                            session.setAttribute("errorMsg",Statics.REGISTER_PASSWORD_MISSMATCH);
                        }
                    }
                }
            }

        %>




        <div class="container">

            <header>
<a class="navbar-brand" style="width:200px;" href="home"><img src="resources/img/logo-white-t.png" style="width: 100px; height: 27px;"/></a>
            </header>
            <section>
                <%if (session.getAttribute("successMsg")!=null) {%>
                <div class="alert alert-success">
                    <Strong><%=session.getAttribute("successMsg")%></Strong>
                </div>
                <%}%>
                <%if (session.getAttribute("errorMsg")!=null) {%>
                <div class="alert alert-danger">
                    <Strong><%=session.getAttribute("errorMsg")%></Strong>
                </div>
                <%}%>
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <h1>Login and Registration Form</h1>

                        <div id="login" class="animate form">
                            <form  action="loginRegister" method="post" autocomplete="on">
                                <h1>Log in</h1> 
                                <p> 
                                    <label for="username" class="uname" > Your username </label>
                                    <input id="username" name="username" required="required" type="text" placeholder="myusername"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd"> Your password </label>
                                    <input id="password" name="password" minlength="2" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                                </p>
                                <!--                                <p class="keeplogin"> 
                                                                    <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
                                                                    <label for="loginkeeping">Keep me logged in</label>
                                                                </p>-->
                                <p class="signin button"> 
                                    <a href="home">Return to Home Page</a>
                                </p>
                                <p class="signin button"> 
                                    <input type="hidden" name="type" value="login"/>
                                    <input type="submit" value="Log in"/> 
                                </p>
                                <p class="change_link">
                                    Not a member yet ?
                                    <a href="#toregister" class="to_register">Join us</a>
                                </p>
                            </form>
                        </div>

                        <div id="register" class="animate form">
                            <form  action="loginRegister#toregister" method="post" autocomplete="on">
                                <h1> Sign up </h1> 
                                <p> 
                                    <label for="usernamesignup" class="uname" >Your username</label>
                                    <input id="usernamesignup" name="username" required="required" type="text" placeholder="mysuperusername690" />
                                </p>
                                <p> 
                                    <label for="emailsignup" class="youmail" > Your email</label>
                                    <input id="emailsignup" name="email" required="required" type="email" placeholder="mysupermail@mail.com"/> 
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" >Your password </label>
                                    <input id="passwordsignup" minlength="2" name="password" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" >Please confirm your password </label>
                                    <input id="passwordsignup_confirm" minlength="2" name="password2" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>

                                <p> 
                                    <label for="usernamesignup" class="uname" >Your name</label>
                                    <input id="usernamesignup"name="name" type="text" placeholder="name" required/>
                                </p>

                                <p> 
                                    <label for="usernamesignup" class="uname" >Your phone</label>
                                    <input id="usernamesignup"name="phone" type="text" placeholder="phone" required/>
                                </p>

                                <p> 
                                    <label for="usernamesignup" class="uname">Your mobile</label>
                                    <input id="usernamesignup" name="mobile" type="text" placeholder="mobile" required/>
                                </p>

                                <p> 
                                    <label for="usernamesignup" class="uname" >Your address</label>
                                    <input id="usernamesignup" name="address" type="text" placeholder="address" />
                                </p>

<!--                                <p> 
                                    <label for="usernamesignup" class="uname" >Your sex</label>
                                    <select name="sex" class="form-control">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </p>-->

                                <p class="signin button"> 
                                    <a href="home">Return to Home Page</a>
                                </p>
                                <p class="signin button"> 
                                    <input type="hidden" name="type" value="register"/>
                                    <input type="submit" value="Sign up"/> 
                                </p>
                                <p class="change_link">  
                                    Already a member ?
                                    <a href="#tologin" class="to_register"> Go and log in </a>
                                </p>
                            </form>
                        </div>

                    </div>
                </div>  
            </section>
        </div>
        <%
            } catch(Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
    </body>
</html>